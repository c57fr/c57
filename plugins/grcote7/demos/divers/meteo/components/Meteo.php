<?php

/*
 * Ce fichier est la propriété de C57.fr
 *
 * (c) Membre@c57.fr - 2019
 *
 * Et C57... C'est à VOUS !
 *
 * Sérieusement, ce fichier source est sujet à la license MIT*.
 * Mais je compte sur vous pour toujours chercher à l'améliorer et à votre tour, en faire profiter
 * un max de monde grâce aux techniques offertes dans c57.fr.
 *
 * @Bi1tô, & Bon code !
 *
 *  *: En gros...: Vous en faites ce que vous voulez !!!
 */

namespace GrCOTE7\Demos\Divers\Meteo\Components;

//TouDoLi Overwrite de ce plugin pour:
  //- format nombre
//- Clé APPID

use Cms\Classes\ComponentBase;
use Request;

class Meteo extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'Météo locale',
            'description' => 'Donne la météo locale sur une page.',
        ];
    }

    public function defineProperties()
    {
        return [
            'country' => [
                'title'   => 'Country',
                'type'    => 'dropdown',
                'default' => 'fr',
            ],
            'state' => [
                'title'       => 'State',
                'type'        => 'dropdown',
                'default'     => 'nord',
                'depends'     => ['country'],
                'placeholder' => 'Select a state',
            ],
            'city' => [
                'title'             => 'City',
                'type'              => 'string',
                'default'           => 'Valenciennes',
                'placeholder'       => 'Enter the city name',
                'validationPattern' => '^[0-9a-zA-Z\s]+$',
                'validationMessage' => 'The City field is required.',
            ],
            'units' => [
                'title'       => 'Units',
                'description' => 'Units for the temperature and wind speed',
                'type'        => 'dropdown',
                'default'     => 'Metric',
                'placeholder' => 'Select units',
                'options'     => [
                    'metric'   => 'Metric',
                    'imperial' => 'Imperial',
                ],
            ],
        ];
    }

    public function getCountryOptions()
    {
        $countries = $this->loadCountryData();
        $result    = [];

        foreach ($countries as $code => $data) {
            $result[$code] = $data['n'];
        }

        return $result;
    }

    public function getStateOptions()
    {
        $countries   = $this->loadCountryData();
        $countryCode = Request::input('country');

        return isset($countries[$countryCode]) ? $countries[$countryCode]['s'] : [];
    }

    public function onRun()
    {
        $this->addCss('/plugins/grcote7/demos/divers/meteo/assets/css/meteo.css');
        $this->page['weatherInfo'] = $this->info();
    }

    public function info()
    {
        $json = file_get_contents(sprintf(
            'http://api.openweathermap.org/data/2.5/weather?lang=fr&q=%s,%s,%s&units=%s&APPID=df792b8809b12bff453b48c2aa81b75e',
            $this->property('city'),
            $this->property('state'),
            $this->property('country'),
            $this->property('units')
      ));

        // echo '<pre>'; print_r($json); echo '</pre>';

        return json_decode($json);
    }

    protected function loadCountryData()
    {
        return json_decode(file_get_contents(__DIR__.'/../data/countries-and-states.json'), true);
    }
}