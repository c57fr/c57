<?php

/*
 * Ce fichier est la propriété de C57.fr
 *
 * (c) Membre@c57.fr - 2019
 *
 * Et C57... C'est à VOUS !
 *
 * Sérieusement, ce fichier source est sujet à la license MIT*.
 * Mais je compte sur vous pour toujours chercher à l'améliorer et à votre tour, en faire profiter
 * un max de monde grâce aux techniques offertes dans c57.fr.
 *
 * @Bi1tô, & Bon code !
 *
 *  *: En gros...: Vous en faites ce que vous voulez !!!
 */

namespace GrCOTE7\Demos\Divers\Ajax\Components;

use Cms\Classes\ComponentBase;
use October\Rain\Support\Facades\Flash;

class Ajax extends ComponentBase
{
  public $var;
  public $reponse;

  public function componentDetails()
  {
    return [
      'name'        => 'Ajax',
      'description' => 'Simple action since a click on a button (Ajax)',
    ];
  }

  // public function onRender()
  // {
  //   $this->var = $this->property('var'); // Récup param d'appel du component
  //   debug($this->var);
  // }

  public function onRun()
  {
    \Debugbar::enable();
    // $this->addCss(['components/w/assets/css/style.css']);

    $this->var = 123;
  }

  public function onClick()
  {
    $this->reponse = date(now()).'<br>';

    $this->page['chgLigne'] = '<br>';

    Flash::info('Informations reçues.');

    return [
      '#copie' => $this->renderPartial('@reponse', ['blockTitle' => 'Dernière réponse Ajax:<br>']),
    ];
  }
}