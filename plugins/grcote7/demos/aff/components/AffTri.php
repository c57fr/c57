<?php

/*
 * Ce fichier est la propriété de C57.fr
 *
 * (c) Membre@c57.fr - 2019
 *
 * Et C57... C'est à VOUS !
 *
 * Sérieusement, ce fichier source est sujet à la license MIT*.
 * Mais je compte sur vous pour toujours chercher à l'améliorer et à votre tour, en faire profiter
 * un max de monde grâce aux techniques offertes dans c57.fr.
 *
 * @Bi1tô, & Bon code !
 *
 *  *: En gros...: Vous en faites ce que vous voulez !!!
 */

namespace GrCOTE7\Demos\Aff\Components;

use Cms\Classes\ComponentBase;

class AffTri extends ComponentBase
{
  /**
   * This is a person's name.
   *
   * @var string
   */
  public $name;

  /**
   * This is the person's role.
   */
  public $role;

  /**
   * The items of the list.
   */
  public $items = ['tortelinis', 'pennes', 'nouilles', 'spaghettis'], $items2;

  public function componentDetails()
  {
    return [
      'name'        => 'Aff Tri',
      'description' => 'Implémente le simple affichage d\'un component.',
    ];
  }

  public function init()
  {
    /* This will execute when the component is
     first initialized, including AJAX events. */
  }

  public function onRun()
  {
    $this->addCss('components/afftri/assets/css/style.css');
    /* This code will note execute AJAX events.
     {{ affTri.name }} (strict) => Never conflict */
    $this->name = 'Lionel';
    $this->role = 'Admin';
    // {% set name = name %}

    // {{ dateDue }} (Relaxed)
    $this->page['dateDue'] = 'Today';
  }

  public function onSortList()
  {
    $items = $this->items;
    sort($items);
    $this->items2        = array_reverse($items);
    $this->page['items'] = $items;
  }
}