<?php

/**
 *             Ce fichier est la propriété de (ɔ) C57.fr
 *
 *             Membre@c57.fr - 2019-2023
 *
 *             Et C57... C'est à VOUS !
 *
 *             Sérieusement, ce fichier source est sujet à la license MIT*.
 *             Mais je compte sur vous pour toujours chercher à l'améliorer
 *             et à votre tour, en faire profiter un max de monde
 *             grâce aux techniques offertes dans c57.fr.
 *
 *             @Bi1tô, & Bon code !
 *
 *             *: En gros...: Vous en faites ce que vous voulez !!!
 */

namespace Grcote7\Movies\Updates;

use Winter\Storm\Database\Updates\Migration;

class BuilderTableUpdateGrcote7Movies3 extends Migration {
	public function up() {
		\Schema::table('grcote7_movies_', function ($table) {
			$table->dropColumn('actors');
		});
	}

	public function down() {
		\Schema::table('grcote7_movies_', function ($table) {
			$table->text('actors')->nullable();
		});
	}
}
