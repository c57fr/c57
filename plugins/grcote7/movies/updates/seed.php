<?php

/**
 *             Ce fichier est la propriété de (ɔ) C57.fr
 *
 *             Membre@c57.fr - 2019-2023
 *
 *             Et C57... C'est à VOUS !
 *
 *             Sérieusement, ce fichier source est sujet à la license MIT*.
 *             Mais je compte sur vous pour toujours chercher à l'améliorer
 *             et à votre tour, en faire profiter un max de monde
 *             grâce aux techniques offertes dans c57.fr.
 *
 *             @Bi1tô, & Bon code !
 *
 *             *: En gros...: Vous en faites ce que vous voulez !!!
 */

namespace Grcote7\Movies\Updates;

use Faker;
use Grcote7\Movies\Models\Movie;
use Winter\Storm\Database\Updates\Seeder;

class Seed extends Seeder {
	public function run() {
		$faker = Faker\Factory::create();

		for ($i = 0; $i < 100; ++$i) {
			$title = $faker->sentence($nbWords = 3, $variableNbWords = true);

			Movie::create(
				[
					'name'        => $title,
					'slug'        => str_slug($title, '-'),
					'description' => $faker->paragraph($nbSentences = 3, $variableNbSentences = true),
					'year'        => $faker->year($max = 'now'),
				]
			);
		}
	}
}
