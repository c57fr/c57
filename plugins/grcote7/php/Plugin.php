<?php

/*
 * Ce fichier est la propriété de C57.fr
 *
 * (c) Membre@c57.fr - 2019
 *
 * Et C57... C'est à VOUS !
 *
 * Sérieusement, ce fichier source est sujet à la license MIT*.
 * Mais je compte sur vous pour toujours chercher à l'améliorer et à votre tour, en faire profiter
 * un max de monde grâce aux techniques offertes dans c57.fr.
 *
 * @Bi1tô, & Très Bonne Année 2019.
 *
 * En gros...: Vous en faites ce que vous voulez !!!
 */

namespace GrCOTE7\Php;

use Backend;
use System\Classes\PluginBase;

/**
 * Php Plugin Information File.
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Php',
            'description' => 'No description provided yet...',
            'author'      => 'GrCOTE7',
            'icon'        => 'icon-leaf',
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     */
    public function register()
    {
    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {
    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        // return []; // Remove this line to activate
        return [
            'GrCOTE7\Php\Components\Php' => 'php',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate
        return [
            'grcote7.php.some_permission' => [
                'tab'   => 'Php',
                'label' => 'Some permission',
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return []; // Remove this line to activate
        return [
            'php' => [
                'label'       => 'Php',
                'url'         => Backend::url('grcote7/php/mycontroller'),
                'icon'        => 'icon-leaf',
                'permissions' => ['grcote7.php.*'],
                'order'       => 500,
            ],
        ];
    }
}
