<?php

/*
 * Ce fichier est la propriété de C57.fr
 *
 * (c) Membre@c57.fr - 2019
 *
 * Et C57... C'est à VOUS !
 *
 * Sérieusement, ce fichier source est sujet à la license MIT*.
 * Mais je compte sur vous pour toujours chercher à l'améliorer et à votre tour, en faire profiter
 * un max de monde grâce aux techniques offertes dans c57.fr.
 *
 * @Bi1tô, & Très Bonne Année 2019.
 *
 *  *: En gros...: Vous en faites ce que vous voulez !!!
 */

namespace GrCOTE7\Php\Components;

class Divers
{
    public function __construct()
    {
    }

    public function init()
    {
        // $result   = ['Users:'];
        // $result[] = $this->users();

        return $this->exempleClosureDansFonction();
    }

    public function exempleClosureDansFonction()
    {
        $ajouterCinq = $this->creerFonctionAjouter(5);
        $ajouterSept = $this->creerFonctionAjouter(7);

        return ['Closure dans fonction:', $ajouterCinq(1514) + $ajouterSept(777)];
    }

    public function creerFonctionAjouter($quantite)
    {
        $ajouter = function ($nombre) use ($quantite) {
            return $nombre + $quantite;
        };

        return $ajouter;
    }
}
