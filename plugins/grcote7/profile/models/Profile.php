<?php

/*
 * Ce fichier est la propriété de C57.fr
 *
 * (c) Membre@c57.fr - 2019
 *
 * Et C57... C'est à VOUS !
 *
 * Sérieusement, ce fichier source est sujet à la license MIT*.
 * Mais je compte sur vous pour toujours chercher à l'améliorer et à votre tour, en faire profiter
 * un max de monde grâce aux techniques offertes dans c57.fr.
 *
 * @Bi1tô, & Très Bonne Année 2019.
 *
 *  *: En gros...: Vous en faites ce que vous voulez !!!
 */

namespace GrCOTE7\Profile\Models;

use Model;

/**
 * Profile Model.
 */
class Profile extends Model
{
    /**
     * @var string the database table used by the model
     */
    public $table = 'grcote7_profile_profiles';

    /**
     * @var array Relations
     */
    public $belongsTo = [
        'user' => ['RainLab\User\Models\User'],
    ];

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    public static function getFromUser($user)
    {
        if ($user->profile) {
            return $user->profile;
        }
        $profile       = new static();
        $profile->user = $user;
        $profile->save();

        $user->profile = $profile;

        return $profile;
    }
}
