<?php

/**
 *             Ce fichier est la propriété de (ɔ) C57.fr
 *
 *             Membre@c57.fr - 2019-2023
 *
 *             Et C57... C'est à VOUS !
 *
 *             Sérieusement, ce fichier source est sujet à la license MIT*.
 *             Mais je compte sur vous pour toujours chercher à l'améliorer
 *             et à votre tour, en faire profiter un max de monde
 *             grâce aux techniques offertes dans c57.fr.
 *
 *             @Bi1tô, & Bon code !
 *
 *             *: En gros...: Vous en faites ce que vous voulez !!!
 */

namespace GrCOTE7\Profile\Updates;

use October\Rain\Database\Schema\Blueprint;
use Winter\Storm\Database\Updates\Migration;

class create_profiles_table extends Migration {
	public function up() {
		\Schema::create('grcote7_profile_profiles', function (Blueprint $table) {
			$table->engine = 'InnoDB';
			$table->increments('id');
			$table->integer('user_id')->unsigned()->index();
			$table->string('pseudo')->nullable;
			$table->string('parr')->nullable;
			$table->enum('sexe', ['Inconnu', 'H', 'F'])->default('Inconnu');
			$table->timestamps();
		});
	}

	public function down() {
		\Schema::dropIfExists('grcote7_profile_profiles');
	}
}
