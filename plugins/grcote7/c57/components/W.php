<?php

/*
 * Ce fichier est la propriété de C57.fr
 *
 * (c) Membre@c57.fr - 2019
 *
 * Et C57... C'est à VOUS !
 *
 * Sérieusement, ce fichier source est sujet à la license MIT*.
 * Mais je compte sur vous pour toujours chercher à l'améliorer et à votre tour, en faire profiter
 * un max de monde grâce aux techniques offertes dans c57.fr.
 *
 * @Bi1tô, & Bon code !
 *
 *  *: En gros...: Vous en faites ce que vous voulez !!!
 */

namespace GrCOTE7\C57\Components;

use Cms\Classes\ComponentBase;

class W extends ComponentBase
{
  public $var;

  public function componentDetails()
  {
    return [
      'name'        => 'W',
      'description' => 'Dev en cours',
    ];
  }

  public function onRun()
  {
    \Debugbar::enable();
    // $this->addCss(['components/w/assets/css/style.css']);
    //2do ini_set('display_errors',true); pour c57 sur SG (route/frontend)
    $this->var = $this->ready();
  }

  public function ready()
  {
    $this->testDB();

    return 'Ready.';
  }

  public function registerSchedule($schedule)
  {
    $schedule->call(function () {
      mail('li@fff.com', 'test', 'ok');
    })->everyFiveMinutes();

    $schedule->call(function () {
      \Db::table('system_event_logs')->delete();
    })->everyFiveMinutes();
  }

  public function testDB()
  {
    // $db = \Db::table('system_event_logs');
    \Db::insert('insert into system_event_logs (message, details) values (?,?)', [date(now()), 'Ok']);
    mail('li@fff.com', 'test', 'ok');
  }
}