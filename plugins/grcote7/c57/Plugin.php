<?php

/*
 * Ce fichier est la propriété de C57.fr
 *
 * (c) Membre@c57.fr - 2019
 *
 * Et C57... C'est à VOUS !
 *
 * Sérieusement, ce fichier source est sujet à la license MIT*.
 * Mais je compte sur vous pour toujours chercher à l'améliorer et à votre tour, en faire profiter
 * un max de monde grâce aux techniques offertes dans c57.fr.
 *
 * @Bi1tô, & Bon code !
 *
 *  *: En gros...: Vous en faites ce que vous voulez !!!
 */

namespace GrCOTE7\C57;

use System\Classes\PluginBase;

class Plugin extends PluginBase
{
  public function pluginDetails()
  {
    return [
      'name'        => 'Grcote7 c57',
      'description' => 'Fournit quelques components propres au concept c57.',
      'author'      => 'GC7',
      'icon'        => 'oc-icon-yelp',
    ];
  }

  public function registerComponents()
  {
    return [
      'GrCOTE7\C57\Components\W'              => 'w',
      'GrCOTE7\C57\Components\DerInscrits'    => 'derInscrits',
      'GrCOTE7\C57\Components\GitLabPageLink' => 'gitLabPageLink',
      'GrCOTE7\C57\Components\Gc7StaticMenu'  => 'gc7staticmenu',
    ];
  }

  public function registerPageSnippets()
  {
    return [
      'GrCOTE7\C57\Components\GitLabPageLink' => 'gitLabPageLink',
    ];
  }

  public function registerSettings()
  {
    return [
      'settings' => [
        'label'       => 'BGC Color',
        'description' => 'Manage background color.',
        'class'       => 'Grcote7\C57\Models\Settings',
        'category'    => 'C57',
        'icon'        => 'icon-globe',
        'order'       => 50,
        'keywords'    => 'c57',
        'permissions' => ['acme.users.access_settings'],
      ],
    ];
  }
}