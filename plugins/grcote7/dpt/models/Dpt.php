<?php namespace GrCOTE7\Dpt\Models;

use Model;

/**
 * Model
 */
class Dpt extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;


    /**
     * @var string The database table used by the model.
     */
    public $table = 'grcote7_dptlist_dpts';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];
}
