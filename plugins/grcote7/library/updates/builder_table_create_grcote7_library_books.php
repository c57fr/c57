<?php

/**
 *             Ce fichier est la propriété de (ɔ) C57.fr
 *
 *             Membre@c57.fr - 2019-2023
 *
 *             Et C57... C'est à VOUS !
 *
 *             Sérieusement, ce fichier source est sujet à la license MIT*.
 *             Mais je compte sur vous pour toujours chercher à l'améliorer
 *             et à votre tour, en faire profiter un max de monde
 *             grâce aux techniques offertes dans c57.fr.
 *
 *             @Bi1tô, & Bon code !
 *
 *             *: En gros...: Vous en faites ce que vous voulez !!!
 */

namespace Grcote7\Library\Updates;

use Winter\Storm\Database\Updates\Migration;

class BuilderTableCreateGrcote7LibraryBooks extends Migration {
	public function up() {
		\Schema::create('grcote7_library_books', function ($table) {
			$table->engine = 'InnoDB';
			$table->increments('id')->unsigned();
			$table->string('title')->nullable();
			$table->string('slug')->nullable();
			$table->string('author')->nullable();
			$table->string('year')->nullable();
			$table->text('short_description')->nullable();
		});
	}

	public function down() {
		\Schema::dropIfExists('grcote7_library_books');
	}
}
