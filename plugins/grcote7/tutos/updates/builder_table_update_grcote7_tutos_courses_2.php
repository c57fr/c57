<?php

/**
 *             Ce fichier est la propriété de (ɔ) C57.fr
 *
 *             Membre@c57.fr - 2019-2023
 *
 *             Et C57... C'est à VOUS !
 *
 *             Sérieusement, ce fichier source est sujet à la license MIT*.
 *             Mais je compte sur vous pour toujours chercher à l'améliorer
 *             et à votre tour, en faire profiter un max de monde
 *             grâce aux techniques offertes dans c57.fr.
 *
 *             @Bi1tô, & Bon code !
 *
 *             *: En gros...: Vous en faites ce que vous voulez !!!
 */

namespace Grcote7\Tutos\Updates;

use Winter\Storm\Database\Updates\Migration;

class BuilderTableUpdateGrcote7TutosCourses2 extends Migration {
	public function up() {
		\Schema::table('grcote7_tutos_courses', function ($table) {
			$table->text('notes')->nullable();
			$table->smallInteger('mastery')->nullable()->default(0);
			$table->string('title')->change();
			$table->string('slug')->change();
			$table->string('url')->change();
		});
	}

	public function down() {
		\Schema::table('grcote7_tutos_courses', function ($table) {
			$table->dropColumn('notes');
			$table->dropColumn('mastery');
			$table->string('title', 191)->change();
			$table->string('slug', 191)->change();
			$table->string('url', 191)->change();
		});
	}
}
