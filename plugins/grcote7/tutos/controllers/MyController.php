<?php

/*
 * Ce fichier est la propriété de C57.fr
 *
 * (c) Membre@c57.fr - 2019
 *
 * Et C57... C'est à VOUS !
 *
 * Sérieusement, ce fichier source est sujet à la license MIT*.
 * Mais je compte sur vous pour toujours chercher à l'améliorer et à votre tour, en faire profiter
 * un max de monde grâce aux techniques offertes dans c57.fr.
 *
 * @Bi1tô, & Bon code !
 *
 *  *: En gros...: Vous en faites ce que vous voulez !!!
 */

namespace Grcote7\Tutos\Controllers;

use Backend\Classes\Controller;
use BackendMenu;
use Cms\Classes\Theme;
use Flash;
use Illuminate\Support\Facades\Storage;
use October\Rain\Support\Facades\File;

class MyController extends Controller
{
    public $implement = [];

    //public $listConfig = 'config_mycontroller.yaml';

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Grcote7.Tutos', 'tutos', 'mycontroller');
    }

    public function index()
    {
        return \Backend::redirect('grcote7/tutos/mycontroller/helloworld');
        /*
        $config = $this->makeConfig('$/grcote7/tutos/models/tuto/columns.yaml');

        $config->model = new \Grcote7\Tutos\Models\Tuto();

        $config->recordUrl = 'grcote7/tutos/mycontroller/update/:id';

        $widget = $this->makeWidget('Backend\Widgets\Lists', $config);

        $widget->bindToController();

        $this->vars['widget'] = $widget;
        */
    }

    public function monIndex2()
    {
        // Retourne cette chaîne directement dans le backend
        // return 'Hello';
        // Check storage/logs/system.log

        $items      = [];
        $theme_dir  = Theme::getActiveTheme()->getDirName();
        $theme_path = themes_path($theme_dir).'/assets/images';

        $files = File::files($theme_path);

        foreach (glob($theme_path.'/*.{ico,png}', GLOB_BRACE) as $item) {
            $items[] = str_replace($theme_path, '', $item);
        }

        //dd($items);

        debug('Dossier du thème: '.$theme_dir, 'Chemin complet: '.$files[1]->getRealPath(), $items);
        debug('Source: monIndex2()');

        $this->vars['files'] = $items;
    }

    public function helloworld()
    {
        // Retourne cette chaîne
        // return 'Hello World !';

        // S'il n'y a rien, va chercher le .htm du dossier mycontroller
    }

    // /URL = be/grcote7/tutos/mycontroller/update/7
    // /URL = be/grcote7/tutos/mycontroller/update/7/foobar
    //
    public function update($id = null, $context = null)
    {
        $this->vars['myId'] = $this->getIdVide($id);
        debug($id);
        debug($this->vars['myId']);
        debug('update()');

        // Check storage/logs/system.log

        $this->vars['myId'] .= '<hr>'.(('7' === $id) ? 'OK, you respect the exemple of the code...' : 'Try with param 1...');
        //trace_log('update was called with ID = '.$id);

        $config = $this->makeConfig('$/grcote7/tutos/models/tuto/fields.yaml');

        if (1 !== $id && 2 !== $id) {
            $id = 2;
        }
        $config->model = \GrCote7\Tutos\Models\Tuto::find($id);

        $widget = $this->makeWidget('Backend\Widgets\Form', $config);

        $this->vars['widget'] = $widget;
    }

    public function affMsg()
    {
        debug('affmsg()');
    }

    public function update_onUpdate($id = null) // Action Ajax
    {
        Flash::success('Jobs done!');
        // Check storage/logs/system.log
        trace_log('onUpdate was called with ID = '.$id);
    }

    public function getIdVide($id)
    {
        return ($id) ? $id : 'Vide';
    }

    public function onUpdate3($id = null)
    {
        $data = post();

        // Check storage/logs/system.log
        trace_log('Enregistrement dans le fichier log', $data);

        Flash::success('Jobs done! (Logs updated)');
    }
}
