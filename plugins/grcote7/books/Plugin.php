<?php

/*
 * Ce fichier est la propriété de C57.fr
 *
 * (c) Membre@c57.fr - 2019
 *
 * Et C57... C'est à VOUS !' .
                       '*
 * Sérieusement, ce fichier source est sujet à la license MIT*.
 * Mais je compte sur vous pour toujours chercher à l'améliorer et à votre tour, en faire profiter
 * un max de monde grâce aux techniques offertes dans c57.fr.
 *
 * @Bi1tô, & Bon code !
 *
 *  *: En gros...: Vous en faites ce que vous voulez !!!
 */

namespace Grcote7\Books;

use System\Classes\PluginBase;

class Plugin extends PluginBase
{
  public function pluginDetails()
  {
    return [
      'name'        => 'Books Plugin',
      'description' => 'Just to entirely code all.',
      'author'      => 'Grcote7',
      'icon'        => 'icon-book',
      'homepage'    => 'https://github.com/chemin2bonheur/o',
    ];
  }

  public function registerComponents()
  {
    return [
      'Grcote7\Books\Components\Books'                           => 'Books',
      'Grcote7\Books\Components\Tests'                           => 'Tests',
      'Grcote7\Books\Components\MoreSimpleComponentForBooksList' => 'fakeBooks',
      'Grcote7\Books\Components\Clicks'                          => 'Clicks',
    ];
  }

  public function registerSettings()
  {
  }

  public function boot()
  {
  }

  // 2do activer ce schedule
  // public function registerSchedule($schedule) {
  //   $schedule->call(function () {
  //     $this['page']->theHour = time();
  //   })->everyFiveMinutes();
  // }
}