<?php

/**
 *             Ce fichier est la propriété de (ɔ) C57.fr
 *
 *             Membre@c57.fr - 2019-2023
 *
 *             Et C57... C'est à VOUS !
 *
 *             Sérieusement, ce fichier source est sujet à la license MIT*.
 *             Mais je compte sur vous pour toujours chercher à l'améliorer
 *             et à votre tour, en faire profiter un max de monde
 *             grâce aux techniques offertes dans c57.fr.
 *
 *             @Bi1tô, & Bon code !
 *
 *             *: En gros...: Vous en faites ce que vous voulez !!!
 */

namespace Grcote7\Books\Updates;

use Grcote7\Books\Models\Book;
use Grcote7\Books\Models\Owner;
use Winter\Storm\Database\Updates\Seeder;
use Winter\Storm\Support\Facades\DB;

class SeedAllTables extends Seeder {
	public function run() {
		trace_log('Table books re-initialized');
		DB::table('grcote7_books_')->truncate();

		$books = [
			[
				'title'       => '20 000 lieues sous les mers',
				'description' => '<p>Un voyage extraordinaire...</p>',
				'owner_id'    => 1,
			],
			[
				'title'       => 'Le Monde des A',
				'description' => '<p>SF de référence</p>',
				'owner_id'    => 2,
			],
			[
				'title'       => 'De la Terre à la lune',
				'description' => '<p>Une autre voyage extraordinaire</p>',
				'owner_id'    => 1,
			],
		];

		foreach ($books as $book) {
			Book::firstOrCreate(
				[
					'title'       => $book['title'],
					'description' => $book['description'],
					'slug'        => str_slug($book['title']),
					'published'   => 1,
					'owner_id'    => $book['owner_id'],
				]
			);
		}

		trace_log('Table owners re-initialized');
		DB::table('grcote7_books_owners')->truncate();

		Owner::firstOrCreate(['firstname' => 'lioNel', 'lastname' => 'CÔte', 'parr' => 0]);
		Owner::firstOrCreate(['firstname' => 'Lio181', 'lastname' => 'Yahoo', 'parr' => 1]);
		Owner::firstOrCreate(['firstname' => 'NonInclus', 'lastname' => '', 'parr' => 0]);
		Owner::firstOrCreate(['firstname' => 'Michel', 'lastname' => 'COLUCCI', 'parr' => 2]);
		Owner::firstOrCreate(['firstname' => 'Pierre', 'lastname' => 'RICHARD', 'parr' => 3]);
		Owner::firstOrCreate(['firstname' => 'PieRre', 'lastname' => 'palmAde', 'parr' => 3]);
	}
}
