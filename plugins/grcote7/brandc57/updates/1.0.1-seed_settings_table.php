<?php

/**
 *             Ce fichier est la propriété de (ɔ) C57.fr
 *
 *             Membre@c57.fr - 2019-2023
 *
 *             Et C57... C'est à VOUS !
 *
 *             Sérieusement, ce fichier source est sujet à la license MIT*.
 *             Mais je compte sur vous pour toujours chercher à l'améliorer
 *             et à votre tour, en faire profiter un max de monde
 *             grâce aux techniques offertes dans c57.fr.
 *
 *             @Bi1tô, & Bon code !
 *
 *             *: En gros...: Vous en faites ce que vous voulez !!!
 */

namespace GrCOTE7\BrandC57\Updates;

use Backend\Facades\BackendAuth;
use System\Console\WinterUp;
use Winter\Storm\Database\Updates\Seeder;
use Winter\Storm\Support\Facades\DB;

class Builder_Table_Seed extends Seeder {
	public function run() {
		// Activez ces lignes ci-dessous pour remplacer admin par vos infos lors de la migration

		// if (!BackendAuth::findUserByLogin('Vous')) {
		// 	$admin                        = BackendAuth::findUserByLogin('admin');
		// 	$admin->last_name             = 'Votre nom';
		// 	$admin->login                 = 'Votre prénom';
		// 	$admin->email                 = 'VotreEmail@provider.com';
		// 	$admin->password              = 'motdepasse';
		// 	$admin->password_confirmation = 'motdepasse';
		// 	$admin->save();

		// 	echo "\n" . str_repeat(' ', 12) . "- Votre compte créé\n";
		// }

		$date = date(now());

		// Put C57 Brand Favicon & logo if doesn't exist
		$images = [
			[
				1, 'gc7c57docfavicon.ico', 'favicon.ico', 1150, 'image/vnd.microsoft.icon', null, null, 'favicon', 1, 'Backend\Models\BrandSetting', 1, 1, $date, $date,
			],
			[
				2, 'gc7c57doclogo.svg', 'logo.svg', 727, 'image/svg+xml', null, null, 'logo', 1, 'Backend\Models\BrandSetting', 1, 5, $date, $date,
			],
		];

		$systemFilesFields = [
			'id', 'disk_name', 'file_name', 'file_size', 'content_type', 'title', 'description', 'field', 'attachment_id', 'attachment_type', 'is_public', 'sort_order', 'created_at', 'updated_at',
		];

		foreach ($images as $image) {
			$sql = [];
			foreach ($systemFilesFields as $k => $field) {
				$sql[$field] = $image[$k];
			}
			DB::table('system_files')->upsert($sql, ['id']);
		}

		// Create GrCOTE7
		if (!BackendAuth::findUserByLogin(env('USER_LOGIN', 'C57'))) {
			BackendAuth::register([
				'first_name'            => env('USER_FIRSTNAME', 'Moi'),
				'last_name'             => env('USER_LASTNAME', 'PERSO'),
				'login'                 => env('USER_LOGIN', 'C57'),
				'email'                 => env('USER_EMAIL', 'C57@c57.fr'),
				'password'              => env('USER_PASSWORD', 'motdepasse'),
				'password_confirmation' => env('USER_PASSWORD', 'motdepasse'),
				'is_activated'          => 1,
				'role_id'               => 2,
				'is_superuser'          => 1,
			]);
			echo str_repeat(' ', 12) . '- Membre ' . env('USER_LOGIN', 'C57') . " créé\n";
		}

		// Add C57 Brand parameters (Slogan, Font, ...)

		DB::table('system_settings')->upsert(
			[
				'id'    => 1,
				'item'  => 'backend_brand_settings',
				'value' => '{"app_name":"C57","app_tagline":"Soyez Partout !","primary_color":"#34495E","secondary_color":"#E67E22","accent_color":"#3498DB","default_colors":[{"color":"#1ABC9C"},{"color":"#16A085"},{"color":"#2ECC71"},{"color":"#27AE60"},{"color":"#3498DB"},{"color":"#2980B9"},{"color":"#9B59B6"},{"color":"#8E44AD"},{"color":"#34495E"},{"color":"#2B3E50"},{"color":"#F1C40F"},{"color":"#F39C12"},{"color":"#E67E22"},{"color":"#D35400"},{"color":"#E74C3C"},{"color":"#C0392B"},{"color":"#ECF0F1"},{"color":"#BDC3C7"},{"color":"#95A5A6"},{"color":"#7F8C8D"}],"menu_mode":"inline","custom_css":""}',
			],
			['item', 'value']
		);

		// Active C57 theme
		DB::table('system_parameters')
			->updateOrInsert(
				[
					'namespace' => 'cms',
					'group'     => 'theme',
					'item'      => 'active',
					'value'     => 'c57',
				]
			);

		// new WinterUp();
		// UpdateManager::instance()
		// ->update();
	}
}
