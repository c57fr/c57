<?php

/**
 *
 *             Ce fichier est la propriété de (ɔ) C57.fr
 *
 *             Membre@c57.fr - 2019-2021
 *
 *             Et C57... C'est à VOUS !
 *
 *             Sérieusement, ce fichier source est sujet à la license MIT*.
 *             Mais je compte sur vous pour toujours chercher à l'améliorer et à votre tour, en faire profiter
 *             un max de monde grâce aux techniques offertes dans c57.fr.
 *
 *             @Bi1tô, & Bon code !
 *
 *              *: En gros...: Vous en faites ce que vous voulez !!!
 */

namespace GC7\Librairy\Models;

use Model;

/**
 * Book Model.
 */
class Book extends \Model {
	use \Winter\Storm\Database\Traits\Validation;

	/**
	 * @var string the database table used by the model
	 */
	public $table = 'gc7_librairy_books';

	/**
	 * @var array Validation rules for attributes
	 */
	public $rules = [
		'title'            => 'required|min:4',
		'synopsis'         => 'required|min:20',
		'publication_year' => 'required',
	];

	/**
	 * @var array Relations
	 */
	public $hasOne = [];

	public $hasMany = [];

	public $hasOneThrough = [];

	public $hasManyThrough = [];

	public $belongsTo = [];

	public $belongsToMany = [];

	public $morphTo = [];

	public $morphOne = [];

	public $morphMany = [];

	public $attachOne = [];

	public $attachMany = [];

	/**
	 * @var array Guarded fields
	 */
	protected $guarded = ['*'];

	/**
	 * @var array Fillable fields
	 */
	protected $fillable = [];

	/**
	 * @var array Attributes to be cast to native types
	 */
	protected $casts = [];

	/**
	 * @var array Attributes to be cast to JSON
	 */
	protected $jsonable = [];

	/**
	 * @var array Attributes to be appended to the API representation of the model (ex. toArray())
	 */
	protected $appends = [];

	/**
	 * @var array Attributes to be removed from the API representation of the model (ex. toArray())
	 */
	protected $hidden = [];

	/**
	 * @var array Attributes to be cast to Argon (Carbon) instances
	 */
	protected $dates = [
		'created_at',
		'updated_at',
	];
}
