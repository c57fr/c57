<?php

namespace GC7\Librairy;

use Backend;
use Backend\Models\UserRole;
use System\Classes\PluginBase;

/**
 * Librairy Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     */
    public function pluginDetails(): array
    {
        return [
            'name'        => 'gc7.librairy::lang.plugin.name',
            'description' => 'gc7.librairy::lang.plugin.description',
            'author'      => 'GC7',
            'icon'        => 'icon-book'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     */
    public function register(): void
    {
    }

    /**
     * Boot method, called right before the request route.
     */
    public function boot(): void
    {
    }

    /**
     * Registers any frontend components implemented in this plugin.
     */
    public function registerComponents(): array
    {
        return []; // Remove this line to activate

        return [
            'GC7\Librairy\Components\MyComponent' => 'myComponent',
        ];
    }

    /**
     * Registers any backend permissions used by this plugin.
     */
    public function registerPermissions(): array
    {
        return []; // Remove this line to activate

        return [
            'gc7.librairy.some_permission' => [
                'tab' => 'gc7.librairy::lang.plugin.name',
                'label' => 'gc7.librairy::lang.permissions.some_permission',
                'roles' => [UserRole::CODE_DEVELOPER, UserRole::CODE_PUBLISHER],
            ],
        ];
    }

    /**
     * Registers backend navigation items for this plugin.
     */
    public function registerNavigation(): array
    {
        return [
            'librairy' => [
                'label'       => 'gc7.librairy::lang.plugin.name',
                'url'         => Backend::url('gc7/librairy/Books'),
                'icon'        => 'icon-book',
                'permissions' => ['gc7.librairy.*'],
                'order'       => 500,
            ],
        ];
    }
}
