<?php

return [
    'plugin' => [
        'name' => 'Librairy',
        'description' => 'No description provided yet...',
    ],
    'permissions' => [
        'some_permission' => 'Some permission',
    ],
    'models' => [
        'general' => [
            'id' => 'ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ],
        'book' => [
            'label' => 'Book',
            'label_plural' => 'Books',
        ],
    ],
];
