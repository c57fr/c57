<?php

/**
 *
 *             Ce fichier est la propriété de C57.fr (ɔ)
 *
 *             Membre@c57.fr - 2019-2021
 *
 *             Et C57... C'est à VOUS !
 *
 *             Sérieusement, ce fichier source est sujet à la license MIT*.
 *             Mais je compte sur vous pour toujours chercher à l'améliorer et à votre tour, en faire profiter
 *             un max de monde grâce aux techniques offertes dans c57.fr.
 *
 *             @Bi1tô, & Bon code !
 *
 *              *: En gros...: Vous en faites ce que vous voulez !!!
 */

namespace GC7\Librairy\Updates;

use Winter\Storm\Database\Updates\Migration;
use Winter\Storm\Support\Facades\Schema;

class BuilderTableCreateGc7LibrairyBooks extends Migration {
	public function up() {
		Schema::create('gc7_librairy_books', function ($table) {
			$table->engine = 'InnoDB';
			$table->increments('id');
			$table->string('title', 255);
			$table->text('synopsis');
			$table->smallInteger('publication_year');
			$table->timestamps();
		});
	}

public function down() {
	Schema::dropIfExists('gc7_librairy_books');
}
}
