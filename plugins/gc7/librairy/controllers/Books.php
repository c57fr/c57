<?php namespace GC7\Librairy\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Books Backend Controller
 */
class Books extends Controller
{
    /**
     * @var array Behaviors that are implemented by this controller.
     */
    public $implement = [
        \Backend\Behaviors\FormController::class,
        \Backend\Behaviors\ListController::class,
    ];

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('GC7.Librairy', 'librairy', 'books');
    }
}
