-- --------------------------------------------------------
-- Hôte:                         127.0.0.1
-- Version du serveur:           5.7.33 - MySQL Community Server (GPL)
-- SE du serveur:                Win64
-- HeidiSQL Version:             11.2.0.6213
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Listage de la structure de la base pour aadminli_c57fr
CREATE DATABASE IF NOT EXISTS `aadminli_c57fr` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `aadminli_c57fr`;

-- Listage de la structure de la table aadminli_c57fr. backend_access_log
CREATE TABLE IF NOT EXISTS `backend_access_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `ip_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table aadminli_c57fr.backend_access_log : ~1 rows (environ)
/*!40000 ALTER TABLE `backend_access_log` DISABLE KEYS */;
INSERT INTO `backend_access_log` (`id`, `user_id`, `ip_address`, `created_at`, `updated_at`) VALUES
	(1, 2, '127.0.0.1', '2023-03-28 17:56:39', '2023-03-28 17:56:39');
/*!40000 ALTER TABLE `backend_access_log` ENABLE KEYS */;

-- Listage de la structure de la table aadminli_c57fr. backend_users
CREATE TABLE IF NOT EXISTS `backend_users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `login` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `activation_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `persist_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reset_password_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permissions` text COLLATE utf8mb4_unicode_ci,
  `is_activated` tinyint(1) NOT NULL DEFAULT '0',
  `role_id` int(10) unsigned DEFAULT NULL,
  `activated_at` timestamp NULL DEFAULT NULL,
  `last_login` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `login_unique` (`login`),
  UNIQUE KEY `email_unique` (`email`),
  KEY `act_code_index` (`activation_code`),
  KEY `reset_code_index` (`reset_password_code`),
  KEY `admin_role_index` (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table aadminli_c57fr.backend_users : ~2 rows (environ)
/*!40000 ALTER TABLE `backend_users` DISABLE KEYS */;
INSERT INTO `backend_users` (`id`, `first_name`, `last_name`, `login`, `email`, `password`, `activation_code`, `persist_code`, `reset_password_code`, `permissions`, `is_activated`, `role_id`, `activated_at`, `last_login`, `created_at`, `updated_at`, `deleted_at`, `is_superuser`) VALUES
	(1, 'Admin', 'Person', 'admin', 'admin@example.com', '$2y$10$H13bapNMKVVmdGqOk4qhXuN48OJ7kaHQ3bse8UksW.fWZh8BgZa/S', NULL, NULL, NULL, '', 1, 2, NULL, NULL, '2023-03-28 17:54:55', '2023-03-28 17:54:55', NULL, 1),
	(2, 'Moi', 'PERSO', 'C57', 'C57@c57.fr', '$2y$10$s9HmafUp.jkxlfwSKdRz4ujdwVGpHfjgmJsrVq1yz5B1XpAZOca6m', NULL, '$2y$10$y8eD5kfZjMGtzJT1Y.uk.unqZ0zTcOeLjNupC1L06MXuDG1QYRllS', NULL, NULL, 1, 2, NULL, '2023-03-28 17:56:39', '2023-03-28 17:55:04', '2023-03-28 17:56:39', NULL, 1);
/*!40000 ALTER TABLE `backend_users` ENABLE KEYS */;

-- Listage de la structure de la table aadminli_c57fr. backend_users_groups
CREATE TABLE IF NOT EXISTS `backend_users_groups` (
  `user_id` int(10) unsigned NOT NULL,
  `user_group_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`user_id`,`user_group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table aadminli_c57fr.backend_users_groups : ~0 rows (environ)
/*!40000 ALTER TABLE `backend_users_groups` DISABLE KEYS */;
INSERT INTO `backend_users_groups` (`user_id`, `user_group_id`) VALUES
	(1, 1);
/*!40000 ALTER TABLE `backend_users_groups` ENABLE KEYS */;

-- Listage de la structure de la table aadminli_c57fr. backend_user_groups
CREATE TABLE IF NOT EXISTS `backend_user_groups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `is_new_user_default` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_unique` (`name`),
  KEY `code_index` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table aadminli_c57fr.backend_user_groups : ~0 rows (environ)
/*!40000 ALTER TABLE `backend_user_groups` DISABLE KEYS */;
INSERT INTO `backend_user_groups` (`id`, `name`, `created_at`, `updated_at`, `code`, `description`, `is_new_user_default`) VALUES
	(1, 'Owners', '2023-03-28 17:54:55', '2023-03-28 17:54:55', 'owners', 'Default group for website owners.', 0);
/*!40000 ALTER TABLE `backend_user_groups` ENABLE KEYS */;

-- Listage de la structure de la table aadminli_c57fr. backend_user_preferences
CREATE TABLE IF NOT EXISTS `backend_user_preferences` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `namespace` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `group` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `item` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `user_item_index` (`user_id`,`namespace`,`group`,`item`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table aadminli_c57fr.backend_user_preferences : ~0 rows (environ)
/*!40000 ALTER TABLE `backend_user_preferences` DISABLE KEYS */;
/*!40000 ALTER TABLE `backend_user_preferences` ENABLE KEYS */;

-- Listage de la structure de la table aadminli_c57fr. backend_user_roles
CREATE TABLE IF NOT EXISTS `backend_user_roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `permissions` text COLLATE utf8mb4_unicode_ci,
  `is_system` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `role_unique` (`name`),
  KEY `role_code_index` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table aadminli_c57fr.backend_user_roles : ~2 rows (environ)
/*!40000 ALTER TABLE `backend_user_roles` DISABLE KEYS */;
INSERT INTO `backend_user_roles` (`id`, `name`, `code`, `description`, `permissions`, `is_system`, `created_at`, `updated_at`) VALUES
	(1, 'Publisher', 'publisher', 'Site editor with access to publishing tools.', '', 1, '2023-03-28 17:54:55', '2023-03-28 17:54:55'),
	(2, 'Developer', 'developer', 'Site administrator with access to developer tools.', '', 1, '2023-03-28 17:54:55', '2023-03-28 17:54:55');
/*!40000 ALTER TABLE `backend_user_roles` ENABLE KEYS */;

-- Listage de la structure de la table aadminli_c57fr. backend_user_throttle
CREATE TABLE IF NOT EXISTS `backend_user_throttle` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned DEFAULT NULL,
  `ip_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attempts` int(11) NOT NULL DEFAULT '0',
  `last_attempt_at` timestamp NULL DEFAULT NULL,
  `is_suspended` tinyint(1) NOT NULL DEFAULT '0',
  `suspended_at` timestamp NULL DEFAULT NULL,
  `is_banned` tinyint(1) NOT NULL DEFAULT '0',
  `banned_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `backend_user_throttle_user_id_index` (`user_id`),
  KEY `backend_user_throttle_ip_address_index` (`ip_address`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table aadminli_c57fr.backend_user_throttle : ~1 rows (environ)
/*!40000 ALTER TABLE `backend_user_throttle` DISABLE KEYS */;
INSERT INTO `backend_user_throttle` (`id`, `user_id`, `ip_address`, `attempts`, `last_attempt_at`, `is_suspended`, `suspended_at`, `is_banned`, `banned_at`) VALUES
	(1, 2, '127.0.0.1', 0, NULL, 0, NULL, 0, NULL);
/*!40000 ALTER TABLE `backend_user_throttle` ENABLE KEYS */;

-- Listage de la structure de la table aadminli_c57fr. cache
CREATE TABLE IF NOT EXISTS `cache` (
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `expiration` int(11) NOT NULL,
  UNIQUE KEY `cache_key_unique` (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table aadminli_c57fr.cache : ~0 rows (environ)
/*!40000 ALTER TABLE `cache` DISABLE KEYS */;
/*!40000 ALTER TABLE `cache` ENABLE KEYS */;

-- Listage de la structure de la table aadminli_c57fr. cms_theme_data
CREATE TABLE IF NOT EXISTS `cms_theme_data` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `theme` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `data` mediumtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cms_theme_data_theme_index` (`theme`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table aadminli_c57fr.cms_theme_data : ~1 rows (environ)
/*!40000 ALTER TABLE `cms_theme_data` DISABLE KEYS */;
INSERT INTO `cms_theme_data` (`id`, `theme`, `data`, `created_at`, `updated_at`) VALUES
	(1, 'c57', '{"site_name":"c57","site_color":"tomato \\/ cornsilk"}', '2023-03-28 17:56:52', '2023-03-28 17:56:52');
/*!40000 ALTER TABLE `cms_theme_data` ENABLE KEYS */;

-- Listage de la structure de la table aadminli_c57fr. cms_theme_logs
CREATE TABLE IF NOT EXISTS `cms_theme_logs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `theme` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `template` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `old_template` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `old_content` longtext COLLATE utf8mb4_unicode_ci,
  `user_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cms_theme_logs_type_index` (`type`),
  KEY `cms_theme_logs_theme_index` (`theme`),
  KEY `cms_theme_logs_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table aadminli_c57fr.cms_theme_logs : ~0 rows (environ)
/*!40000 ALTER TABLE `cms_theme_logs` DISABLE KEYS */;
/*!40000 ALTER TABLE `cms_theme_logs` ENABLE KEYS */;

-- Listage de la structure de la table aadminli_c57fr. cms_theme_templates
CREATE TABLE IF NOT EXISTS `cms_theme_templates` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `source` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `path` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_size` int(10) unsigned NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cms_theme_templates_source_index` (`source`),
  KEY `cms_theme_templates_path_index` (`path`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table aadminli_c57fr.cms_theme_templates : ~0 rows (environ)
/*!40000 ALTER TABLE `cms_theme_templates` DISABLE KEYS */;
/*!40000 ALTER TABLE `cms_theme_templates` ENABLE KEYS */;

-- Listage de la structure de la table aadminli_c57fr. deferred_bindings
CREATE TABLE IF NOT EXISTS `deferred_bindings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `master_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `master_field` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slave_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slave_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pivot_data` mediumtext COLLATE utf8mb4_unicode_ci,
  `session_key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_bind` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `deferred_bindings_master_type_index` (`master_type`),
  KEY `deferred_bindings_master_field_index` (`master_field`),
  KEY `deferred_bindings_slave_type_index` (`slave_type`),
  KEY `deferred_bindings_slave_id_index` (`slave_id`),
  KEY `deferred_bindings_session_key_index` (`session_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table aadminli_c57fr.deferred_bindings : ~0 rows (environ)
/*!40000 ALTER TABLE `deferred_bindings` DISABLE KEYS */;
/*!40000 ALTER TABLE `deferred_bindings` ENABLE KEYS */;

-- Listage de la structure de la table aadminli_c57fr. failed_jobs
CREATE TABLE IF NOT EXISTS `failed_jobs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci,
  `failed_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table aadminli_c57fr.failed_jobs : ~0 rows (environ)
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;

-- Listage de la structure de la table aadminli_c57fr. gc7_librairy_books
CREATE TABLE IF NOT EXISTS `gc7_librairy_books` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `synopsis` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `publication_year` smallint(6) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table aadminli_c57fr.gc7_librairy_books : ~0 rows (environ)
/*!40000 ALTER TABLE `gc7_librairy_books` DISABLE KEYS */;
/*!40000 ALTER TABLE `gc7_librairy_books` ENABLE KEYS */;

-- Listage de la structure de la table aadminli_c57fr. grcote7_books_
CREATE TABLE IF NOT EXISTS `grcote7_books_` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `published` tinyint(1) DEFAULT NULL,
  `owner_id` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table aadminli_c57fr.grcote7_books_ : ~3 rows (environ)
/*!40000 ALTER TABLE `grcote7_books_` DISABLE KEYS */;
INSERT INTO `grcote7_books_` (`id`, `title`, `description`, `slug`, `created_at`, `updated_at`, `published`, `owner_id`) VALUES
	(1, '20 000 lieues sous les mers', '<p>Un voyage extraordinaire...</p>', '20-000-lieues-sous-les-mers', '2023-03-28 17:55:03', '2023-03-28 17:55:03', 1, '1'),
	(2, 'Le Monde des A', '<p>SF de référence</p>', 'le-monde-des-a', '2023-03-28 17:55:03', '2023-03-28 17:55:03', 1, '2'),
	(3, 'De la Terre à la lune', '<p>Une autre voyage extraordinaire</p>', 'de-la-terre-a-la-lune', '2023-03-28 17:55:03', '2023-03-28 17:55:03', 1, '1');
/*!40000 ALTER TABLE `grcote7_books_` ENABLE KEYS */;

-- Listage de la structure de la table aadminli_c57fr. grcote7_books_owners
CREATE TABLE IF NOT EXISTS `grcote7_books_owners` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `firstname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `parr` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table aadminli_c57fr.grcote7_books_owners : ~5 rows (environ)
/*!40000 ALTER TABLE `grcote7_books_owners` DISABLE KEYS */;
INSERT INTO `grcote7_books_owners` (`id`, `firstname`, `lastname`, `slug`, `parr`) VALUES
	(1, 'Lionel', 'CÔTE', 'lionel-cote', 0),
	(2, 'Lio181', 'YAHOO', 'lio181-yahoo', 1),
	(3, 'Michel', 'COLUCCI', 'michel-colucci', 2),
	(4, 'Pierre', 'RICHARD', 'pierre-richard', 3),
	(5, 'Pierre', 'PALMADE', 'pierre-palmade', 3);
/*!40000 ALTER TABLE `grcote7_books_owners` ENABLE KEYS */;

-- Listage de la structure de la table aadminli_c57fr. grcote7_dptlist_dpts
CREATE TABLE IF NOT EXISTS `grcote7_dptlist_dpts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `num` int(10) unsigned NOT NULL,
  `nom` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table aadminli_c57fr.grcote7_dptlist_dpts : ~0 rows (environ)
/*!40000 ALTER TABLE `grcote7_dptlist_dpts` DISABLE KEYS */;
/*!40000 ALTER TABLE `grcote7_dptlist_dpts` ENABLE KEYS */;

-- Listage de la structure de la table aadminli_c57fr. grcote7_library_books
CREATE TABLE IF NOT EXISTS `grcote7_library_books` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `author` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `year` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `short_description` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table aadminli_c57fr.grcote7_library_books : ~0 rows (environ)
/*!40000 ALTER TABLE `grcote7_library_books` DISABLE KEYS */;
/*!40000 ALTER TABLE `grcote7_library_books` ENABLE KEYS */;

-- Listage de la structure de la table aadminli_c57fr. grcote7_movies_
CREATE TABLE IF NOT EXISTS `grcote7_movies_` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `year` int(11) DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `published` tinyint(1) DEFAULT NULL,
  `techteam` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table aadminli_c57fr.grcote7_movies_ : ~100 rows (environ)
/*!40000 ALTER TABLE `grcote7_movies_` DISABLE KEYS */;
INSERT INTO `grcote7_movies_` (`id`, `name`, `description`, `year`, `slug`, `created_at`, `published`, `techteam`) VALUES
	(1, 'Amet molestiae.', 'Nihil optio qui voluptatibus velit incidunt enim reprehenderit blanditiis. Molestias maiores a id fugit eos ullam. Eos autem reiciendis qui.', 1984, 'amet-molestiae', NULL, NULL, NULL),
	(2, 'Rem qui distinctio.', 'Qui sunt incidunt totam et veritatis voluptas. Dolor voluptatibus ut voluptates voluptas qui voluptatem aut. Delectus quidem repudiandae qui atque quia fuga consectetur. Eius omnis molestiae non voluptatibus.', 1996, 'rem-qui-distinctio', NULL, NULL, NULL),
	(3, 'Totam omnis et.', 'Enim quam expedita sunt quidem. Accusantium accusantium nesciunt pariatur enim facere ut nulla. Corporis nemo eaque rerum numquam.', 2016, 'totam-omnis-et', NULL, NULL, NULL),
	(4, 'Expedita placeat suscipit.', 'Facilis voluptatem et minima cumque voluptates deleniti. Rerum et culpa ipsa quia dolorem veniam. Ullam ut commodi est natus pariatur quibusdam est.', 2011, 'expedita-placeat-suscipit', NULL, NULL, NULL),
	(5, 'Porro ut.', 'Nulla illum natus vero quo numquam sed consequuntur. Et nesciunt et laboriosam nostrum totam deleniti quos inventore. Fugit in quia optio sit deserunt debitis. Quia itaque hic accusamus illum a.', 2016, 'porro-ut', NULL, NULL, NULL),
	(6, 'Consectetur quas fugit.', 'Placeat ut sed dolor ea similique. Libero voluptatum voluptas ut accusantium est. Tenetur distinctio ad assumenda numquam.', 1997, 'consectetur-quas-fugit', NULL, NULL, NULL),
	(7, 'Voluptas accusantium sint illo.', 'Sint accusamus neque et nam. Dolor laudantium et minima veniam et a molestias iusto. Id ipsam sed itaque repudiandae voluptatum qui. Ullam nesciunt quam ratione sapiente accusantium maiores.', 2022, 'voluptas-accusantium-sint-illo', NULL, NULL, NULL),
	(8, 'Commodi assumenda quas fugiat.', 'Earum qui distinctio delectus quos. Magni quo officia voluptatem molestias mollitia hic consequuntur rem. Qui aut commodi et et nostrum alias perspiciatis non.', 1995, 'commodi-assumenda-quas-fugiat', NULL, NULL, NULL),
	(9, 'Dolorem similique placeat aperiam.', 'Aut sit doloremque omnis voluptatem cumque. Illo sit impedit debitis et. Veritatis vel blanditiis inventore voluptatibus autem assumenda ad.', 2013, 'dolorem-similique-placeat-aperiam', NULL, NULL, NULL),
	(10, 'Qui sapiente vel omnis.', 'Sint qui voluptatem in nobis sed dolorum ut. Consequatur et et asperiores unde temporibus voluptatem voluptatem amet. Et natus nihil soluta est consequatur eos nam. Ut voluptas saepe quae repudiandae tenetur blanditiis consequuntur.', 2022, 'qui-sapiente-vel-omnis', NULL, NULL, NULL),
	(11, 'Ea voluptatum sit.', 'Consequatur soluta facere a fugiat qui quaerat. Nisi perspiciatis dolor eligendi ipsum est molestiae vitae libero. Voluptatum minima incidunt exercitationem sequi enim.', 2018, 'ea-voluptatum-sit', NULL, NULL, NULL),
	(12, 'Voluptatem et asperiores vel.', 'Ipsum eum quaerat sint ratione eveniet similique animi. Laborum temporibus totam amet totam molestiae. Sequi harum dolorem saepe voluptatibus sint nam quos enim. Consequatur rerum consequatur culpa voluptas architecto consequatur aliquid alias. Sit quos unde veniam consequuntur maiores blanditiis et soluta.', 2013, 'voluptatem-et-asperiores-vel', NULL, NULL, NULL),
	(13, 'Qui soluta iusto eum.', 'Nihil iure nihil soluta itaque officia et. Cupiditate corporis officiis similique voluptate alias hic quidem. Libero nemo quo omnis vel nihil.', 1982, 'qui-soluta-iusto-eum', NULL, NULL, NULL),
	(14, 'Accusamus similique accusantium voluptatem.', 'Sequi quisquam dolorum tempora alias earum molestiae vero. Unde soluta sed cumque quia quas aspernatur tempora. Non et distinctio ut itaque.', 1987, 'accusamus-similique-accusantium-voluptatem', NULL, NULL, NULL),
	(15, 'Occaecati necessitatibus.', 'Tempore voluptas et quae aut cupiditate. Quia eius est et aut libero. Vel est voluptate atque neque adipisci ratione dolor.', 1998, 'occaecati-necessitatibus', NULL, NULL, NULL),
	(16, 'Ipsum cumque non pariatur.', 'Quidem id rerum placeat eveniet dolor quia repellat. Nemo quia odio et aperiam dolorum officia. Ut esse eos sunt dolores incidunt est autem ut. Saepe doloremque repellendus ut sapiente nobis aliquam quae ratione.', 1972, 'ipsum-cumque-non-pariatur', NULL, NULL, NULL),
	(17, 'Enim doloremque eaque.', 'Labore occaecati ad illo qui quod ut. Cupiditate atque in repellendus sit maxime possimus.', 1998, 'enim-doloremque-eaque', NULL, NULL, NULL),
	(18, 'Consequatur dolor cupiditate.', 'Mollitia eveniet veritatis dolor sed porro sit nihil. Earum consectetur sequi sunt nobis. Et sed architecto fugiat sed voluptatum. Et rem modi deserunt quidem. Et id nam sunt provident debitis.', 1971, 'consequatur-dolor-cupiditate', NULL, NULL, NULL),
	(19, 'Voluptates molestiae non nihil.', 'Porro consequuntur veniam expedita tempora. Eligendi qui quas fugit explicabo. Totam voluptas quae totam porro. Reprehenderit quod minus quos deleniti explicabo et eum nostrum.', 1991, 'voluptates-molestiae-non-nihil', NULL, NULL, NULL),
	(20, 'Beatae dignissimos velit autem.', 'Nihil nostrum ab non dolor aut asperiores. Qui eum expedita pariatur libero. Ut autem omnis saepe officia sit maiores.', 1972, 'beatae-dignissimos-velit-autem', NULL, NULL, NULL),
	(21, 'Ut est est.', 'Et et ut natus. Dignissimos cum quo sed quam. Rerum at aliquid ea minus dolores non quia.', 2015, 'ut-est-est', NULL, NULL, NULL),
	(22, 'Porro quo eos vero molestias.', 'Et enim rem consectetur non ut cumque modi. Et dolore eaque eos facere ut libero possimus. Autem minima qui in voluptatum.', 1979, 'porro-quo-eos-vero-molestias', NULL, NULL, NULL),
	(23, 'Asperiores earum voluptas earum.', 'Qui temporibus sit velit neque. Quis nemo non impedit vel maiores soluta fugit. Magni voluptate qui voluptatum.', 2010, 'asperiores-earum-voluptas-earum', NULL, NULL, NULL),
	(24, 'Eos sed sunt ut.', 'In sequi molestias ut est. Ducimus quae veritatis perspiciatis voluptate quam voluptatem autem aut. Voluptas temporibus a suscipit architecto temporibus animi rerum.', 1978, 'eos-sed-sunt-ut', NULL, NULL, NULL),
	(25, 'Enim consequatur porro maiores.', 'Nobis quaerat deleniti eum et eos dolore. Cupiditate suscipit omnis ad quia temporibus fugiat ab. Assumenda ex eligendi harum omnis.', 1986, 'enim-consequatur-porro-maiores', NULL, NULL, NULL),
	(26, 'Autem eum dolorem.', 'Aliquid esse quia quaerat veritatis dolores iusto sunt. Voluptas aperiam modi ut voluptas beatae ut. Veniam et sapiente saepe sint. Eum excepturi sed vero.', 1999, 'autem-eum-dolorem', NULL, NULL, NULL),
	(27, 'Aut rerum molestiae nihil.', 'Dolore autem sit enim culpa neque itaque ut. Recusandae incidunt et aut autem. Doloribus dicta pariatur fugit illum minus sunt.', 2011, 'aut-rerum-molestiae-nihil', NULL, NULL, NULL),
	(28, 'Suscipit est quae est.', 'Unde eveniet maxime architecto id labore nisi quis itaque. Tempore soluta nihil voluptatem non nisi. Quasi a in iusto ipsa veritatis fuga necessitatibus. In ab quisquam commodi beatae vel commodi omnis.', 2010, 'suscipit-est-quae-est', NULL, NULL, NULL),
	(29, 'Explicabo beatae blanditiis.', 'Iusto nemo non est. Et dolorum exercitationem aut cupiditate omnis asperiores. Cupiditate quis tempora est laboriosam.', 2001, 'explicabo-beatae-blanditiis', NULL, NULL, NULL),
	(30, 'Voluptas et natus quam.', 'Qui provident et non modi. Architecto amet assumenda aliquid. Magnam natus dolores voluptas officiis voluptas itaque corporis.', 1975, 'voluptas-et-natus-quam', NULL, NULL, NULL),
	(31, 'A non sint.', 'Harum eos placeat aut quibusdam et rerum. Rerum unde earum odit vel hic. In et rem fugiat aut est blanditiis. Tempore qui nam nisi totam soluta omnis a tenetur.', 1985, 'a-non-sint', NULL, NULL, NULL),
	(32, 'Facere reprehenderit et.', 'Et numquam dolore rerum soluta ut natus et. Voluptas suscipit non fugit unde. Et ut qui ea quo asperiores voluptas. Atque eaque amet voluptas vero vero repellendus.', 2020, 'facere-reprehenderit-et', NULL, NULL, NULL),
	(33, 'Et voluptas quas.', 'Porro id magnam aliquam quisquam est sit rem. Consequatur ad ipsam corporis quas. Reiciendis nesciunt debitis sunt dolores dolorem.', 2013, 'et-voluptas-quas', NULL, NULL, NULL),
	(34, 'At dolores.', 'Qui deserunt praesentium dolore corrupti delectus et. Omnis quis aperiam vitae quis tempora. Eaque necessitatibus mollitia eius maxime.', 2006, 'at-dolores', NULL, NULL, NULL),
	(35, 'Labore amet nesciunt.', 'Hic esse exercitationem voluptatem. Amet delectus modi est nihil. Est in voluptatem voluptatem. Voluptatibus id facere asperiores optio facere voluptate quod voluptas.', 2015, 'labore-amet-nesciunt', NULL, NULL, NULL),
	(36, 'Temporibus sequi quia.', 'Dolorem facere sint quaerat odio recusandae dignissimos earum. Veritatis facilis nam temporibus natus eveniet temporibus. Id debitis voluptatum culpa sed necessitatibus. Sed eos amet veniam temporibus ut aut ut.', 2009, 'temporibus-sequi-quia', NULL, NULL, NULL),
	(37, 'Exercitationem et voluptate numquam.', 'Non iste accusamus ut. Reprehenderit corrupti necessitatibus impedit rerum ducimus. Voluptatem aut soluta et sed quaerat voluptates laudantium.', 1992, 'exercitationem-et-voluptate-numquam', NULL, NULL, NULL),
	(38, 'Vel quo molestias.', 'Dolores reprehenderit itaque ut quia. Dicta corrupti ab sapiente a facilis.', 1991, 'vel-quo-molestias', NULL, NULL, NULL),
	(39, 'Voluptates consectetur cupiditate.', 'Voluptas et ab et ut nemo. Eius nihil nemo consequatur et tenetur perspiciatis nostrum. Sed ad nihil fugiat sunt earum sunt.', 1988, 'voluptates-consectetur-cupiditate', NULL, NULL, NULL),
	(40, 'Nulla tempore accusantium consequatur.', 'Officia enim velit pariatur nobis qui. Provident quibusdam voluptas est dicta ullam. Ratione nesciunt cupiditate eos assumenda aut eius sequi.', 1995, 'nulla-tempore-accusantium-consequatur', NULL, NULL, NULL),
	(41, 'Nisi corporis.', 'Vel itaque quibusdam qui ut. Ea exercitationem exercitationem cum inventore cumque. Perferendis cupiditate accusantium quasi consequuntur sunt porro. Dolores accusamus blanditiis facere repellendus aut quisquam velit.', 1987, 'nisi-corporis', NULL, NULL, NULL),
	(42, 'Fuga et.', 'Consequatur et fugiat voluptatum. Beatae nemo a magnam est placeat aliquam. Voluptas ut necessitatibus rerum sit. Explicabo fugit et rem eos magni rerum aut.', 2006, 'fuga-et', NULL, NULL, NULL),
	(43, 'Voluptatem nihil.', 'Suscipit eum corporis repellat eos rerum. Tempora facilis et provident asperiores omnis. Vel tempora facere voluptatum ut quisquam inventore quis rerum.', 2005, 'voluptatem-nihil', NULL, NULL, NULL),
	(44, 'Consequatur in temporibus quis.', 'Esse sit veritatis corrupti enim voluptatem fugiat voluptatem. Corporis voluptas velit libero quis et sunt.', 2006, 'consequatur-in-temporibus-quis', NULL, NULL, NULL),
	(45, 'Officiis deleniti dolorem.', 'Quos deleniti hic aut nobis harum quisquam quo perspiciatis. Amet mollitia enim minus quae odio. Doloribus cumque ut praesentium qui quae et voluptatem. Ut velit totam voluptas vel vero ut.', 2007, 'officiis-deleniti-dolorem', NULL, NULL, NULL),
	(46, 'Asperiores dolor suscipit.', 'Ut voluptas aliquam inventore explicabo. Quas iure veritatis quam quod doloremque ut. Non necessitatibus dolorem rem doloremque eum.', 1992, 'asperiores-dolor-suscipit', NULL, NULL, NULL),
	(47, 'Fugit asperiores quia.', 'Aut illum unde nihil totam cumque sunt rerum. Quod qui ducimus dolorum eligendi rerum unde aut. Id id illum possimus qui.', 2009, 'fugit-asperiores-quia', NULL, NULL, NULL),
	(48, 'Quas soluta.', 'Voluptatibus molestiae temporibus consequatur. Enim in odio vel inventore doloribus est perferendis. Et minus quas rerum consectetur. Nemo enim molestias accusantium corrupti consequatur odio.', 1983, 'quas-soluta', NULL, NULL, NULL),
	(49, 'Aut accusantium exercitationem.', 'Iusto nihil debitis nihil vero aut ab ullam odit. Distinctio voluptatem incidunt vel similique. Et laboriosam rerum illo possimus atque quos quasi.', 1974, 'aut-accusantium-exercitationem', NULL, NULL, NULL),
	(50, 'Labore qui et officiis.', 'Perferendis quod cupiditate possimus quibusdam nisi non amet. Qui possimus delectus sequi harum ipsum voluptatem quis. Deleniti quia modi sed.', 1974, 'labore-qui-et-officiis', NULL, NULL, NULL),
	(51, 'Labore voluptatem molestiae nulla.', 'Quam iste sint consequatur perspiciatis. Qui enim repellat ut eveniet et qui autem. Id ullam vero quidem omnis rerum.', 1977, 'labore-voluptatem-molestiae-nulla', NULL, NULL, NULL),
	(52, 'Ut aspernatur dolores aut.', 'Explicabo et vel est molestias quos voluptatem ratione voluptatem. Minus omnis in consequatur maiores distinctio ut. Ipsa qui dolore rerum. Rerum cupiditate explicabo sit quam et.', 1980, 'ut-aspernatur-dolores-aut', NULL, NULL, NULL),
	(53, 'Harum veniam aut delectus.', 'Unde deleniti quo ducimus dolor. Non non nemo ullam voluptatem maxime consequatur et. Corporis molestias eveniet quasi et. Non aliquid assumenda expedita.', 1977, 'harum-veniam-aut-delectus', NULL, NULL, NULL),
	(54, 'Sapiente vero molestias quia non.', 'Dolores nihil nulla facilis ut ex qui quae. Et mollitia rerum omnis autem ab vel. Quis voluptas voluptates dolorum.', 2020, 'sapiente-vero-molestias-quia-non', NULL, NULL, NULL),
	(55, 'Voluptatibus vero voluptatem.', 'Est voluptatem voluptatem quae sed temporibus qui explicabo. Autem ab ut et voluptates provident aut aut.', 2017, 'voluptatibus-vero-voluptatem', NULL, NULL, NULL),
	(56, 'Aut nihil repellendus.', 'Mollitia voluptatem a officiis ut. Itaque et id et. Voluptatibus et id optio sit sapiente.', 1983, 'aut-nihil-repellendus', NULL, NULL, NULL),
	(57, 'Velit animi aut sint.', 'Rerum velit autem rerum assumenda asperiores repudiandae iste. Itaque ut et assumenda maxime sapiente voluptatum tempore. Consequatur quaerat accusantium harum ex quisquam ut. Non et occaecati ut inventore et ea voluptate.', 1987, 'velit-animi-aut-sint', NULL, NULL, NULL),
	(58, 'Esse neque ut.', 'Eveniet quia tenetur placeat voluptatum at odit. Distinctio dolore nam et impedit vitae magni. Enim autem saepe veritatis vel esse.', 1999, 'esse-neque-ut', NULL, NULL, NULL),
	(59, 'Rerum aut illo exercitationem.', 'Nobis maxime cum consequatur ad. Et velit dolorum accusantium aliquid amet possimus eius. Ut magnam omnis qui enim sunt nobis.', 1983, 'rerum-aut-illo-exercitationem', NULL, NULL, NULL),
	(60, 'Vero et ut.', 'Minus dolorum mollitia voluptas. Voluptates et eveniet voluptas dolorem nobis et. Magnam ducimus et sit error et. Blanditiis corrupti id sit quis tenetur.', 1999, 'vero-et-ut', NULL, NULL, NULL),
	(61, 'Ipsam omnis vel.', 'Veniam qui labore dolore voluptatem quas et labore tenetur. Nemo molestiae voluptates quo. Voluptatem inventore qui perferendis est quia aliquam. Vel et id consectetur rem velit voluptatem consectetur.', 1998, 'ipsam-omnis-vel', NULL, NULL, NULL),
	(62, 'Delectus sunt quia.', 'At vel dicta neque a. Maxime corporis dolor quibusdam fugit dolorem. Magni labore et nihil amet dignissimos dolore unde totam. Amet ipsam modi delectus voluptatem delectus. Et reprehenderit ut ut at.', 1979, 'delectus-sunt-quia', NULL, NULL, NULL),
	(63, 'Iste ea voluptas.', 'Animi amet in alias qui est non unde. Sed ut in quo sed fugit quas. Sed quod repudiandae alias consequuntur. Rerum ad beatae voluptatem accusantium ducimus omnis.', 1993, 'iste-ea-voluptas', NULL, NULL, NULL),
	(64, 'Dolorem et non.', 'Deserunt quisquam eum corporis corporis. Ullam est et soluta quae omnis tempora. Dolorum et voluptate aut repellat ut.', 1972, 'dolorem-et-non', NULL, NULL, NULL),
	(65, 'Quasi reiciendis.', 'Laudantium consequatur maiores eum doloremque voluptatem. In velit cupiditate doloremque et nobis est. Doloribus qui officia dolor repellat qui ratione est.', 2000, 'quasi-reiciendis', NULL, NULL, NULL),
	(66, 'Consequuntur at ut autem.', 'Aperiam mollitia aut recusandae beatae inventore ab consequatur autem. Excepturi dignissimos consequatur sed et quam eaque nemo. Ducimus minima dignissimos totam qui non commodi ea. Veritatis assumenda sunt qui ut atque.', 1995, 'consequuntur-at-ut-autem', NULL, NULL, NULL),
	(67, 'Perspiciatis debitis aliquid reprehenderit dolor.', 'Quidem nihil est eum at. Laboriosam nihil dolor mollitia ea fuga maxime.', 1987, 'perspiciatis-debitis-aliquid-reprehenderit-dolor', NULL, NULL, NULL),
	(68, 'Voluptatum tempora vel.', 'Voluptatibus impedit vel mollitia sed. Dolor ipsum suscipit voluptatem. Vel eos quidem ullam distinctio minima quibusdam similique dolorum.', 1972, 'voluptatum-tempora-vel', NULL, NULL, NULL),
	(69, 'Qui odio sequi rerum.', 'Tempore sint reprehenderit aliquam. Ut veniam neque voluptas voluptas debitis est quidem. Rerum quisquam vero excepturi quia quas.', 1994, 'qui-odio-sequi-rerum', NULL, NULL, NULL),
	(70, 'Iste non atque minima.', 'Enim unde provident beatae deleniti. Dolorem minima placeat asperiores dolore recusandae quia. Repellat eum itaque voluptates minus consequuntur similique velit eum. Doloribus nemo excepturi beatae vitae ratione.', 2021, 'iste-non-atque-minima', NULL, NULL, NULL),
	(71, 'Facilis exercitationem iure.', 'Sunt ad commodi id. Et quia iure esse minus maxime quos. Porro occaecati ut nobis aut vel. Quo temporibus quibusdam et earum perferendis pariatur. Quis libero debitis fuga iste et aut.', 1986, 'facilis-exercitationem-iure', NULL, NULL, NULL),
	(72, 'Quae reprehenderit consectetur.', 'In facilis officia recusandae et non aspernatur soluta. Dolorum occaecati voluptate impedit consequatur. Id rerum voluptatem quibusdam.', 2022, 'quae-reprehenderit-consectetur', NULL, NULL, NULL),
	(73, 'Eos corrupti odit amet.', 'Dolores magnam nisi omnis vel. Ea porro magni aut optio. Eligendi iusto alias autem consequuntur amet eum.', 2005, 'eos-corrupti-odit-amet', NULL, NULL, NULL),
	(74, 'Aut eum laborum.', 'Dolor illo ex tempore id. Est natus nihil distinctio. Qui laborum repellat officiis ipsa dicta. Numquam omnis quis excepturi sed.', 1974, 'aut-eum-laborum', NULL, NULL, NULL),
	(75, 'Non incidunt iste.', 'Non eum qui quia cupiditate. Sed consequatur vel quis modi praesentium. Ipsum cupiditate dolorum laborum sint dolor perferendis tenetur.', 2013, 'non-incidunt-iste', NULL, NULL, NULL),
	(76, 'Sed voluptatem ab quia.', 'Labore quam quia et error. Quaerat dolor dolore adipisci soluta est voluptatibus. Nisi facere voluptas voluptas sit ab et nam vel.', 2002, 'sed-voluptatem-ab-quia', NULL, NULL, NULL),
	(77, 'A et ut dolor.', 'Provident quia voluptas est unde consequatur nihil rerum in. Deserunt eum non dolorem nulla unde aliquid. Ut ratione et officia et sunt.', 2014, 'a-et-ut-dolor', NULL, NULL, NULL),
	(78, 'Sit dicta quaerat.', 'Iste quo qui animi beatae ut. Rerum dignissimos hic quidem. Dignissimos ipsam atque velit occaecati eos optio. Autem dolor dolorem et sed officiis. Aut soluta sint est beatae amet earum.', 2000, 'sit-dicta-quaerat', NULL, NULL, NULL),
	(79, 'Ipsam et fugit.', 'Pariatur natus nulla ab rerum quisquam qui velit. Modi molestiae dolores veritatis nihil sit numquam quos. Praesentium et neque ut officiis voluptates ex. Explicabo expedita voluptate cumque exercitationem voluptatum.', 2002, 'ipsam-et-fugit', NULL, NULL, NULL),
	(80, 'Explicabo ipsam voluptates.', 'Accusamus qui vero ad repellendus. Magnam quo veritatis itaque explicabo a. Saepe ut et id ut dignissimos.', 2006, 'explicabo-ipsam-voluptates', NULL, NULL, NULL),
	(81, 'Magni eos in.', 'Dolores nobis ducimus dolores porro ratione blanditiis. Blanditiis quam qui blanditiis porro.', 1976, 'magni-eos-in', NULL, NULL, NULL),
	(82, 'Porro ea a consequuntur autem.', 'Animi consequatur et sit saepe repudiandae iure. Nobis in ipsa ut ut in exercitationem. Qui est ratione possimus non nulla nesciunt. Iste quo et modi architecto.', 1998, 'porro-ea-a-consequuntur-autem', NULL, NULL, NULL),
	(83, 'Quidem autem omnis.', 'Reprehenderit excepturi quas iure corrupti harum id sint. Occaecati adipisci ut aut qui voluptatum.', 2016, 'quidem-autem-omnis', NULL, NULL, NULL),
	(84, 'Dicta fugiat praesentium.', 'Quia nihil quae est qui ex. Assumenda beatae est deserunt vel. Non expedita reprehenderit est maiores accusamus quo. Magnam ipsam magni provident animi vel voluptatem ab perspiciatis.', 1970, 'dicta-fugiat-praesentium', NULL, NULL, NULL),
	(85, 'Tenetur fugiat dicta.', 'Suscipit ut cum quis ea rerum. Vel odit odit molestias quae ullam corrupti. Vero atque consectetur quae id. Suscipit eius earum quam reiciendis accusamus dolorem rerum.', 1983, 'tenetur-fugiat-dicta', NULL, NULL, NULL),
	(86, 'A et soluta.', 'Dolores ad et neque nobis consequuntur consequuntur rerum. Laboriosam ad provident ipsam sit. Voluptate voluptatem dolorem perspiciatis distinctio rerum.', 2015, 'a-et-soluta', NULL, NULL, NULL),
	(87, 'Inventore iste in ea.', 'Dolore error ut officia ea ut. Et consequatur qui cupiditate laboriosam vero quasi. Voluptas optio aperiam officia et beatae voluptatem nesciunt inventore. Voluptate explicabo molestiae velit totam fugit.', 1976, 'inventore-iste-in-ea', NULL, NULL, NULL),
	(88, 'Vitae sunt corporis tempore.', 'Possimus nobis vitae esse temporibus rerum minus rerum. Architecto porro nemo sapiente ut eum eligendi atque. Ea corporis amet mollitia iusto veritatis. Pariatur autem quis voluptates odio quidem eos beatae est. Sapiente et sit ab ab similique quis ullam.', 1983, 'vitae-sunt-corporis-tempore', NULL, NULL, NULL),
	(89, 'Quis ut ut voluptatem.', 'Sed nesciunt ut doloribus. Sed dolorem modi architecto deserunt sunt. Hic doloremque quia quam soluta exercitationem expedita.', 1973, 'quis-ut-ut-voluptatem', NULL, NULL, NULL),
	(90, 'Quasi repellat molestias.', 'Esse ratione deserunt quaerat quos sequi sunt. Et non quo quos quia. Dolorem voluptatem illum iure perferendis accusantium.', 2010, 'quasi-repellat-molestias', NULL, NULL, NULL),
	(91, 'Ut asperiores dolor ea.', 'Sunt rerum animi sit laudantium qui laboriosam laborum. Repudiandae sit qui impedit voluptatem quis. Voluptates minima voluptas est.', 2007, 'ut-asperiores-dolor-ea', NULL, NULL, NULL),
	(92, 'Vel reprehenderit minus.', 'Voluptas nemo repellendus ipsa deserunt quis ut. Maxime eaque vel pariatur quia eius. Adipisci ut accusantium adipisci nulla temporibus beatae dolorem. Quis non incidunt ea blanditiis quo sunt numquam.', 1988, 'vel-reprehenderit-minus', NULL, NULL, NULL),
	(93, 'Dolorum sunt molestiae qui.', 'Ut eligendi dolorem quia esse. Mollitia harum alias itaque. Voluptatem minus aperiam voluptas non.', 2007, 'dolorum-sunt-molestiae-qui', NULL, NULL, NULL),
	(94, 'Recusandae inventore dicta tempore.', 'Vero consequatur rerum aut voluptatem omnis. Voluptatem repellendus sit dolor. Et rerum voluptatem qui et id itaque animi et.', 1978, 'recusandae-inventore-dicta-tempore', NULL, NULL, NULL),
	(95, 'Magni odit.', 'Praesentium nulla cumque blanditiis hic nostrum consequatur occaecati. Ea autem magnam qui velit qui delectus eum. Quia blanditiis dolores eveniet est facilis et culpa possimus. Architecto numquam laborum vero in quia asperiores.', 1986, 'magni-odit', NULL, NULL, NULL),
	(96, 'Hic accusantium ut ipsum.', 'Molestiae aliquid accusantium soluta quas. Dolores fuga quaerat et aliquam eligendi quaerat magni. Commodi molestiae consequatur nemo possimus voluptas. Tenetur totam delectus ducimus quo.', 1997, 'hic-accusantium-ut-ipsum', NULL, NULL, NULL),
	(97, 'Placeat architecto consectetur quidem.', 'Dolores omnis commodi ut odio. Voluptas et fugit cupiditate ad nesciunt rerum qui. Nisi reiciendis et non velit suscipit laboriosam.', 1995, 'placeat-architecto-consectetur-quidem', NULL, NULL, NULL),
	(98, 'Minus omnis repellendus dolor.', 'Similique est repellat vero tempore eius omnis nobis facilis. Perspiciatis velit voluptatibus itaque quia est facere voluptas. Consectetur perspiciatis eum nostrum.', 2007, 'minus-omnis-repellendus-dolor', NULL, NULL, NULL),
	(99, 'Et enim.', 'Rem omnis et eos sunt vitae quae dolorum. Omnis dolor expedita placeat velit. Voluptas consequatur omnis aut occaecati porro error. Laudantium iure praesentium consequuntur maiores recusandae sapiente consequuntur possimus.', 2013, 'et-enim', NULL, NULL, NULL),
	(100, 'Eum perferendis temporibus architecto.', 'Reprehenderit quis sed laboriosam quia sint aut sunt. Aliquam hic iusto ipsam consequatur rem. Fugiat iusto quod qui delectus id blanditiis. Distinctio occaecati esse eum eum perferendis.', 1975, 'eum-perferendis-temporibus-architecto', NULL, NULL, NULL);
/*!40000 ALTER TABLE `grcote7_movies_` ENABLE KEYS */;

-- Listage de la structure de la table aadminli_c57fr. grcote7_movies_actors
CREATE TABLE IF NOT EXISTS `grcote7_movies_actors` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lastname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table aadminli_c57fr.grcote7_movies_actors : ~0 rows (environ)
/*!40000 ALTER TABLE `grcote7_movies_actors` DISABLE KEYS */;
/*!40000 ALTER TABLE `grcote7_movies_actors` ENABLE KEYS */;

-- Listage de la structure de la table aadminli_c57fr. grcote7_movies_actors_movies
CREATE TABLE IF NOT EXISTS `grcote7_movies_actors_movies` (
  `actor_id` int(10) unsigned NOT NULL,
  `movie_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`actor_id`,`movie_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table aadminli_c57fr.grcote7_movies_actors_movies : ~0 rows (environ)
/*!40000 ALTER TABLE `grcote7_movies_actors_movies` DISABLE KEYS */;
/*!40000 ALTER TABLE `grcote7_movies_actors_movies` ENABLE KEYS */;

-- Listage de la structure de la table aadminli_c57fr. grcote7_movies_genres
CREATE TABLE IF NOT EXISTS `grcote7_movies_genres` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `genre_title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table aadminli_c57fr.grcote7_movies_genres : ~0 rows (environ)
/*!40000 ALTER TABLE `grcote7_movies_genres` DISABLE KEYS */;
/*!40000 ALTER TABLE `grcote7_movies_genres` ENABLE KEYS */;

-- Listage de la structure de la table aadminli_c57fr. grcote7_movies_movies_genres
CREATE TABLE IF NOT EXISTS `grcote7_movies_movies_genres` (
  `movie_id` int(11) NOT NULL,
  `genre_id` int(11) NOT NULL,
  PRIMARY KEY (`movie_id`,`genre_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table aadminli_c57fr.grcote7_movies_movies_genres : ~0 rows (environ)
/*!40000 ALTER TABLE `grcote7_movies_movies_genres` DISABLE KEYS */;
/*!40000 ALTER TABLE `grcote7_movies_movies_genres` ENABLE KEYS */;

-- Listage de la structure de la table aadminli_c57fr. grcote7_movies_tags
CREATE TABLE IF NOT EXISTS `grcote7_movies_tags` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table aadminli_c57fr.grcote7_movies_tags : ~0 rows (environ)
/*!40000 ALTER TABLE `grcote7_movies_tags` DISABLE KEYS */;
/*!40000 ALTER TABLE `grcote7_movies_tags` ENABLE KEYS */;

-- Listage de la structure de la table aadminli_c57fr. grcote7_movies_tags_movies
CREATE TABLE IF NOT EXISTS `grcote7_movies_tags_movies` (
  `movie_id` int(10) unsigned NOT NULL,
  `tag_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`movie_id`,`tag_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table aadminli_c57fr.grcote7_movies_tags_movies : ~0 rows (environ)
/*!40000 ALTER TABLE `grcote7_movies_tags_movies` DISABLE KEYS */;
/*!40000 ALTER TABLE `grcote7_movies_tags_movies` ENABLE KEYS */;

-- Listage de la structure de la table aadminli_c57fr. grcote7_profile_profiles
CREATE TABLE IF NOT EXISTS `grcote7_profile_profiles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `pseudo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parr` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sexe` enum('Inconnu','H','F') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Inconnu',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `grcote7_profile_profiles_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table aadminli_c57fr.grcote7_profile_profiles : ~0 rows (environ)
/*!40000 ALTER TABLE `grcote7_profile_profiles` DISABLE KEYS */;
/*!40000 ALTER TABLE `grcote7_profile_profiles` ENABLE KEYS */;

-- Listage de la structure de la table aadminli_c57fr. grcote7_tutos_categories
CREATE TABLE IF NOT EXISTS `grcote7_tutos_categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table aadminli_c57fr.grcote7_tutos_categories : ~0 rows (environ)
/*!40000 ALTER TABLE `grcote7_tutos_categories` DISABLE KEYS */;
/*!40000 ALTER TABLE `grcote7_tutos_categories` ENABLE KEYS */;

-- Listage de la structure de la table aadminli_c57fr. grcote7_tutos_courses
CREATE TABLE IF NOT EXISTS `grcote7_tutos_courses` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `duration` time DEFAULT NULL,
  `tuto_id` int(11) DEFAULT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `notes` text COLLATE utf8mb4_unicode_ci,
  `mastery` smallint(6) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table aadminli_c57fr.grcote7_tutos_courses : ~0 rows (environ)
/*!40000 ALTER TABLE `grcote7_tutos_courses` DISABLE KEYS */;
/*!40000 ALTER TABLE `grcote7_tutos_courses` ENABLE KEYS */;

-- Listage de la structure de la table aadminli_c57fr. grcote7_tutos_tutos
CREATE TABLE IF NOT EXISTS `grcote7_tutos_tutos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `author` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `slug` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table aadminli_c57fr.grcote7_tutos_tutos : ~0 rows (environ)
/*!40000 ALTER TABLE `grcote7_tutos_tutos` DISABLE KEYS */;
/*!40000 ALTER TABLE `grcote7_tutos_tutos` ENABLE KEYS */;

-- Listage de la structure de la table aadminli_c57fr. grcote7_tutos_tutos_categories
CREATE TABLE IF NOT EXISTS `grcote7_tutos_tutos_categories` (
  `tuto_id` int(11) NOT NULL,
  `categorie_id` int(11) NOT NULL,
  PRIMARY KEY (`tuto_id`,`categorie_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table aadminli_c57fr.grcote7_tutos_tutos_categories : ~0 rows (environ)
/*!40000 ALTER TABLE `grcote7_tutos_tutos_categories` DISABLE KEYS */;
/*!40000 ALTER TABLE `grcote7_tutos_tutos_categories` ENABLE KEYS */;

-- Listage de la structure de la table aadminli_c57fr. jobs
CREATE TABLE IF NOT EXISTS `jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `queue` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `attempts` tinyint(3) unsigned NOT NULL,
  `reserved_at` int(10) unsigned DEFAULT NULL,
  `available_at` int(10) unsigned NOT NULL,
  `created_at` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `jobs_queue_reserved_at_index` (`queue`,`reserved_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table aadminli_c57fr.jobs : ~0 rows (environ)
/*!40000 ALTER TABLE `jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `jobs` ENABLE KEYS */;

-- Listage de la structure de la table aadminli_c57fr. migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table aadminli_c57fr.migrations : ~42 rows (environ)
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(1, '2013_10_01_000001_Db_Deferred_Bindings', 1),
	(2, '2013_10_01_000002_Db_System_Files', 1),
	(3, '2013_10_01_000003_Db_System_Plugin_Versions', 1),
	(4, '2013_10_01_000004_Db_System_Plugin_History', 1),
	(5, '2013_10_01_000005_Db_System_Settings', 1),
	(6, '2013_10_01_000006_Db_System_Parameters', 1),
	(7, '2013_10_01_000007_Db_System_Add_Disabled_Flag', 1),
	(8, '2013_10_01_000008_Db_System_Mail_Templates', 1),
	(9, '2013_10_01_000009_Db_System_Mail_Layouts', 1),
	(10, '2014_10_01_000010_Db_Jobs', 1),
	(11, '2014_10_01_000011_Db_System_Event_Logs', 1),
	(12, '2014_10_01_000012_Db_System_Request_Logs', 1),
	(13, '2014_10_01_000013_Db_System_Sessions', 1),
	(14, '2015_10_01_000014_Db_System_Mail_Layout_Rename', 1),
	(15, '2015_10_01_000015_Db_System_Add_Frozen_Flag', 1),
	(16, '2015_10_01_000016_Db_Cache', 1),
	(17, '2015_10_01_000017_Db_System_Revisions', 1),
	(18, '2015_10_01_000018_Db_FailedJobs', 1),
	(19, '2016_10_01_000019_Db_System_Plugin_History_Detail_Text', 1),
	(20, '2016_10_01_000020_Db_System_Timestamp_Fix', 1),
	(21, '2017_08_04_121309_Db_Deferred_Bindings_Add_Index_Session', 1),
	(22, '2017_10_01_000021_Db_System_Sessions_Update', 1),
	(23, '2017_10_01_000022_Db_Jobs_FailedJobs_Update', 1),
	(24, '2017_10_01_000023_Db_System_Mail_Partials', 1),
	(25, '2017_10_23_000024_Db_System_Mail_Layouts_Add_Options_Field', 1),
	(26, '2021_10_01_000025_Db_Add_Pivot_Data_To_Deferred_Bindings', 1),
	(27, '2022_08_06_000026_Db_System_Add_App_Birthday_Date', 1),
	(28, '2022_10_14_000027_Db_Jobs_FailedJobs_Update', 1),
	(29, '2013_10_01_000001_Db_Backend_Users', 2),
	(30, '2013_10_01_000002_Db_Backend_User_Groups', 2),
	(31, '2013_10_01_000003_Db_Backend_Users_Groups', 2),
	(32, '2013_10_01_000004_Db_Backend_User_Throttle', 2),
	(33, '2014_01_04_000005_Db_Backend_User_Preferences', 2),
	(34, '2014_10_01_000006_Db_Backend_Access_Log', 2),
	(35, '2014_10_01_000007_Db_Backend_Add_Description_Field', 2),
	(36, '2015_10_01_000008_Db_Backend_Add_Superuser_Flag', 2),
	(37, '2016_10_01_000009_Db_Backend_Timestamp_Fix', 2),
	(38, '2017_10_01_000010_Db_Backend_User_Roles', 2),
	(39, '2018_12_16_000011_Db_Backend_Add_Deleted_At', 2),
	(40, '2014_10_01_000001_Db_Cms_Theme_Data', 3),
	(41, '2016_10_01_000002_Db_Cms_Timestamp_Fix', 3),
	(42, '2017_10_01_000003_Db_Cms_Theme_Logs', 3),
	(43, '2018_11_01_000001_Db_Cms_Theme_Templates', 3);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Listage de la structure de la table aadminli_c57fr. rainlab_sitemap_definitions
CREATE TABLE IF NOT EXISTS `rainlab_sitemap_definitions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `theme` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `data` mediumtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `rainlab_sitemap_definitions_theme_index` (`theme`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table aadminli_c57fr.rainlab_sitemap_definitions : ~0 rows (environ)
/*!40000 ALTER TABLE `rainlab_sitemap_definitions` DISABLE KEYS */;
/*!40000 ALTER TABLE `rainlab_sitemap_definitions` ENABLE KEYS */;

-- Listage de la structure de la table aadminli_c57fr. sessions
CREATE TABLE IF NOT EXISTS `sessions` (
  `id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` text COLLATE utf8mb4_unicode_ci,
  `last_activity` int(11) DEFAULT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_agent` text COLLATE utf8mb4_unicode_ci,
  UNIQUE KEY `sessions_id_unique` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table aadminli_c57fr.sessions : ~0 rows (environ)
/*!40000 ALTER TABLE `sessions` DISABLE KEYS */;
/*!40000 ALTER TABLE `sessions` ENABLE KEYS */;

-- Listage de la structure de la table aadminli_c57fr. system_event_logs
CREATE TABLE IF NOT EXISTS `system_event_logs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `level` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` text COLLATE utf8mb4_unicode_ci,
  `details` mediumtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `system_event_logs_level_index` (`level`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table aadminli_c57fr.system_event_logs : ~2 rows (environ)
/*!40000 ALTER TABLE `system_event_logs` DISABLE KEYS */;
INSERT INTO `system_event_logs` (`id`, `level`, `message`, `details`, `created_at`, `updated_at`) VALUES
	(1, 'info', 'Table books re-initialized', NULL, '2023-03-28 17:55:02', '2023-03-28 17:55:02'),
	(2, 'info', 'Table owners re-initialized', NULL, '2023-03-28 17:55:03', '2023-03-28 17:55:03');
/*!40000 ALTER TABLE `system_event_logs` ENABLE KEYS */;

-- Listage de la structure de la table aadminli_c57fr. system_files
CREATE TABLE IF NOT EXISTS `system_files` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `disk_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_size` int(11) NOT NULL,
  `content_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `field` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attachment_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attachment_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_public` tinyint(1) NOT NULL DEFAULT '1',
  `sort_order` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `system_files_field_index` (`field`),
  KEY `system_files_attachment_id_index` (`attachment_id`),
  KEY `system_files_attachment_type_index` (`attachment_type`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table aadminli_c57fr.system_files : ~2 rows (environ)
/*!40000 ALTER TABLE `system_files` DISABLE KEYS */;
INSERT INTO `system_files` (`id`, `disk_name`, `file_name`, `file_size`, `content_type`, `title`, `description`, `field`, `attachment_id`, `attachment_type`, `is_public`, `sort_order`, `created_at`, `updated_at`) VALUES
	(1, 'gc7c57docfavicon.ico', 'favicon.ico', 1150, 'image/vnd.microsoft.icon', NULL, NULL, 'favicon', '1', 'Backend\\Models\\BrandSetting', 1, 1, '2023-03-28 17:55:04', '2023-03-28 17:55:04'),
	(2, 'gc7c57doclogo.svg', 'logo.svg', 727, 'image/svg+xml', NULL, NULL, 'logo', '1', 'Backend\\Models\\BrandSetting', 1, 5, '2023-03-28 17:55:04', '2023-03-28 17:55:04');
/*!40000 ALTER TABLE `system_files` ENABLE KEYS */;

-- Listage de la structure de la table aadminli_c57fr. system_mail_layouts
CREATE TABLE IF NOT EXISTS `system_mail_layouts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content_html` text COLLATE utf8mb4_unicode_ci,
  `content_text` text COLLATE utf8mb4_unicode_ci,
  `content_css` text COLLATE utf8mb4_unicode_ci,
  `is_locked` tinyint(1) NOT NULL DEFAULT '0',
  `options` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table aadminli_c57fr.system_mail_layouts : ~2 rows (environ)
/*!40000 ALTER TABLE `system_mail_layouts` DISABLE KEYS */;
INSERT INTO `system_mail_layouts` (`id`, `name`, `code`, `content_html`, `content_text`, `content_css`, `is_locked`, `options`, `created_at`, `updated_at`) VALUES
	(1, 'Default layout', 'default', '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">\n<html xmlns="http://www.w3.org/1999/xhtml">\n<head>\n    <meta name="viewport" content="width=device-width, initial-scale=1.0" />\n    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />\n</head>\n<body>\n    <style type="text/css" media="screen">\n        {{ brandCss|raw }}\n        {{ css|raw }}\n    </style>\n\n    <table class="wrapper layout-default" width="100%" cellpadding="0" cellspacing="0">\n\n        <!-- Header -->\n        {% partial \'header\' body %}\n            {{ subject|raw }}\n        {% endpartial %}\n\n        <tr>\n            <td align="center">\n                <table class="content" width="100%" cellpadding="0" cellspacing="0">\n                    <!-- Email Body -->\n                    <tr>\n                        <td class="body" width="100%" cellpadding="0" cellspacing="0">\n                            <table class="inner-body" align="center" width="570" cellpadding="0" cellspacing="0">\n                                <!-- Body content -->\n                                <tr>\n                                    <td class="content-cell">\n                                        {{ content|raw }}\n                                    </td>\n                                </tr>\n                            </table>\n                        </td>\n                    </tr>\n                </table>\n            </td>\n        </tr>\n\n        <!-- Footer -->\n        {% partial \'footer\' body %}\n            &copy; {{ "now"|date("Y") }} {{ appName }}. All rights reserved.\n        {% endpartial %}\n\n    </table>\n\n</body>\n</html>', '{{ content|raw }}', '@media only screen and (max-width: 600px) {\n    .inner-body {\n        width: 100% !important;\n    }\n\n    .footer {\n        width: 100% !important;\n    }\n}\n\n@media only screen and (max-width: 500px) {\n    .button {\n        width: 100% !important;\n    }\n}', 1, NULL, '2023-03-28 17:54:55', '2023-03-28 17:54:55'),
	(2, 'System layout', 'system', '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">\n<html xmlns="http://www.w3.org/1999/xhtml">\n<head>\n    <meta name="viewport" content="width=device-width, initial-scale=1.0" />\n    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />\n</head>\n<body>\n    <style type="text/css" media="screen">\n        {{ brandCss|raw }}\n        {{ css|raw }}\n    </style>\n\n    <table class="wrapper layout-system" width="100%" cellpadding="0" cellspacing="0">\n        <tr>\n            <td align="center">\n                <table class="content" width="100%" cellpadding="0" cellspacing="0">\n                    <!-- Email Body -->\n                    <tr>\n                        <td class="body" width="100%" cellpadding="0" cellspacing="0">\n                            <table class="inner-body" align="center" width="570" cellpadding="0" cellspacing="0">\n                                <!-- Body content -->\n                                <tr>\n                                    <td class="content-cell">\n                                        {{ content|raw }}\n\n                                        <!-- Subcopy -->\n                                        {% partial \'subcopy\' body %}\n                                            **This is an automatic message. Please do not reply to it.**\n                                        {% endpartial %}\n                                    </td>\n                                </tr>\n                            </table>\n                        </td>\n                    </tr>\n                </table>\n            </td>\n        </tr>\n    </table>\n\n</body>\n</html>', '{{ content|raw }}\n\n\n---\nThis is an automatic message. Please do not reply to it.', '@media only screen and (max-width: 600px) {\n    .inner-body {\n        width: 100% !important;\n    }\n\n    .footer {\n        width: 100% !important;\n    }\n}\n\n@media only screen and (max-width: 500px) {\n    .button {\n        width: 100% !important;\n    }\n}', 1, NULL, '2023-03-28 17:54:55', '2023-03-28 17:54:55');
/*!40000 ALTER TABLE `system_mail_layouts` ENABLE KEYS */;

-- Listage de la structure de la table aadminli_c57fr. system_mail_partials
CREATE TABLE IF NOT EXISTS `system_mail_partials` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content_html` text COLLATE utf8mb4_unicode_ci,
  `content_text` text COLLATE utf8mb4_unicode_ci,
  `is_custom` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table aadminli_c57fr.system_mail_partials : ~0 rows (environ)
/*!40000 ALTER TABLE `system_mail_partials` DISABLE KEYS */;
/*!40000 ALTER TABLE `system_mail_partials` ENABLE KEYS */;

-- Listage de la structure de la table aadminli_c57fr. system_mail_templates
CREATE TABLE IF NOT EXISTS `system_mail_templates` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subject` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `content_html` text COLLATE utf8mb4_unicode_ci,
  `content_text` text COLLATE utf8mb4_unicode_ci,
  `layout_id` int(11) DEFAULT NULL,
  `is_custom` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `system_mail_templates_layout_id_index` (`layout_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table aadminli_c57fr.system_mail_templates : ~0 rows (environ)
/*!40000 ALTER TABLE `system_mail_templates` DISABLE KEYS */;
/*!40000 ALTER TABLE `system_mail_templates` ENABLE KEYS */;

-- Listage de la structure de la table aadminli_c57fr. system_parameters
CREATE TABLE IF NOT EXISTS `system_parameters` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `namespace` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `group` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `item` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `item_index` (`namespace`,`group`,`item`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table aadminli_c57fr.system_parameters : ~4 rows (environ)
/*!40000 ALTER TABLE `system_parameters` DISABLE KEYS */;
INSERT INTO `system_parameters` (`id`, `namespace`, `group`, `item`, `value`) VALUES
	(1, 'system', 'app', 'birthday', '"2023-03-28T15:54:33.473584Z"'),
	(2, 'cms', 'theme', 'active', 'c57'),
	(3, 'system', 'update', 'count', '0'),
	(4, 'system', 'update', 'retry', '1680105406');
/*!40000 ALTER TABLE `system_parameters` ENABLE KEYS */;

-- Listage de la structure de la table aadminli_c57fr. system_plugin_history
CREATE TABLE IF NOT EXISTS `system_plugin_history` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `version` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `detail` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `system_plugin_history_code_index` (`code`),
  KEY `system_plugin_history_type_index` (`type`)
) ENGINE=InnoDB AUTO_INCREMENT=368 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table aadminli_c57fr.system_plugin_history : ~367 rows (environ)
/*!40000 ALTER TABLE `system_plugin_history` DISABLE KEYS */;
INSERT INTO `system_plugin_history` (`id`, `code`, `type`, `version`, `detail`, `created_at`) VALUES
	(1, 'Alxy.SimpleMenu', 'comment', '1.0.1', 'First version of SimpleMenu', '2023-03-28 17:54:56'),
	(2, 'GC7.Librairy', 'script', '1.0.1', '1.0.1-builder_table_create_gc7_librairy_books.php', '2023-03-28 17:54:57'),
	(3, 'GC7.Librairy', 'comment', '1.0.1', 'First version of Librairy and created table gc7_librairy_books', '2023-03-28 17:54:57'),
	(4, 'Grcote7.Books', 'comment', '1.0.1', 'Initialize plugin.', '2023-03-28 17:54:57'),
	(5, 'Grcote7.Books', 'script', '1.0.2', 'builder_table_create_grcote7_books_.php', '2023-03-28 17:54:58'),
	(6, 'Grcote7.Books', 'comment', '1.0.2', 'Created table grcote7_books_', '2023-03-28 17:54:58'),
	(7, 'Grcote7.Books', 'script', '1.0.3', 'builder_table_create_grcote7_books_owners.php', '2023-03-28 17:54:59'),
	(8, 'Grcote7.Books', 'comment', '1.0.3', 'Created table grcote7_books_owners', '2023-03-28 17:54:59'),
	(9, 'Grcote7.Books', 'script', '1.0.4', 'builder_table_update_grcote7_books_.php', '2023-03-28 17:54:59'),
	(10, 'Grcote7.Books', 'comment', '1.0.4', 'Updated table grcote7_books_', '2023-03-28 17:54:59'),
	(11, 'Grcote7.Books', 'script', '1.0.5', 'builder_table_update_grcote7_books_owners.php', '2023-03-28 17:55:00'),
	(12, 'Grcote7.Books', 'comment', '1.0.5', 'Updated table grcote7_books_owners', '2023-03-28 17:55:00'),
	(13, 'Grcote7.Books', 'script', '1.0.6', 'builder_table_update_grcote7_books_owners_2.php', '2023-03-28 17:55:01'),
	(14, 'Grcote7.Books', 'comment', '1.0.6', 'Updated table grcote7_books_owners', '2023-03-28 17:55:01'),
	(15, 'Grcote7.Books', 'script', '1.0.7', 'builder_table_update_grcote7_books_owners_3.php', '2023-03-28 17:55:02'),
	(16, 'Grcote7.Books', 'script', '1.0.7', 'seed_all_tables.php', '2023-03-28 17:55:04'),
	(17, 'Grcote7.Books', 'comment', '1.0.7', 'Updated table grcote7_books_owners and seed tables', '2023-03-28 17:55:04'),
	(18, 'GrCOTE7.BrandC57', 'script', '1.0.1', '1.0.1-seed_settings_table.php', '2023-03-28 17:55:04'),
	(19, 'GrCOTE7.BrandC57', 'comment', '1.0.1', 'Initialize brand C57', '2023-03-28 17:55:04'),
	(20, 'GrCOTE7.C57', 'comment', '1.0.1', 'Initialize plugin.', '2023-03-28 17:55:04'),
	(21, 'GrCOTE7.C57', 'comment', '1.0.2', 'Seeds demo users and profile', '2023-03-28 17:55:05'),
	(22, 'Grcote7.Contact', 'comment', '1.0.1', 'Initialize plugin.', '2023-03-28 17:55:05'),
	(23, 'GrCOTE7.Demos', 'comment', '1.0.0', 'First version of plugin Demos', '2023-03-28 17:55:05'),
	(24, 'GrCOTE7.Demos', 'comment', '1.0.1', 'Component HomeT', '2023-03-28 17:55:05'),
	(25, 'GrCOTE7.Demos', 'comment', '1.0.2', 'Component affTri et weather', '2023-03-28 17:55:05'),
	(26, 'GrCOTE7.Demos', 'comment', '1.0.3', 'Component tAF', '2023-03-28 17:55:05'),
	(27, 'GrCOTE7.Dpt', 'comment', '1.0.1', 'Initialize plugin.', '2023-03-28 17:55:05'),
	(28, 'GrCOTE7.Dpt', 'script', '1.0.2', 'builder_table_create_grcote7_dptlist_dpts.php', '2023-03-28 17:55:06'),
	(29, 'GrCOTE7.Dpt', 'comment', '1.0.2', 'Created table grcote7_dptlist_dpts', '2023-03-28 17:55:06'),
	(30, 'Grcote7.Library', 'comment', '1.0.1', 'Initialize plugin.', '2023-03-28 17:55:06'),
	(31, 'Grcote7.Library', 'script', '1.0.2', 'builder_table_create_grcote7_library_books.php', '2023-03-28 17:55:07'),
	(32, 'Grcote7.Library', 'comment', '1.0.2', 'Created table grcote7_library_books', '2023-03-28 17:55:07'),
	(33, 'Grcote7.Movies', 'comment', '1.0.1', 'Initialize plugin.', '2023-03-28 17:55:07'),
	(34, 'Grcote7.Movies', 'script', '1.0.2', 'builder_table_create_grcote7_movies_.php', '2023-03-28 17:55:07'),
	(35, 'Grcote7.Movies', 'comment', '1.0.2', 'Created table grcote7_movies_', '2023-03-28 17:55:07'),
	(36, 'Grcote7.Movies', 'script', '1.0.3', 'builder_table_update_grcote7_movies_.php', '2023-03-28 17:55:08'),
	(37, 'Grcote7.Movies', 'comment', '1.0.3', 'Updated table grcote7_movies_', '2023-03-28 17:55:08'),
	(38, 'Grcote7.Movies', 'script', '1.0.4', 'builder_table_create_grcote7_movies_genres.php', '2023-03-28 17:55:09'),
	(39, 'Grcote7.Movies', 'comment', '1.0.4', 'Created table grcote7_movies_genres', '2023-03-28 17:55:09'),
	(40, 'Grcote7.Movies', 'script', '1.0.5', 'builder_table_create_grcote7_movies_movies_genres.php', '2023-03-28 17:55:11'),
	(41, 'Grcote7.Movies', 'comment', '1.0.5', 'Created table grcote7_movies_movies_genres', '2023-03-28 17:55:11'),
	(42, 'Grcote7.Movies', 'script', '1.0.6', 'builder_table_update_grcote7_movies__2.php', '2023-03-28 17:55:12'),
	(43, 'Grcote7.Movies', 'comment', '1.0.6', 'Updated table grcote7_movies_', '2023-03-28 17:55:12'),
	(44, 'Grcote7.Movies', 'script', '1.0.7', 'builder_table_create_grcote7_movies_actors.php', '2023-03-28 17:55:12'),
	(45, 'Grcote7.Movies', 'comment', '1.0.7', 'Created table grcote7_movies_actors', '2023-03-28 17:55:12'),
	(46, 'Grcote7.Movies', 'script', '1.0.8', 'builder_table_update_grcote7_movies__3.php', '2023-03-28 17:55:13'),
	(47, 'Grcote7.Movies', 'comment', '1.0.8', 'Updated table grcote7_movies_', '2023-03-28 17:55:13'),
	(48, 'Grcote7.Movies', 'script', '1.0.9', 'builder_table_create_grcote7_movies_actors_movies.php', '2023-03-28 17:55:15'),
	(49, 'Grcote7.Movies', 'comment', '1.0.9', 'Created table grcote7_movies_actors_movies', '2023-03-28 17:55:15'),
	(50, 'Grcote7.Movies', 'script', '1.0.10', 'seed.php', '2023-03-28 17:55:22'),
	(51, 'Grcote7.Movies', 'comment', '1.0.10', 'Seed movies', '2023-03-28 17:55:22'),
	(52, 'Grcote7.Movies', 'script', '1.0.11', 'builder_table_update_grcote7_movies__4.php', '2023-03-28 17:55:23'),
	(53, 'Grcote7.Movies', 'comment', '1.0.11', 'Updated table grcote7_movies_', '2023-03-28 17:55:23'),
	(54, 'Grcote7.Movies', 'script', '1.0.12', 'builder_table_update_grcote7_movies__5.php', '2023-03-28 17:55:25'),
	(55, 'Grcote7.Movies', 'comment', '1.0.12', 'Updated table grcote7_movies_', '2023-03-28 17:55:25'),
	(56, 'Grcote7.Movies', 'script', '1.0.13', 'builder_table_create_grcote7_movies_tags.php', '2023-03-28 17:55:26'),
	(57, 'Grcote7.Movies', 'comment', '1.0.13', 'Created table grcote7_movies_tags', '2023-03-28 17:55:26'),
	(58, 'Grcote7.Movies', 'script', '1.0.14', 'builder_table_create_grcote7_movies_tags_movies.php', '2023-03-28 17:55:27'),
	(59, 'Grcote7.Movies', 'comment', '1.0.14', 'Created table grcote7_movies_tags_movies', '2023-03-28 17:55:27'),
	(60, 'Grcote7.Movies', 'script', '1.0.15', 'builder_table_update_grcote7_movies_tags_movies.php', '2023-03-28 17:55:28'),
	(61, 'Grcote7.Movies', 'comment', '1.0.15', 'Updated table grcote7_movies_tags_movies', '2023-03-28 17:55:28'),
	(62, 'GrCOTE7.Php', 'comment', '1.0.1', 'First version of Php', '2023-03-28 17:55:28'),
	(63, 'Grcote7.Vehicules', 'comment', '1.0.1', 'Initialize plugin.', '2023-03-28 17:55:28'),
	(64, 'Marcomessa.Vite', 'comment', '1.0.1', 'First version of Vite', '2023-03-28 17:55:28'),
	(65, 'RainLab.GoogleAnalytics', 'comment', '1.0.1', 'Initialize plugin', '2023-03-28 17:55:28'),
	(66, 'RainLab.GoogleAnalytics', 'comment', '1.0.2', 'Fixed a minor bug in the Top Pages widget', '2023-03-28 17:55:28'),
	(67, 'RainLab.GoogleAnalytics', 'comment', '1.0.3', 'Minor improvements to the code', '2023-03-28 17:55:28'),
	(68, 'RainLab.GoogleAnalytics', 'comment', '1.0.4', 'Fixes a bug where the certificate upload fails', '2023-03-28 17:55:29'),
	(69, 'RainLab.GoogleAnalytics', 'comment', '1.0.5', 'Minor fix to support the updated Google Analytics library', '2023-03-28 17:55:29'),
	(70, 'RainLab.GoogleAnalytics', 'comment', '1.0.6', 'Fixes dashboard widget using latest Google Analytics library', '2023-03-28 17:55:29'),
	(71, 'RainLab.GoogleAnalytics', 'comment', '1.0.7', 'Removes Client ID from settings because the workflow no longer needs it', '2023-03-28 17:55:29'),
	(72, 'RainLab.GoogleAnalytics', 'comment', '1.1.0', '!!! Updated to the latest Google API library', '2023-03-28 17:55:29'),
	(73, 'RainLab.GoogleAnalytics', 'comment', '1.2.0', 'Update Guzzle library to version 6.0', '2023-03-28 17:55:29'),
	(74, 'RainLab.GoogleAnalytics', 'comment', '1.2.1', 'Update the plugin compatibility with RC8 Google API client', '2023-03-28 17:55:29'),
	(75, 'RainLab.GoogleAnalytics', 'comment', '1.2.2', 'Improve translations, bump version requirement to PHP 7', '2023-03-28 17:55:30'),
	(76, 'RainLab.GoogleAnalytics', 'comment', '1.2.3', 'Added a switch for forceSSL', '2023-03-28 17:55:30'),
	(77, 'RainLab.GoogleAnalytics', 'comment', '1.2.4', 'Added permission for dashboard widgets. Added Turkish, Spanish and Estonian translations.', '2023-03-28 17:55:30'),
	(78, 'RainLab.GoogleAnalytics', 'comment', '1.2.5', 'Fixed issues with PHP 7.4 compatibility.', '2023-03-28 17:55:30'),
	(79, 'RainLab.GoogleAnalytics', 'comment', '1.3.0', '!!! Upgraded tracker code to Universal Analytics (gtag.js)', '2023-03-28 17:55:30'),
	(80, 'RainLab.Sitemap', 'comment', '1.0.1', 'First version of Sitemap', '2023-03-28 17:55:30'),
	(81, 'RainLab.Sitemap', 'script', '1.0.2', 'create_definitions_table.php', '2023-03-28 17:55:31'),
	(82, 'RainLab.Sitemap', 'comment', '1.0.2', 'Create definitions table', '2023-03-28 17:55:31'),
	(83, 'RainLab.Sitemap', 'comment', '1.0.3', 'Minor improvements to the code.', '2023-03-28 17:55:31'),
	(84, 'RainLab.Sitemap', 'comment', '1.0.4', 'Fixes issue where correct headers not being sent.', '2023-03-28 17:55:31'),
	(85, 'RainLab.Sitemap', 'comment', '1.0.5', 'Minor back-end styling fix.', '2023-03-28 17:55:32'),
	(86, 'RainLab.Sitemap', 'comment', '1.0.6', 'Minor fix to internal API.', '2023-03-28 17:55:32'),
	(87, 'RainLab.Sitemap', 'comment', '1.0.7', 'Added access premissions.', '2023-03-28 17:55:32'),
	(88, 'RainLab.Sitemap', 'comment', '1.0.8', 'Minor styling updates.', '2023-03-28 17:55:32'),
	(89, 'Winter.Blog', 'script', '1.0.1', 'v1.0.1/create_posts_table.php', '2023-03-28 17:55:34'),
	(90, 'Winter.Blog', 'script', '1.0.1', 'v1.0.1/create_categories_table.php', '2023-03-28 17:55:37'),
	(91, 'Winter.Blog', 'script', '1.0.1', 'v1.0.1/seed_all_tables.php', '2023-03-28 17:55:40'),
	(92, 'Winter.Blog', 'comment', '1.0.1', 'Initialize plugin.', '2023-03-28 17:55:40'),
	(93, 'Winter.Blog', 'comment', '1.0.2', 'Added the processed HTML content column to the posts table.', '2023-03-28 17:55:40'),
	(94, 'Winter.Blog', 'comment', '1.0.3', 'Category component has been merged with Posts component.', '2023-03-28 17:55:40'),
	(95, 'Winter.Blog', 'comment', '1.0.4', 'Improvements to the Posts list management UI.', '2023-03-28 17:55:40'),
	(96, 'Winter.Blog', 'comment', '1.0.5', 'Removes the Author column from blog post list.', '2023-03-28 17:55:40'),
	(97, 'Winter.Blog', 'comment', '1.0.6', 'Featured images now appear in the Post component.', '2023-03-28 17:55:40'),
	(98, 'Winter.Blog', 'comment', '1.0.7', 'Added support for the Static Pages menus.', '2023-03-28 17:55:40'),
	(99, 'Winter.Blog', 'comment', '1.0.8', 'Added total posts to category list.', '2023-03-28 17:55:41'),
	(100, 'Winter.Blog', 'comment', '1.0.9', 'Added support for the Sitemap plugin.', '2023-03-28 17:55:41'),
	(101, 'Winter.Blog', 'comment', '1.0.10', 'Added permission to prevent users from seeing posts they did not create.', '2023-03-28 17:55:41'),
	(102, 'Winter.Blog', 'comment', '1.0.11', 'Deprecate "idParam" component property in favour of "slug" property.', '2023-03-28 17:55:41'),
	(103, 'Winter.Blog', 'comment', '1.0.12', 'Fixes issue where images cannot be uploaded caused by latest Markdown library.', '2023-03-28 17:55:41'),
	(104, 'Winter.Blog', 'comment', '1.0.13', 'Fixes problem with providing pages to Sitemap and Pages plugins.', '2023-03-28 17:55:41'),
	(105, 'Winter.Blog', 'comment', '1.0.14', 'Add support for CSRF protection feature added to core.', '2023-03-28 17:55:41'),
	(106, 'Winter.Blog', 'comment', '1.1.0', 'Replaced the Post editor with the new core Markdown editor.', '2023-03-28 17:55:42'),
	(107, 'Winter.Blog', 'comment', '1.1.1', 'Posts can now be imported and exported.', '2023-03-28 17:55:42'),
	(108, 'Winter.Blog', 'comment', '1.1.2', 'Posts are no longer visible if the published date has not passed.', '2023-03-28 17:55:42'),
	(109, 'Winter.Blog', 'comment', '1.1.3', 'Added a New Post shortcut button to the blog menu.', '2023-03-28 17:55:42'),
	(110, 'Winter.Blog', 'script', '1.2.0', 'v1.2.0/categories_add_nested_fields.php', '2023-03-28 17:55:42'),
	(111, 'Winter.Blog', 'comment', '1.2.0', 'Categories now support nesting.', '2023-03-28 17:55:42'),
	(112, 'Winter.Blog', 'comment', '1.2.1', 'Post slugs now must be unique.', '2023-03-28 17:55:42'),
	(113, 'Winter.Blog', 'comment', '1.2.2', 'Fixes issue on new installs.', '2023-03-28 17:55:43'),
	(114, 'Winter.Blog', 'comment', '1.2.3', 'Minor user interface update.', '2023-03-28 17:55:43'),
	(115, 'Winter.Blog', 'script', '1.2.4', 'v1.2.4/update_timestamp_nullable.php', '2023-03-28 17:55:44'),
	(116, 'Winter.Blog', 'comment', '1.2.4', 'Database maintenance. Updated all timestamp columns to be nullable.', '2023-03-28 17:55:44'),
	(117, 'Winter.Blog', 'comment', '1.2.5', 'Added translation support for blog posts.', '2023-03-28 17:55:44'),
	(118, 'Winter.Blog', 'comment', '1.2.6', 'The published field can now supply a time with the date.', '2023-03-28 17:55:44'),
	(119, 'Winter.Blog', 'comment', '1.2.7', 'Introduced a new RSS feed component.', '2023-03-28 17:55:44'),
	(120, 'Winter.Blog', 'comment', '1.2.8', 'Fixes issue with translated `content_html` attribute on blog posts.', '2023-03-28 17:55:44'),
	(121, 'Winter.Blog', 'comment', '1.2.9', 'Added translation support for blog categories.', '2023-03-28 17:55:44'),
	(122, 'Winter.Blog', 'comment', '1.2.10', 'Added translation support for post slugs.', '2023-03-28 17:55:44'),
	(123, 'Winter.Blog', 'comment', '1.2.11', 'Fixes bug where excerpt is not translated.', '2023-03-28 17:55:45'),
	(124, 'Winter.Blog', 'comment', '1.2.12', 'Description field added to category form.', '2023-03-28 17:55:45'),
	(125, 'Winter.Blog', 'comment', '1.2.13', 'Improved support for Static Pages menus, added a blog post and all blog posts.', '2023-03-28 17:55:45'),
	(126, 'Winter.Blog', 'comment', '1.2.14', 'Added post exception property to the post list component, useful for showing related posts.', '2023-03-28 17:55:45'),
	(127, 'Winter.Blog', 'comment', '1.2.15', 'Back-end navigation sort order updated.', '2023-03-28 17:55:45'),
	(128, 'Winter.Blog', 'comment', '1.2.16', 'Added `nextPost` and `previousPost` to the blog post component.', '2023-03-28 17:55:45'),
	(129, 'Winter.Blog', 'comment', '1.2.17', 'Improved the next and previous logic to sort by the published date.', '2023-03-28 17:55:45'),
	(130, 'Winter.Blog', 'comment', '1.2.18', 'Minor change to internals.', '2023-03-28 17:55:46'),
	(131, 'Winter.Blog', 'comment', '1.2.19', 'Improved support for Build 420+', '2023-03-28 17:55:46'),
	(132, 'Winter.Blog', 'script', '1.3.0', 'v1.3.0/posts_add_metadata.php', '2023-03-28 17:55:47'),
	(133, 'Winter.Blog', 'comment', '1.3.0', 'Added metadata column for plugins to store data in', '2023-03-28 17:55:48'),
	(134, 'Winter.Blog', 'comment', '1.3.1', 'Fixed metadata column not being jsonable', '2023-03-28 17:55:48'),
	(135, 'Winter.Blog', 'comment', '1.3.2', 'Allow custom slug name for components, add 404 handling for missing blog posts, allow exporting of blog images.', '2023-03-28 17:55:48'),
	(136, 'Winter.Blog', 'comment', '1.3.3', 'Fixed \'excluded categories\' filter from being run when value is empty.', '2023-03-28 17:55:48'),
	(137, 'Winter.Blog', 'comment', '1.3.4', 'Allow post author to be specified. Improved translations.', '2023-03-28 17:55:48'),
	(138, 'Winter.Blog', 'comment', '1.3.5', 'Fixed missing user info from breaking initial seeder in migrations. Fixed a PostgreSQL issue with blog exports.', '2023-03-28 17:55:48'),
	(139, 'Winter.Blog', 'comment', '1.3.6', 'Improved French translations.', '2023-03-28 17:55:48'),
	(140, 'Winter.Blog', 'comment', '1.4.0', 'Stability improvements. Rollback custom slug names for components', '2023-03-28 17:55:49'),
	(141, 'Winter.Blog', 'comment', '1.4.1', 'Fixes potential security issue with unsafe Markdown. Allow blog bylines to be translated.', '2023-03-28 17:55:49'),
	(142, 'Winter.Blog', 'comment', '1.4.2', 'Fix 404 redirects for missing blog posts. Assign current category to the listed posts when using the Posts component on a page with the category parameter available.', '2023-03-28 17:55:49'),
	(143, 'Winter.Blog', 'comment', '1.4.3', 'Fixes incompatibility with locale switching when plugin is used in conjunction with the Translate plugin. Fixes undefined category error.', '2023-03-28 17:55:49'),
	(144, 'Winter.Blog', 'comment', '1.4.4', 'Rollback translated bylines, please move or override the default component markup instead.', '2023-03-28 17:55:49'),
	(145, 'Winter.Blog', 'comment', '1.5.0', 'Implement support for October CMS v2.0', '2023-03-28 17:55:49'),
	(146, 'Winter.Blog', 'script', '2.0.0', 'v2.0.0/rename_tables.php', '2023-03-28 17:55:50'),
	(147, 'Winter.Blog', 'comment', '2.0.0', 'Rebrand to Winter.Blog', '2023-03-28 17:55:50'),
	(148, 'Winter.Builder', 'comment', '1.0.1', 'Initialize plugin.', '2023-03-28 17:55:51'),
	(149, 'Winter.Builder', 'comment', '1.0.2', 'Fixes the problem with selecting a plugin. Minor localization corrections. Configuration files in the list and form behaviors are now autocomplete.', '2023-03-28 17:55:51'),
	(150, 'Winter.Builder', 'comment', '1.0.3', 'Improved handling of the enum data type.', '2023-03-28 17:55:51'),
	(151, 'Winter.Builder', 'comment', '1.0.4', 'Added user permissions to work with the Builder.', '2023-03-28 17:55:51'),
	(152, 'Winter.Builder', 'comment', '1.0.5', 'Fixed permissions registration.', '2023-03-28 17:55:51'),
	(153, 'Winter.Builder', 'comment', '1.0.6', 'Fixed front-end record ordering in the Record List component.', '2023-03-28 17:55:51'),
	(154, 'Winter.Builder', 'comment', '1.0.7', 'Builder settings are now protected with user permissions. The database table column list is scrollable now. Minor code cleanup.', '2023-03-28 17:55:51'),
	(155, 'Winter.Builder', 'comment', '1.0.8', 'Added the Reorder Controller behavior.', '2023-03-28 17:55:52'),
	(156, 'Winter.Builder', 'comment', '1.0.9', 'Minor API and UI updates.', '2023-03-28 17:55:52'),
	(157, 'Winter.Builder', 'comment', '1.0.10', 'Minor styling update.', '2023-03-28 17:55:52'),
	(158, 'Winter.Builder', 'comment', '1.0.11', 'Fixed a bug where clicking placeholder in a repeater would open Inspector. Fixed a problem with saving forms with repeaters in tabs. Minor style fix.', '2023-03-28 17:55:52'),
	(159, 'Winter.Builder', 'comment', '1.0.12', 'Added support for the Trigger property to the Media Finder widget configuration. Names of form fields and list columns definition files can now contain underscores.', '2023-03-28 17:55:52'),
	(160, 'Winter.Builder', 'comment', '1.0.13', 'Minor styling fix on the database editor.', '2023-03-28 17:55:52'),
	(161, 'Winter.Builder', 'comment', '1.0.14', 'Added support for published_at timestamp field', '2023-03-28 17:55:52'),
	(162, 'Winter.Builder', 'comment', '1.0.15', 'Fixed a bug where saving a localization string in Inspector could cause a JavaScript error. Added support for Timestamps and Soft Deleting for new models.', '2023-03-28 17:55:53'),
	(163, 'Winter.Builder', 'comment', '1.0.16', 'Fixed a bug when saving a form with the Repeater widget in a tab could create invalid fields in the form\'s outside area. Added a check that prevents creating localization strings inside other existing strings.', '2023-03-28 17:55:53'),
	(164, 'Winter.Builder', 'comment', '1.0.17', 'Added support Trigger attribute support for RecordFinder and Repeater form widgets.', '2023-03-28 17:55:53'),
	(165, 'Winter.Builder', 'comment', '1.0.18', 'Fixes a bug where \'::class\' notations in a model class definition could prevent the model from appearing in the Builder model list. Added emptyOption property support to the dropdown form control.', '2023-03-28 17:55:53'),
	(166, 'Winter.Builder', 'comment', '1.0.19', 'Added a feature allowing to add all database columns to a list definition. Added max length validation for database table and column names.', '2023-03-28 17:55:53'),
	(167, 'Winter.Builder', 'comment', '1.0.20', 'Fixes a bug where form the builder could trigger the "current.hasAttribute is not a function" error.', '2023-03-28 17:55:53'),
	(168, 'Winter.Builder', 'comment', '1.0.21', 'Back-end navigation sort order updated.', '2023-03-28 17:55:53'),
	(169, 'Winter.Builder', 'comment', '1.0.22', 'Added scopeValue property to the RecordList component.', '2023-03-28 17:55:54'),
	(170, 'Winter.Builder', 'comment', '1.0.23', 'Added support for balloon-selector field type, added Brazilian Portuguese translation, fixed some bugs', '2023-03-28 17:55:54'),
	(171, 'Winter.Builder', 'comment', '1.0.24', 'Added support for tag list field type, added read only toggle for fields. Prevent plugins from using reserved PHP keywords for class names and namespaces', '2023-03-28 17:55:54'),
	(172, 'Winter.Builder', 'comment', '1.0.25', 'Allow editing of migration code in the "Migration" popup when saving changes in the database editor.', '2023-03-28 17:55:54'),
	(173, 'Winter.Builder', 'comment', '1.0.26', 'Allow special default values for columns and added new "Add ID column" button to database editor.', '2023-03-28 17:55:54'),
	(174, 'Winter.Builder', 'comment', '1.0.27', 'Added ability to use \'scope\' in a form relation field, added ability to change the sort order of versions and added additional properties for repeater widget in form builder. Added Polish translation.', '2023-03-28 17:55:54'),
	(175, 'Winter.Builder', 'script', '2.0.0', 'v2.0.0/convert_data.php', '2023-03-28 17:55:54'),
	(176, 'Winter.Builder', 'comment', '2.0.0', 'Rebrand to Winter.Builder', '2023-03-28 17:55:54'),
	(177, 'Winter.Builder', 'comment', '2.0.0', 'Fixes namespace parsing on php >= 8.0', '2023-03-28 17:55:55'),
	(178, 'Winter.Debugbar', 'comment', '1.0.1', 'First version of Debugbar', '2023-03-28 17:55:55'),
	(179, 'Winter.Debugbar', 'comment', '1.0.2', 'Debugbar facade aliased (Alxy)', '2023-03-28 17:55:55'),
	(180, 'Winter.Debugbar', 'comment', '1.0.3', 'Added ajax debugging', '2023-03-28 17:55:55'),
	(181, 'Winter.Debugbar', 'comment', '1.0.4', 'Only display to backend authenticated users', '2023-03-28 17:55:55'),
	(182, 'Winter.Debugbar', 'comment', '1.0.5', 'Use elevated privileges', '2023-03-28 17:55:55'),
	(183, 'Winter.Debugbar', 'comment', '1.0.6', 'Fix fatal error when cms.page.beforeDisplay is fired multiple times (mnishihan)', '2023-03-28 17:55:55'),
	(184, 'Winter.Debugbar', 'comment', '1.0.7', 'Allow plugin to be installed via Composer (tim0991)', '2023-03-28 17:55:56'),
	(185, 'Winter.Debugbar', 'comment', '1.0.8', 'Fix debugbar dependency', '2023-03-28 17:55:56'),
	(186, 'Winter.Debugbar', 'comment', '2.0.0', '!!! Upgrade for compatibility with Laravel 5.5 (PHP 7+, October 420+)', '2023-03-28 17:55:56'),
	(187, 'Winter.Debugbar', 'comment', '2.0.1', 'Add config file to prevent exceptions from being thrown (credit alxy)', '2023-03-28 17:55:56'),
	(188, 'Winter.Debugbar', 'comment', '3.0.0', 'Switched vendor to RainLab from Bedard, upgraded for compatibility with Laravel 6.x', '2023-03-28 17:55:56'),
	(189, 'Winter.Debugbar', 'comment', '3.0.1', 'Fixed bug that caused 502 errors on AJAX requests', '2023-03-28 17:55:56'),
	(190, 'Winter.Debugbar', 'comment', '3.1.0', 'Important security update and improved styling.', '2023-03-28 17:55:57'),
	(191, 'Winter.Debugbar', 'comment', '3.1.1', 'Added new "store all requests" config option. Added Slovenian translations.', '2023-03-28 17:55:57'),
	(192, 'Winter.Debugbar', 'comment', '4.0.0', 'Switched vendor to Winter from RainLab', '2023-03-28 17:55:57'),
	(193, 'Winter.Debugbar', 'comment', '4.0.1', 'Added Russian translation, added support for Twig v3 / Winter v1.2 / Laravel 9', '2023-03-28 17:55:57'),
	(194, 'Winter.Debugbar', 'comment', '4.0.2', 'Adds new collectors for Twig, CMS and Backend. Updated styling to match Winter branding', '2023-03-28 17:55:57'),
	(195, 'Winter.Demo', 'comment', '1.0.1', 'First version of Demo', '2023-03-28 17:55:57'),
	(196, 'Winter.Notes', 'script', '1.0.1', 'v1.0.1/create_notes.php', '2023-03-28 17:55:58'),
	(197, 'Winter.Notes', 'comment', '1.0.1', 'Initial version', '2023-03-28 17:55:59'),
	(198, 'Winter.Notes', 'comment', '1.0.2', 'Added Russian translation', '2023-03-28 17:55:59'),
	(199, 'Winter.Notes', 'comment', '1.0.3', 'Added Persian translation', '2023-03-28 17:55:59'),
	(200, 'Winter.Notes', 'comment', '1.0.4', 'Added French translation', '2023-03-28 17:55:59'),
	(201, 'Winter.Notes', 'comment', '1.0.5', 'Added Spanish translation', '2023-03-28 17:55:59'),
	(202, 'Winter.Pages', 'comment', '1.0.1', 'Implemented the static pages management and the Static Page component.', '2023-03-28 17:55:59'),
	(203, 'Winter.Pages', 'comment', '1.0.2', 'Fixed the page preview URL.', '2023-03-28 17:55:59'),
	(204, 'Winter.Pages', 'comment', '1.0.3', 'Implemented menus.', '2023-03-28 17:55:59'),
	(205, 'Winter.Pages', 'comment', '1.0.4', 'Implemented the content block management and placeholder support.', '2023-03-28 17:56:00'),
	(206, 'Winter.Pages', 'comment', '1.0.5', 'Added support for the Sitemap plugin.', '2023-03-28 17:56:00'),
	(207, 'Winter.Pages', 'comment', '1.0.6', 'Minor updates to the internal API.', '2023-03-28 17:56:00'),
	(208, 'Winter.Pages', 'comment', '1.0.7', 'Added the Snippets feature.', '2023-03-28 17:56:00'),
	(209, 'Winter.Pages', 'comment', '1.0.8', 'Minor improvements to the code.', '2023-03-28 17:56:00'),
	(210, 'Winter.Pages', 'comment', '1.0.9', 'Fixes issue where Snippet tab is missing from the Partials form.', '2023-03-28 17:56:00'),
	(211, 'Winter.Pages', 'comment', '1.0.10', 'Add translations for various locales.', '2023-03-28 17:56:00'),
	(212, 'Winter.Pages', 'comment', '1.0.11', 'Fixes issue where placeholders tabs were missing from Page form.', '2023-03-28 17:56:01'),
	(213, 'Winter.Pages', 'comment', '1.0.12', 'Implement Media Manager support.', '2023-03-28 17:56:01'),
	(214, 'Winter.Pages', 'script', '1.1.0', 'v1.1.0/snippets_rename_viewbag_properties.php', '2023-03-28 17:56:02'),
	(215, 'Winter.Pages', 'comment', '1.1.0', 'Adds meta title and description to pages. Adds |staticPage filter.', '2023-03-28 17:56:03'),
	(216, 'Winter.Pages', 'comment', '1.1.1', 'Add support for Syntax Fields.', '2023-03-28 17:56:03'),
	(217, 'Winter.Pages', 'comment', '1.1.2', 'Static Breadcrumbs component now respects the hide from navigation setting.', '2023-03-28 17:56:03'),
	(218, 'Winter.Pages', 'comment', '1.1.3', 'Minor back-end styling fix.', '2023-03-28 17:56:03'),
	(219, 'Winter.Pages', 'comment', '1.1.4', 'Minor fix to the StaticPage component API.', '2023-03-28 17:56:03'),
	(220, 'Winter.Pages', 'comment', '1.1.5', 'Fixes bug when using syntax fields.', '2023-03-28 17:56:04'),
	(221, 'Winter.Pages', 'comment', '1.1.6', 'Minor styling fix to the back-end UI.', '2023-03-28 17:56:04'),
	(222, 'Winter.Pages', 'comment', '1.1.7', 'Improved menu item form to include CSS class, open in a new window and hidden flag.', '2023-03-28 17:56:04'),
	(223, 'Winter.Pages', 'comment', '1.1.8', 'Improved the output of snippet partials when saved.', '2023-03-28 17:56:04'),
	(224, 'Winter.Pages', 'comment', '1.1.9', 'Minor update to snippet inspector internal API.', '2023-03-28 17:56:04'),
	(225, 'Winter.Pages', 'comment', '1.1.10', 'Fixes a bug where selecting a layout causes permanent unsaved changes.', '2023-03-28 17:56:04'),
	(226, 'Winter.Pages', 'comment', '1.1.11', 'Add support for repeater syntax field.', '2023-03-28 17:56:05'),
	(227, 'Winter.Pages', 'comment', '1.2.0', 'Added support for translations, UI updates.', '2023-03-28 17:56:05'),
	(228, 'Winter.Pages', 'comment', '1.2.1', 'Use nice titles when listing the content files.', '2023-03-28 17:56:05'),
	(229, 'Winter.Pages', 'comment', '1.2.2', 'Minor styling update.', '2023-03-28 17:56:05'),
	(230, 'Winter.Pages', 'comment', '1.2.3', 'Snippets can now be moved by dragging them.', '2023-03-28 17:56:05'),
	(231, 'Winter.Pages', 'comment', '1.2.4', 'Fixes a bug where the cursor is misplaced when editing text files.', '2023-03-28 17:56:05'),
	(232, 'Winter.Pages', 'comment', '1.2.5', 'Fixes a bug where the parent page is lost upon changing a page layout.', '2023-03-28 17:56:05'),
	(233, 'Winter.Pages', 'comment', '1.2.6', 'Shared view variables are now passed to static pages.', '2023-03-28 17:56:05'),
	(234, 'Winter.Pages', 'comment', '1.2.7', 'Fixes issue with duplicating properties when adding multiple snippets on the same page.', '2023-03-28 17:56:06'),
	(235, 'Winter.Pages', 'comment', '1.2.8', 'Fixes a bug where creating a content block without extension doesn\'t save the contents to file.', '2023-03-28 17:56:06'),
	(236, 'Winter.Pages', 'comment', '1.2.9', 'Add conditional support for translating page URLs.', '2023-03-28 17:56:06'),
	(237, 'Winter.Pages', 'comment', '1.2.10', 'Streamline generation of URLs to use the new Cms::url helper.', '2023-03-28 17:56:06'),
	(238, 'Winter.Pages', 'comment', '1.2.11', 'Implements repeater usage with translate plugin.', '2023-03-28 17:56:06'),
	(239, 'Winter.Pages', 'comment', '1.2.12', 'Fixes minor issue when using snippets and switching the application locale.', '2023-03-28 17:56:06'),
	(240, 'Winter.Pages', 'comment', '1.2.13', 'Fixes bug when AJAX is used on a page that does not yet exist.', '2023-03-28 17:56:06'),
	(241, 'Winter.Pages', 'comment', '1.2.14', 'Add theme logging support for changes made to menus.', '2023-03-28 17:56:06'),
	(242, 'Winter.Pages', 'comment', '1.2.15', 'Back-end navigation sort order updated.', '2023-03-28 17:56:06'),
	(243, 'Winter.Pages', 'comment', '1.2.16', 'Fixes a bug when saving a template that has been modified outside of the CMS (mtime mismatch).', '2023-03-28 17:56:07'),
	(244, 'Winter.Pages', 'comment', '1.2.17', 'Changes locations of custom fields to secondary tabs instead of the primary Settings area. New menu search ability on adding menu items', '2023-03-28 17:56:07'),
	(245, 'Winter.Pages', 'comment', '1.2.18', 'Fixes cache-invalidation issues when Winter.Translate is not installed. Added Greek & Simplified Chinese translations. Removed deprecated calls. Allowed saving HTML in snippet properties. Added support for the MediaFinder in menu items.', '2023-03-28 17:56:07'),
	(246, 'Winter.Pages', 'comment', '1.2.19', 'Catch exception with corrupted menu file.', '2023-03-28 17:56:07'),
	(247, 'Winter.Pages', 'comment', '1.2.20', 'StaticMenu component now exposes menuName property; added pages.menu.referencesGenerated event.', '2023-03-28 17:56:07'),
	(248, 'Winter.Pages', 'comment', '1.2.21', 'Fixes a bug where last Static Menu item cannot be deleted. Improved Persian, Slovak and Turkish translations.', '2023-03-28 17:56:07'),
	(249, 'Winter.Pages', 'comment', '1.3.0', 'Added support for using Database-driven Themes when enabled in the CMS configuration.', '2023-03-28 17:56:07'),
	(250, 'Winter.Pages', 'comment', '1.3.1', 'Added ChildPages Component, prevent hidden pages from being returned via menu item resolver.', '2023-03-28 17:56:07'),
	(251, 'Winter.Pages', 'comment', '1.3.2', 'Fixes error when creating a subpage whose parent has no layout set.', '2023-03-28 17:56:08'),
	(252, 'Winter.Pages', 'comment', '1.3.3', 'Improves user experience for users with only partial access through permissions', '2023-03-28 17:56:08'),
	(253, 'Winter.Pages', 'comment', '1.3.4', 'Fix error where large menus were being truncated due to the PHP "max_input_vars" configuration value. Improved Slovenian translation.', '2023-03-28 17:56:08'),
	(254, 'Winter.Pages', 'comment', '1.3.5', 'Minor fix to bust the browser cache for JS assets. Prevent duplicate property fields in snippet inspector.', '2023-03-28 17:56:08'),
	(255, 'Winter.Pages', 'comment', '1.3.6', 'ChildPages component now displays localized page titles from Translate plugin.', '2023-03-28 17:56:08'),
	(256, 'Winter.Pages', 'comment', '1.3.7', 'Improved page loading performance, added MenuPicker formwidget, added pages.snippets.listSnippets', '2023-03-28 17:56:08'),
	(257, 'Winter.Pages', 'comment', '1.4.0', 'Fixes bug when adding menu items in October CMS v2.0.', '2023-03-28 17:56:08'),
	(258, 'Winter.Pages', 'comment', '1.4.1', 'Fixes support for configuration values.', '2023-03-28 17:56:08'),
	(259, 'Winter.Pages', 'comment', '1.4.3', 'Fixes page deletion in newer platform builds.', '2023-03-28 17:56:09'),
	(260, 'Winter.Pages', 'comment', '2.0.0', 'Rebrand to Winter.Pages', '2023-03-28 17:56:09'),
	(261, 'Winter.Pages', 'comment', '2.0.1', 'Fixes rich editor usage inside repeaters.', '2023-03-28 17:56:09'),
	(262, 'Winter.Pages', 'comment', '2.0.1', 'Fixes a lifecycle issue when switching the page layout.', '2023-03-28 17:56:09'),
	(263, 'Winter.Pages', 'comment', '2.0.1', 'Fixes maintenance mode when using static pages.', '2023-03-28 17:56:09'),
	(264, 'Winter.Pages', 'comment', '2.0.2', 'Add support for Winter 1.2', '2023-03-28 17:56:09'),
	(265, 'Winter.Test', 'comment', '1.0.1', 'First version of Test', '2023-03-28 17:56:09'),
	(266, 'Winter.Test', 'script', '1.0.2', 'v1.0.2/create_tables.php', '2023-03-28 17:56:29'),
	(267, 'Winter.Test', 'comment', '1.0.2', 'Create tables', '2023-03-28 17:56:30'),
	(268, 'Winter.Test', 'script', '1.0.3', 'v1.0.3/seed_tables.php', '2023-03-28 17:56:37'),
	(269, 'Winter.Test', 'comment', '1.0.3', 'Seed tables', '2023-03-28 17:56:37'),
	(270, 'Winter.Test', 'comment', '2.0.0', 'Rebrand to Winter.Test', '2023-03-28 17:56:37'),
	(271, 'Winter.Test', 'comment', '2.0.1', 'Add SoftDelete trait to Post model', '2023-03-28 17:56:37'),
	(272, 'Winter.Test', 'script', '2.0.2', 'v2.0.2/extend_users_roles_table.php', '2023-03-28 17:56:38'),
	(273, 'Winter.Test', 'comment', '2.0.2', 'Add an awards JSON field for UserRolePivot table', '2023-03-28 17:56:38'),
	(274, 'Winter.Test', 'script', '2.0.3', 'v2.0.3/add_page_columns.php', '2023-03-28 17:56:40'),
	(275, 'Winter.Test', 'comment', '2.0.3', 'Add descriptive page columns', '2023-03-28 17:56:40'),
	(276, 'Winter.Test', 'script', '2.0.4', 'v2.0.4/add_page_settings_columns.php', '2023-03-28 17:56:41'),
	(277, 'Winter.Test', 'comment', '2.0.4', 'Add settings columns for pages', '2023-03-28 17:56:41'),
	(278, 'Winter.Test', 'script', '2.1.0', 'v2.1.0/create_records_table.php', '2023-03-28 17:56:43'),
	(279, 'Winter.Test', 'comment', '2.1.0', 'Add \'Records\' section for testing of backend formwidgets', '2023-03-28 17:56:43'),
	(280, 'Winter.User', 'script', '1.0.1', 'v1.0.1/create_users_table.php', '2023-03-28 17:56:45'),
	(281, 'Winter.User', 'script', '1.0.1', 'v1.0.1/create_throttle_table.php', '2023-03-28 17:56:46'),
	(282, 'Winter.User', 'comment', '1.0.1', 'Initialize plugin.', '2023-03-28 17:56:47'),
	(283, 'Winter.User', 'comment', '1.0.2', 'Seed tables.', '2023-03-28 17:56:47'),
	(284, 'Winter.User', 'comment', '1.0.3', 'Translated hard-coded text to language strings.', '2023-03-28 17:56:47'),
	(285, 'Winter.User', 'comment', '1.0.4', 'Improvements to user-interface for Location manager.', '2023-03-28 17:56:47'),
	(286, 'Winter.User', 'comment', '1.0.5', 'Added contact details for users.', '2023-03-28 17:56:47'),
	(287, 'Winter.User', 'script', '1.0.6', 'v1.0.6/create_mail_blockers_table.php', '2023-03-28 17:56:49'),
	(288, 'Winter.User', 'comment', '1.0.6', 'Added Mail Blocker utility so users can block specific mail templates.', '2023-03-28 17:56:49'),
	(289, 'Winter.User', 'comment', '1.0.7', 'Add back-end Settings page.', '2023-03-28 17:56:49'),
	(290, 'Winter.User', 'comment', '1.0.8', 'Updated the Settings page.', '2023-03-28 17:56:50'),
	(291, 'Winter.User', 'comment', '1.0.9', 'Adds new welcome mail message for users and administrators.', '2023-03-28 17:56:50'),
	(292, 'Winter.User', 'comment', '1.0.10', 'Adds administrator-only activation mode.', '2023-03-28 17:56:50'),
	(293, 'Winter.User', 'script', '1.0.11', 'v1.0.11/users_add_login_column.php', '2023-03-28 17:56:53'),
	(294, 'Winter.User', 'comment', '1.0.11', 'Users now have an optional login field that defaults to the email field.', '2023-03-28 17:56:53'),
	(295, 'Winter.User', 'script', '1.0.12', 'v1.0.12/users_rename_login_to_username.php', '2023-03-28 17:56:53'),
	(296, 'Winter.User', 'comment', '1.0.12', 'Create a dedicated setting for choosing the login mode.', '2023-03-28 17:56:53'),
	(297, 'Winter.User', 'comment', '1.0.13', 'Minor fix to the Account sign in logic.', '2023-03-28 17:56:53'),
	(298, 'Winter.User', 'comment', '1.0.14', 'Minor improvements to the code.', '2023-03-28 17:56:54'),
	(299, 'Winter.User', 'script', '1.0.15', 'v1.0.15/users_add_surname.php', '2023-03-28 17:56:55'),
	(300, 'Winter.User', 'comment', '1.0.15', 'Adds last name column to users table (surname).', '2023-03-28 17:56:55'),
	(301, 'Winter.User', 'comment', '1.0.16', 'Require permissions for settings page too.', '2023-03-28 17:56:55'),
	(302, 'Winter.User', 'comment', '1.1.0', '!!! Profile fields and Locations have been removed.', '2023-03-28 17:56:55'),
	(303, 'Winter.User', 'script', '1.1.1', 'v1.1.1/create_user_groups_table.php', '2023-03-28 17:57:04'),
	(304, 'Winter.User', 'script', '1.1.1', 'v1.1.1/seed_user_groups_table.php', '2023-03-28 17:57:04'),
	(305, 'Winter.User', 'comment', '1.1.1', 'Users can now be added to groups.', '2023-03-28 17:57:04'),
	(306, 'Winter.User', 'comment', '1.1.2', 'A raw URL can now be passed as the redirect property in the Account component.', '2023-03-28 17:57:04'),
	(307, 'Winter.User', 'comment', '1.1.3', 'Adds a super user flag to the users table, reserved for future use.', '2023-03-28 17:57:04'),
	(308, 'Winter.User', 'comment', '1.1.4', 'User list can be filtered by the group they belong to.', '2023-03-28 17:57:04'),
	(309, 'Winter.User', 'comment', '1.1.5', 'Adds a new permission to hide the User settings menu item.', '2023-03-28 17:57:05'),
	(310, 'Winter.User', 'script', '1.2.0', 'v1.2.0/users_add_deleted_at.php', '2023-03-28 17:57:09'),
	(311, 'Winter.User', 'comment', '1.2.0', 'Users can now deactivate their own accounts.', '2023-03-28 17:57:09'),
	(312, 'Winter.User', 'comment', '1.2.1', 'New feature for checking if a user is recently active/online.', '2023-03-28 17:57:09'),
	(313, 'Winter.User', 'comment', '1.2.2', 'Add bulk action button to user list.', '2023-03-28 17:57:09'),
	(314, 'Winter.User', 'comment', '1.2.3', 'Included some descriptive paragraphs in the Reset Password component markup.', '2023-03-28 17:57:09'),
	(315, 'Winter.User', 'comment', '1.2.4', 'Added a checkbox for blocking all mail sent to the user.', '2023-03-28 17:57:10'),
	(316, 'Winter.User', 'script', '1.2.5', 'v1.2.5/update_timestamp_nullable.php', '2023-03-28 17:57:10'),
	(317, 'Winter.User', 'comment', '1.2.5', 'Database maintenance. Updated all timestamp columns to be nullable.', '2023-03-28 17:57:10'),
	(318, 'Winter.User', 'script', '1.2.6', 'v1.2.6/users_add_last_seen.php', '2023-03-28 17:57:12'),
	(319, 'Winter.User', 'comment', '1.2.6', 'Add a dedicated last seen column for users.', '2023-03-28 17:57:12'),
	(320, 'Winter.User', 'comment', '1.2.7', 'Minor fix to user timestamp attributes.', '2023-03-28 17:57:13'),
	(321, 'Winter.User', 'comment', '1.2.8', 'Add date range filter to users list. Introduced a logout event.', '2023-03-28 17:57:13'),
	(322, 'Winter.User', 'comment', '1.2.9', 'Add invitation mail for new accounts created in the back-end.', '2023-03-28 17:57:14'),
	(323, 'Winter.User', 'script', '1.3.0', 'v1.3.0/users_add_guest_flag.php', '2023-03-28 17:57:22'),
	(324, 'Winter.User', 'script', '1.3.0', 'v1.3.0/users_add_superuser_flag.php', '2023-03-28 17:57:25'),
	(325, 'Winter.User', 'comment', '1.3.0', 'Introduced guest user accounts.', '2023-03-28 17:57:25'),
	(326, 'Winter.User', 'comment', '1.3.1', 'User notification variables can now be extended.', '2023-03-28 17:57:32'),
	(327, 'Winter.User', 'comment', '1.3.2', 'Minor fix to the Auth::register method.', '2023-03-28 17:57:32'),
	(328, 'Winter.User', 'comment', '1.3.3', 'Allow prevention of concurrent user sessions via the user settings.', '2023-03-28 17:57:32'),
	(329, 'Winter.User', 'comment', '1.3.4', 'Added force secure protocol property to the account component.', '2023-03-28 17:57:32'),
	(330, 'Winter.User', 'comment', '1.4.0', '!!! The Notifications tab in User settings has been removed.', '2023-03-28 17:57:32'),
	(331, 'Winter.User', 'comment', '1.4.1', 'Added support for user impersonation.', '2023-03-28 17:57:33'),
	(332, 'Winter.User', 'comment', '1.4.2', 'Fixes security bug in Password Reset component.', '2023-03-28 17:57:33'),
	(333, 'Winter.User', 'comment', '1.4.3', 'Fixes session handling for AJAX requests.', '2023-03-28 17:57:33'),
	(334, 'Winter.User', 'comment', '1.4.4', 'Fixes bug where impersonation touches the last seen timestamp.', '2023-03-28 17:57:33'),
	(335, 'Winter.User', 'comment', '1.4.5', 'Added token fallback process to Account / Reset Password components when parameter is missing.', '2023-03-28 17:57:33'),
	(336, 'Winter.User', 'comment', '1.4.6', 'Fixes Auth::register method signature mismatch with core Winter CMS Auth library', '2023-03-28 17:57:34'),
	(337, 'Winter.User', 'comment', '1.4.7', 'Fixes redirect bug in Account component / Update translations and separate user and group management.', '2023-03-28 17:57:35'),
	(338, 'Winter.User', 'comment', '1.4.8', 'Fixes a bug where calling MailBlocker::removeBlock could remove all mail blocks for the user.', '2023-03-28 17:57:35'),
	(339, 'Winter.User', 'comment', '1.5.0', '!!! Required password length is now a minimum of 8 characters. Previous passwords will not be affected until the next password change.', '2023-03-28 17:57:36'),
	(340, 'Winter.User', 'script', '1.5.1', 'v1.5.1/users_add_ip_address.php', '2023-03-28 17:57:50'),
	(341, 'Winter.User', 'comment', '1.5.1', 'User IP addresses are now logged. Introduce registration throttle.', '2023-03-28 17:57:51'),
	(342, 'Winter.User', 'comment', '1.5.2', 'Whitespace from usernames is now trimmed, allowed for username to be added to Reset Password mail templates.', '2023-03-28 17:57:51'),
	(343, 'Winter.User', 'comment', '1.5.3', 'Fixes a bug in the user update functionality if password is not changed. Added highlighting for banned users in user list.', '2023-03-28 17:57:52'),
	(344, 'Winter.User', 'comment', '1.5.4', 'Multiple translation improvements. Added view events to extend user preview and user listing toolbars.', '2023-03-28 17:57:52'),
	(345, 'Winter.User', 'script', '2.0.0', 'v2.0.0/rename_tables.php', '2023-03-28 17:57:54'),
	(346, 'Winter.User', 'comment', '2.0.0', 'Rebrand to Winter.User', '2023-03-28 17:57:54'),
	(347, 'Grcote7.Profile', 'script', '1.0.1', 'add_new_fields.php', '2023-03-28 17:58:02'),
	(348, 'Grcote7.Profile', 'comment', '1.0.1', 'Initialize plugin.', '2023-03-28 17:58:02'),
	(349, 'Grcote7.Profile', 'script', '1.0.2', 'create_profiles_table.php', '2023-03-28 17:58:05'),
	(350, 'Grcote7.Profile', 'comment', '1.0.2', 'Created profiles table', '2023-03-28 17:58:05'),
	(351, 'Grcote7.Tutos', 'comment', '1.0.1', 'Initialize plugin.', '2023-03-28 17:58:05'),
	(352, 'Grcote7.Tutos', 'script', '1.0.2', 'builder_table_create_grcote7_tutos_courses.php', '2023-03-28 17:58:06'),
	(353, 'Grcote7.Tutos', 'comment', '1.0.2', 'Created table grcote7_tutos_courses', '2023-03-28 17:58:06'),
	(354, 'Grcote7.Tutos', 'script', '1.0.3', 'builder_table_create_grcote7_tutos_tutos.php', '2023-03-28 17:58:08'),
	(355, 'Grcote7.Tutos', 'comment', '1.0.3', 'Created table grcote7_tutos_tutos', '2023-03-28 17:58:08'),
	(356, 'Grcote7.Tutos', 'script', '1.0.4', 'builder_table_update_grcote7_tutos_courses.php', '2023-03-28 17:58:12'),
	(357, 'Grcote7.Tutos', 'comment', '1.0.4', 'Updated table grcote7_tutos_courses', '2023-03-28 17:58:12'),
	(358, 'Grcote7.Tutos', 'script', '1.0.5', 'builder_table_update_grcote7_tutos_courses_2.php', '2023-03-28 17:58:13'),
	(359, 'Grcote7.Tutos', 'comment', '1.0.5', 'Updated table grcote7_tutos_courses', '2023-03-28 17:58:14'),
	(360, 'Grcote7.Tutos', 'script', '1.0.6', 'builder_table_create_grcote7_tutos_categories.php', '2023-03-28 17:58:16'),
	(361, 'Grcote7.Tutos', 'comment', '1.0.6', 'Created table grcote7_tutos_categories', '2023-03-28 17:58:16'),
	(362, 'Grcote7.Tutos', 'script', '1.0.7', 'builder_table_create_grcote7_tutos_tutos_categories.php', '2023-03-28 17:58:19'),
	(363, 'Grcote7.Tutos', 'comment', '1.0.7', 'Created table grcote7_tutos_tutos_categories', '2023-03-28 17:58:19'),
	(364, 'Grcote7.Tutos', 'script', '1.0.8', 'builder_table_update_grcote7_tutos_tutos.php', '2023-03-28 17:58:20'),
	(365, 'Grcote7.Tutos', 'comment', '1.0.8', 'Updated table grcote7_tutos_tutos', '2023-03-28 17:58:21'),
	(366, 'Grcote7.Tutos', 'script', '1.0.9', 'builder_table_update_grcote7_tutos_tutos_2.php', '2023-03-28 17:58:22'),
	(367, 'Grcote7.Tutos', 'comment', '1.0.9', 'Updated table grcote7_tutos_tutos', '2023-03-28 17:58:22');
/*!40000 ALTER TABLE `system_plugin_history` ENABLE KEYS */;

-- Listage de la structure de la table aadminli_c57fr. system_plugin_versions
CREATE TABLE IF NOT EXISTS `system_plugin_versions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `version` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `is_disabled` tinyint(1) NOT NULL DEFAULT '0',
  `is_frozen` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `system_plugin_versions_code_index` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table aadminli_c57fr.system_plugin_versions : ~23 rows (environ)
/*!40000 ALTER TABLE `system_plugin_versions` DISABLE KEYS */;
INSERT INTO `system_plugin_versions` (`id`, `code`, `version`, `created_at`, `is_disabled`, `is_frozen`) VALUES
	(1, 'Alxy.SimpleMenu', '1.0.1', '2023-03-28 17:54:56', 0, 0),
	(2, 'GC7.Librairy', '1.0.1', '2023-03-28 17:54:57', 0, 0),
	(3, 'Grcote7.Books', '1.0.7', '2023-03-28 17:55:04', 0, 0),
	(4, 'GrCOTE7.BrandC57', '1.0.1', '2023-03-28 17:55:04', 0, 0),
	(5, 'GrCOTE7.C57', '1.0.2', '2023-03-28 17:55:05', 0, 0),
	(6, 'Grcote7.Contact', '1.0.1', '2023-03-28 17:55:05', 0, 0),
	(7, 'GrCOTE7.Demos', '1.0.3', '2023-03-28 17:55:05', 0, 0),
	(8, 'GrCOTE7.Dpt', '1.0.2', '2023-03-28 17:55:06', 0, 0),
	(9, 'Grcote7.Library', '1.0.2', '2023-03-28 17:55:07', 0, 0),
	(10, 'Grcote7.Movies', '1.0.15', '2023-03-28 17:55:28', 0, 0),
	(11, 'GrCOTE7.Php', '1.0.1', '2023-03-28 17:55:28', 0, 0),
	(12, 'Grcote7.Vehicules', '1.0.1', '2023-03-28 17:55:28', 0, 0),
	(13, 'Marcomessa.Vite', '1.0.1', '2023-03-28 17:55:28', 0, 0),
	(14, 'RainLab.GoogleAnalytics', '1.3.0', '2023-03-28 17:55:30', 0, 0),
	(15, 'RainLab.Sitemap', '1.0.8', '2023-03-28 17:55:32', 0, 0),
	(16, 'Winter.Blog', '2.0.0', '2023-03-28 17:55:50', 0, 0),
	(17, 'Winter.Builder', '2.0.0', '2023-03-28 17:55:55', 0, 0),
	(18, 'Winter.Debugbar', '4.0.2', '2023-03-28 17:55:57', 0, 0),
	(19, 'Winter.Demo', '1.0.1', '2023-03-28 17:55:57', 0, 0),
	(20, 'Winter.Notes', '1.0.5', '2023-03-28 17:55:59', 0, 0),
	(21, 'Winter.Pages', '2.0.2', '2023-03-28 17:56:09', 0, 0),
	(22, 'Winter.Test', '2.1.0', '2023-03-28 17:56:43', 0, 0),
	(23, 'Winter.User', '2.0.0', '2023-03-28 17:57:54', 0, 0),
	(24, 'Grcote7.Profile', '1.0.2', '2023-03-28 17:58:05', 0, 0),
	(25, 'Grcote7.Tutos', '1.0.9', '2023-03-28 17:58:22', 0, 0);
/*!40000 ALTER TABLE `system_plugin_versions` ENABLE KEYS */;

-- Listage de la structure de la table aadminli_c57fr. system_request_logs
CREATE TABLE IF NOT EXISTS `system_request_logs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `status_code` int(11) DEFAULT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `referer` text COLLATE utf8mb4_unicode_ci,
  `count` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table aadminli_c57fr.system_request_logs : ~0 rows (environ)
/*!40000 ALTER TABLE `system_request_logs` DISABLE KEYS */;
/*!40000 ALTER TABLE `system_request_logs` ENABLE KEYS */;

-- Listage de la structure de la table aadminli_c57fr. system_revisions
CREATE TABLE IF NOT EXISTS `system_revisions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned DEFAULT NULL,
  `field` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cast` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `old_value` text COLLATE utf8mb4_unicode_ci,
  `new_value` text COLLATE utf8mb4_unicode_ci,
  `revisionable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revisionable_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `system_revisions_revisionable_id_revisionable_type_index` (`revisionable_id`,`revisionable_type`),
  KEY `system_revisions_user_id_index` (`user_id`),
  KEY `system_revisions_field_index` (`field`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table aadminli_c57fr.system_revisions : ~0 rows (environ)
/*!40000 ALTER TABLE `system_revisions` DISABLE KEYS */;
/*!40000 ALTER TABLE `system_revisions` ENABLE KEYS */;

-- Listage de la structure de la table aadminli_c57fr. system_settings
CREATE TABLE IF NOT EXISTS `system_settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `item` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `value` mediumtext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `system_settings_item_index` (`item`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table aadminli_c57fr.system_settings : ~1 rows (environ)
/*!40000 ALTER TABLE `system_settings` DISABLE KEYS */;
INSERT INTO `system_settings` (`id`, `item`, `value`) VALUES
	(1, 'backend_brand_settings', '{"app_name":"C57","app_tagline":"Soyez Partout !","primary_color":"#34495E","secondary_color":"#E67E22","accent_color":"#3498DB","default_colors":[{"color":"#1ABC9C"},{"color":"#16A085"},{"color":"#2ECC71"},{"color":"#27AE60"},{"color":"#3498DB"},{"color":"#2980B9"},{"color":"#9B59B6"},{"color":"#8E44AD"},{"color":"#34495E"},{"color":"#2B3E50"},{"color":"#F1C40F"},{"color":"#F39C12"},{"color":"#E67E22"},{"color":"#D35400"},{"color":"#E74C3C"},{"color":"#C0392B"},{"color":"#ECF0F1"},{"color":"#BDC3C7"},{"color":"#95A5A6"},{"color":"#7F8C8D"}],"menu_mode":"inline","custom_css":""}');
/*!40000 ALTER TABLE `system_settings` ENABLE KEYS */;

-- Listage de la structure de la table aadminli_c57fr. users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `activation_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `persist_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reset_password_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permissions` text COLLATE utf8mb4_unicode_ci,
  `is_activated` tinyint(1) NOT NULL DEFAULT '0',
  `activated_at` timestamp NULL DEFAULT NULL,
  `last_login` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `surname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `last_seen` timestamp NULL DEFAULT NULL,
  `is_guest` tinyint(1) NOT NULL DEFAULT '0',
  `is_superuser` tinyint(1) NOT NULL DEFAULT '0',
  `created_ip_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_ip_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `facebook` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bio` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  UNIQUE KEY `users_login_unique` (`username`),
  KEY `users_activation_code_index` (`activation_code`),
  KEY `users_reset_password_code_index` (`reset_password_code`),
  KEY `users_login_index` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table aadminli_c57fr.users : ~0 rows (environ)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

-- Listage de la structure de la table aadminli_c57fr. users_groups
CREATE TABLE IF NOT EXISTS `users_groups` (
  `user_id` int(10) unsigned NOT NULL,
  `user_group_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`user_id`,`user_group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table aadminli_c57fr.users_groups : ~0 rows (environ)
/*!40000 ALTER TABLE `users_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `users_groups` ENABLE KEYS */;

-- Listage de la structure de la table aadminli_c57fr. user_groups
CREATE TABLE IF NOT EXISTS `user_groups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_groups_code_index` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table aadminli_c57fr.user_groups : ~2 rows (environ)
/*!40000 ALTER TABLE `user_groups` DISABLE KEYS */;
INSERT INTO `user_groups` (`id`, `name`, `code`, `description`, `created_at`, `updated_at`) VALUES
	(1, 'Guest', 'guest', 'Default group for guest users.', '2023-03-28 17:57:04', '2023-03-28 17:57:04'),
	(2, 'Registered', 'registered', 'Default group for registered users.', '2023-03-28 17:57:04', '2023-03-28 17:57:04');
/*!40000 ALTER TABLE `user_groups` ENABLE KEYS */;

-- Listage de la structure de la table aadminli_c57fr. user_throttle
CREATE TABLE IF NOT EXISTS `user_throttle` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned DEFAULT NULL,
  `ip_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attempts` int(11) NOT NULL DEFAULT '0',
  `last_attempt_at` timestamp NULL DEFAULT NULL,
  `is_suspended` tinyint(1) NOT NULL DEFAULT '0',
  `suspended_at` timestamp NULL DEFAULT NULL,
  `is_banned` tinyint(1) NOT NULL DEFAULT '0',
  `banned_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_throttle_user_id_index` (`user_id`),
  KEY `user_throttle_ip_address_index` (`ip_address`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table aadminli_c57fr.user_throttle : ~0 rows (environ)
/*!40000 ALTER TABLE `user_throttle` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_throttle` ENABLE KEYS */;

-- Listage de la structure de la table aadminli_c57fr. winter_blog_categories
CREATE TABLE IF NOT EXISTS `winter_blog_categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `nest_left` int(11) DEFAULT NULL,
  `nest_right` int(11) DEFAULT NULL,
  `nest_depth` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `rainlab_blog_categories_slug_index` (`slug`),
  KEY `rainlab_blog_categories_parent_id_index` (`parent_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table aadminli_c57fr.winter_blog_categories : ~0 rows (environ)
/*!40000 ALTER TABLE `winter_blog_categories` DISABLE KEYS */;
INSERT INTO `winter_blog_categories` (`id`, `name`, `slug`, `code`, `description`, `parent_id`, `nest_left`, `nest_right`, `nest_depth`, `created_at`, `updated_at`) VALUES
	(1, 'Non catégorisé', 'uncategorized', NULL, NULL, NULL, 1, 2, 0, '2023-03-28 17:55:39', '2023-03-28 17:55:39');
/*!40000 ALTER TABLE `winter_blog_categories` ENABLE KEYS */;

-- Listage de la structure de la table aadminli_c57fr. winter_blog_posts
CREATE TABLE IF NOT EXISTS `winter_blog_posts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `excerpt` text COLLATE utf8mb4_unicode_ci,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `content_html` longtext COLLATE utf8mb4_unicode_ci,
  `published_at` timestamp NULL DEFAULT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `metadata` mediumtext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `rainlab_blog_posts_user_id_index` (`user_id`),
  KEY `rainlab_blog_posts_slug_index` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table aadminli_c57fr.winter_blog_posts : ~1 rows (environ)
/*!40000 ALTER TABLE `winter_blog_posts` DISABLE KEYS */;
INSERT INTO `winter_blog_posts` (`id`, `user_id`, `title`, `slug`, `excerpt`, `content`, `content_html`, `published_at`, `published`, `created_at`, `updated_at`, `metadata`) VALUES
	(1, 2, 'First blog post', 'first-blog-post', 'The first ever blog post is here. It might be a good idea to update this post with some more relevant content.', 'This is your first ever **blog post**! It might be a good idea to update this post with some more relevant content.\r\n\r\nYou can edit this content by selecting **Blog** from the administration back-end menu.\r\n\r\n*Enjoy the good times!*', '<p>This is your first ever <strong>blog post</strong>! It might be a good idea to update this post with some more relevant content.</p>\n<p>You can edit this content by selecting <strong>Blog</strong> from the administration back-end menu.</p>\n<p><em>Enjoy the good times!</em></p>', '2023-03-28 17:55:37', 1, '2023-03-28 17:55:39', '2023-03-28 17:55:39', NULL);
/*!40000 ALTER TABLE `winter_blog_posts` ENABLE KEYS */;

-- Listage de la structure de la table aadminli_c57fr. winter_blog_posts_categories
CREATE TABLE IF NOT EXISTS `winter_blog_posts_categories` (
  `post_id` int(10) unsigned NOT NULL,
  `category_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`post_id`,`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table aadminli_c57fr.winter_blog_posts_categories : ~0 rows (environ)
/*!40000 ALTER TABLE `winter_blog_posts_categories` DISABLE KEYS */;
/*!40000 ALTER TABLE `winter_blog_posts_categories` ENABLE KEYS */;

-- Listage de la structure de la table aadminli_c57fr. winter_notes_notes
CREATE TABLE IF NOT EXISTS `winter_notes_notes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `target_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `target_id` int(10) unsigned DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` mediumtext COLLATE utf8mb4_unicode_ci,
  `additional_data` mediumtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `winter_notes_notes_target_id_index` (`target_id`),
  KEY `winter_notes_notes_name_index` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table aadminli_c57fr.winter_notes_notes : ~0 rows (environ)
/*!40000 ALTER TABLE `winter_notes_notes` DISABLE KEYS */;
/*!40000 ALTER TABLE `winter_notes_notes` ENABLE KEYS */;

-- Listage de la structure de la table aadminli_c57fr. winter_test_attributes
CREATE TABLE IF NOT EXISTS `winter_test_attributes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `label` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_default` tinyint(1) NOT NULL DEFAULT '0',
  `sort_order` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table aadminli_c57fr.winter_test_attributes : ~0 rows (environ)
/*!40000 ALTER TABLE `winter_test_attributes` DISABLE KEYS */;
/*!40000 ALTER TABLE `winter_test_attributes` ENABLE KEYS */;

-- Listage de la structure de la table aadminli_c57fr. winter_test_categories
CREATE TABLE IF NOT EXISTS `winter_test_categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sort_order` int(11) DEFAULT NULL,
  `is_visible` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `winter_test_categories_parent_id_index` (`parent_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table aadminli_c57fr.winter_test_categories : ~11 rows (environ)
/*!40000 ALTER TABLE `winter_test_categories` DISABLE KEYS */;
INSERT INTO `winter_test_categories` (`id`, `parent_id`, `name`, `description`, `sort_order`, `is_visible`, `created_at`, `updated_at`) VALUES
	(1, NULL, 'Web', 'Websites & Web Applications', 1, 1, '2023-03-28 17:56:35', '2023-03-28 17:56:35'),
	(2, 1, 'Create a website', NULL, 2, 1, '2023-03-28 17:56:35', '2023-03-28 17:56:35'),
	(3, 1, 'Convert a template to a website', NULL, 3, 1, '2023-03-28 17:56:36', '2023-03-28 17:56:36'),
	(4, NULL, 'Desktop', 'Desktop Software', 4, 1, '2023-03-28 17:56:36', '2023-03-28 17:56:36'),
	(5, 4, 'Write software for Windows', NULL, 5, 1, '2023-03-28 17:56:36', '2023-03-28 17:56:36'),
	(6, 4, 'Write software for Mac', NULL, 6, 1, '2023-03-28 17:56:36', '2023-03-28 17:56:36'),
	(7, 4, 'Write software for Linux', NULL, 7, 1, '2023-03-28 17:56:36', '2023-03-28 17:56:36'),
	(8, NULL, 'Mobile', 'Mobile Apps', 8, 1, '2023-03-28 17:56:36', '2023-03-28 17:56:36'),
	(9, 8, 'Write software for iPhone / iPad', NULL, 9, 1, '2023-03-28 17:56:36', '2023-03-28 17:56:36'),
	(10, 8, 'Write software for Android', NULL, 10, 1, '2023-03-28 17:56:36', '2023-03-28 17:56:36'),
	(11, 8, 'Create a mobile website', NULL, 11, 1, '2023-03-28 17:56:36', '2023-03-28 17:56:37');
/*!40000 ALTER TABLE `winter_test_categories` ENABLE KEYS */;

-- Listage de la structure de la table aadminli_c57fr. winter_test_channels
CREATE TABLE IF NOT EXISTS `winter_test_channels` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nest_left` int(11) DEFAULT NULL,
  `nest_right` int(11) DEFAULT NULL,
  `nest_depth` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `winter_test_channels_parent_id_index` (`parent_id`),
  KEY `winter_test_channels_user_id_index` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table aadminli_c57fr.winter_test_channels : ~9 rows (environ)
/*!40000 ALTER TABLE `winter_test_channels` DISABLE KEYS */;
INSERT INTO `winter_test_channels` (`id`, `parent_id`, `user_id`, `title`, `description`, `nest_left`, `nest_right`, `nest_depth`, `created_at`, `updated_at`) VALUES
	(1, NULL, 1, 'Channel Orange', 'A root level forum channel', 1, 12, 0, '2023-03-28 17:56:33', '2023-03-28 17:56:34'),
	(2, 1, NULL, 'Autumn Leaves', 'Disccusion about the season of falling leaves.', 2, 9, 1, '2023-03-28 17:56:33', '2023-03-28 17:56:34'),
	(3, 2, NULL, 'September', 'The start of the fall season.', 3, 4, 2, '2023-03-28 17:56:34', '2023-03-28 17:56:34'),
	(4, 2, NULL, 'Winter', 'The middle of the fall season.', 5, 6, 2, '2023-03-28 17:56:34', '2023-03-28 17:56:34'),
	(5, 2, NULL, 'November', 'The end of the fall season.', 7, 8, 2, '2023-03-28 17:56:34', '2023-03-28 17:56:34'),
	(6, 1, NULL, 'Summer Breeze', 'Disccusion about the wind at the ocean.', 10, 11, 1, '2023-03-28 17:56:34', '2023-03-28 17:56:35'),
	(7, NULL, 1, 'Channel Green', 'A root level forum channel', 13, 18, 0, '2023-03-28 17:56:35', '2023-03-28 17:56:35'),
	(8, 7, NULL, 'Winter Snow', 'Disccusion about the frosty snow flakes.', 14, 15, 1, '2023-03-28 17:56:35', '2023-03-28 17:56:35'),
	(9, 7, NULL, 'Spring Trees', 'Disccusion about the blooming gardens.', 16, 17, 1, '2023-03-28 17:56:35', '2023-03-28 17:56:35');
/*!40000 ALTER TABLE `winter_test_channels` ENABLE KEYS */;

-- Listage de la structure de la table aadminli_c57fr. winter_test_cities
CREATE TABLE IF NOT EXISTS `winter_test_cities` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `country_id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table aadminli_c57fr.winter_test_cities : ~8 rows (environ)
/*!40000 ALTER TABLE `winter_test_cities` DISABLE KEYS */;
INSERT INTO `winter_test_cities` (`id`, `country_id`, `name`) VALUES
	(1, 1, 'Regina'),
	(2, 1, 'Vancouver'),
	(3, 1, 'Toronto'),
	(4, 1, 'Ottawa'),
	(5, 2, 'New York'),
	(6, 2, 'Seattle'),
	(7, 2, 'Boston'),
	(8, 2, 'San Francisco');
/*!40000 ALTER TABLE `winter_test_cities` ENABLE KEYS */;

-- Listage de la structure de la table aadminli_c57fr. winter_test_comments
CREATE TABLE IF NOT EXISTS `winter_test_comments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci,
  `content_md` text COLLATE utf8mb4_unicode_ci,
  `content_html` text COLLATE utf8mb4_unicode_ci,
  `breakdown` text COLLATE utf8mb4_unicode_ci,
  `mood` text COLLATE utf8mb4_unicode_ci,
  `quotes` text COLLATE utf8mb4_unicode_ci,
  `is_visible` tinyint(1) NOT NULL DEFAULT '1',
  `post_id` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `winter_test_comments_post_id_index` (`post_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table aadminli_c57fr.winter_test_comments : ~2 rows (environ)
/*!40000 ALTER TABLE `winter_test_comments` DISABLE KEYS */;
INSERT INTO `winter_test_comments` (`id`, `name`, `content`, `content_md`, `content_html`, `breakdown`, `mood`, `quotes`, `is_visible`, `post_id`, `created_at`, `updated_at`) VALUES
	(1, 'deadmau5', 'Hai fwiend, hai fwiend, hai fwiend, hai fwiend, hai fwiend. Brrrrrup bloop. Brrrrrp bloop. Brrrrrp bloop. Brrrrrp bloop.', NULL, NULL, NULL, NULL, NULL, 1, 1, '2023-03-28 17:56:31', '2023-03-28 17:56:31'),
	(2, 'Hidden comment', 'This comment should be hidden as defined in the relationship.', NULL, NULL, NULL, NULL, NULL, 0, 1, '2023-03-28 17:56:31', '2023-03-28 17:56:31');
/*!40000 ALTER TABLE `winter_test_comments` ENABLE KEYS */;

-- Listage de la structure de la table aadminli_c57fr. winter_test_countries
CREATE TABLE IF NOT EXISTS `winter_test_countries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci,
  `pages` text COLLATE utf8mb4_unicode_ci,
  `states` text COLLATE utf8mb4_unicode_ci,
  `locations` text COLLATE utf8mb4_unicode_ci,
  `language` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `currency` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table aadminli_c57fr.winter_test_countries : ~2 rows (environ)
/*!40000 ALTER TABLE `winter_test_countries` DISABLE KEYS */;
INSERT INTO `winter_test_countries` (`id`, `name`, `code`, `content`, `pages`, `states`, `locations`, `language`, `currency`, `is_active`, `created_at`, `updated_at`) VALUES
	(1, 'Petoria', 'petoria', NULL, NULL, '[{"title":"Stewie"},{"title":"Brian"},{"title":"Chris"},{"title":"Lois"},{"title":"Meg"}]', NULL, 'eh', 'btc', 0, '2023-03-28 17:56:31', '2023-03-28 17:56:31'),
	(2, 'Blueseed', 'blueseed', NULL, NULL, '[{"title":"Boaty"},{"title":"McBoat"},{"title":"Face"}]', NULL, 'bs', 'btc', 0, '2023-03-28 17:56:32', '2023-03-28 17:56:32');
/*!40000 ALTER TABLE `winter_test_countries` ENABLE KEYS */;

-- Listage de la structure de la table aadminli_c57fr. winter_test_countries_types
CREATE TABLE IF NOT EXISTS `winter_test_countries_types` (
  `country_id` int(10) unsigned NOT NULL,
  `attribute_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`country_id`,`attribute_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table aadminli_c57fr.winter_test_countries_types : ~0 rows (environ)
/*!40000 ALTER TABLE `winter_test_countries_types` DISABLE KEYS */;
/*!40000 ALTER TABLE `winter_test_countries_types` ENABLE KEYS */;

-- Listage de la structure de la table aadminli_c57fr. winter_test_galleries
CREATE TABLE IF NOT EXISTS `winter_test_galleries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `party_mode` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table aadminli_c57fr.winter_test_galleries : ~0 rows (environ)
/*!40000 ALTER TABLE `winter_test_galleries` DISABLE KEYS */;
INSERT INTO `winter_test_galleries` (`id`, `title`, `status`, `party_mode`, `created_at`, `updated_at`) VALUES
	(1, 'Featured', NULL, NULL, '2023-03-28 17:56:32', '2023-03-28 17:56:32');
/*!40000 ALTER TABLE `winter_test_galleries` ENABLE KEYS */;

-- Listage de la structure de la table aadminli_c57fr. winter_test_gallery_entity
CREATE TABLE IF NOT EXISTS `winter_test_gallery_entity` (
  `gallery_id` int(10) unsigned NOT NULL,
  `entity_id` int(10) unsigned NOT NULL,
  `entity_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`gallery_id`,`entity_id`,`entity_type`),
  KEY `gallery_id_idx` (`gallery_id`),
  KEY `entity_id_idx` (`entity_id`),
  KEY `entity_type_idx` (`entity_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table aadminli_c57fr.winter_test_gallery_entity : ~0 rows (environ)
/*!40000 ALTER TABLE `winter_test_gallery_entity` DISABLE KEYS */;
INSERT INTO `winter_test_gallery_entity` (`gallery_id`, `entity_id`, `entity_type`) VALUES
	(1, 1, 'Winter\\Test\\Models\\Post');
/*!40000 ALTER TABLE `winter_test_gallery_entity` ENABLE KEYS */;

-- Listage de la structure de la table aadminli_c57fr. winter_test_locations
CREATE TABLE IF NOT EXISTS `winter_test_locations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `country_id` int(10) unsigned NOT NULL,
  `city_id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table aadminli_c57fr.winter_test_locations : ~8 rows (environ)
/*!40000 ALTER TABLE `winter_test_locations` DISABLE KEYS */;
INSERT INTO `winter_test_locations` (`id`, `country_id`, `city_id`, `name`) VALUES
	(1, 1, 1, '240 5th Ave'),
	(2, 1, 2, '101 McKay Street'),
	(3, 1, 3, '123 Nowhere Lane'),
	(4, 1, 4, '10099 Bob Loop'),
	(5, 2, 5, '9442 Scary Street'),
	(6, 2, 6, '5309 Imagination Crescrent'),
	(7, 2, 7, '22 2201 Seymour Drive'),
	(8, 2, 8, '2322 Xray Alphabet');
/*!40000 ALTER TABLE `winter_test_locations` ENABLE KEYS */;

-- Listage de la structure de la table aadminli_c57fr. winter_test_members
CREATE TABLE IF NOT EXISTS `winter_test_members` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `winter_test_members_parent_id_index` (`parent_id`),
  KEY `winter_test_members_user_id_index` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table aadminli_c57fr.winter_test_members : ~7 rows (environ)
/*!40000 ALTER TABLE `winter_test_members` DISABLE KEYS */;
INSERT INTO `winter_test_members` (`id`, `parent_id`, `user_id`, `name`, `created_at`, `updated_at`) VALUES
	(1, NULL, NULL, 'Khaleesi', '2023-03-28 17:56:33', '2023-03-28 17:56:33'),
	(2, 1, NULL, 'Ian', '2023-03-28 17:56:33', '2023-03-28 17:56:33'),
	(3, 2, NULL, 'Vangelis', '2023-03-28 17:56:33', '2023-03-28 17:56:33'),
	(4, 3, NULL, 'Joe', '2023-03-28 17:56:33', '2023-03-28 17:56:33'),
	(5, 4, 1, 'Johnny', '2023-03-28 17:56:33', '2023-03-28 17:56:33'),
	(6, 4, 1, 'Sally', '2023-03-28 17:56:33', '2023-03-28 17:56:33'),
	(7, 4, 1, 'Rick', '2023-03-28 17:56:33', '2023-03-28 17:56:33');
/*!40000 ALTER TABLE `winter_test_members` ENABLE KEYS */;

-- Listage de la structure de la table aadminli_c57fr. winter_test_meta
CREATE TABLE IF NOT EXISTS `winter_test_meta` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `taggable_id` int(10) unsigned DEFAULT NULL,
  `taggable_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_keywords` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `canonical_url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect_url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `robot_index` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `robot_follow` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `winter_test_meta_taggable_id_index` (`taggable_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table aadminli_c57fr.winter_test_meta : ~0 rows (environ)
/*!40000 ALTER TABLE `winter_test_meta` DISABLE KEYS */;
/*!40000 ALTER TABLE `winter_test_meta` ENABLE KEYS */;

-- Listage de la structure de la table aadminli_c57fr. winter_test_pages
CREATE TABLE IF NOT EXISTS `winter_test_pages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` int(10) unsigned NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci,
  `enabled` tinyint(1) NOT NULL DEFAULT '0',
  `hidden` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table aadminli_c57fr.winter_test_pages : ~2 rows (environ)
/*!40000 ALTER TABLE `winter_test_pages` DISABLE KEYS */;
INSERT INTO `winter_test_pages` (`id`, `title`, `slug`, `type`, `content`, `enabled`, `hidden`) VALUES
	(1, NULL, NULL, 1, '{"title":"This is a simple page","content":"<h1>Hello, World<\\/h1>"}', 0, 0),
	(2, NULL, NULL, 2, '{"title":"This is a complex page","content":"<h1>Hello, Complex World<\\/h1>","meta_description":"Meta Description","meta_tags":["WinterCMS","Fun"],"colors":{"primary":"#ff0000","secondary":"#00ff00"}}', 0, 0);
/*!40000 ALTER TABLE `winter_test_pages` ENABLE KEYS */;

-- Listage de la structure de la table aadminli_c57fr. winter_test_people
CREATE TABLE IF NOT EXISTS `winter_test_people` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `preferred_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bio` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `expenses` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `birth` datetime DEFAULT NULL,
  `birthtime` time DEFAULT NULL,
  `birthdate` date DEFAULT NULL,
  `favcolor` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hobbies` text COLLATE utf8mb4_unicode_ci,
  `sports` text COLLATE utf8mb4_unicode_ci,
  `is_married` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table aadminli_c57fr.winter_test_people : ~6 rows (environ)
/*!40000 ALTER TABLE `winter_test_people` DISABLE KEYS */;
INSERT INTO `winter_test_people` (`id`, `name`, `preferred_name`, `bio`, `expenses`, `birth`, `birthtime`, `birthdate`, `favcolor`, `hobbies`, `sports`, `is_married`, `created_at`, `updated_at`) VALUES
	(1, 'Eddie Valiant', NULL, 'I have a phone set up already', '19999', NULL, NULL, NULL, '#5fb6f5', NULL, NULL, NULL, '2023-03-28 17:56:30', '2023-03-28 17:56:30'),
	(2, 'Baby Herman', NULL, 'I have nothing at all', '550', NULL, NULL, NULL, '#990000', NULL, NULL, NULL, '2023-03-28 17:56:30', '2023-03-28 17:56:30'),
	(3, 'Jon Doe', NULL, 'I like turtles', NULL, NULL, NULL, NULL, '#111111', NULL, NULL, NULL, '2023-03-28 17:56:30', '2023-03-28 17:56:30'),
	(4, 'John Smith', NULL, 'I like dolphins', NULL, NULL, NULL, NULL, '#222222', NULL, NULL, NULL, '2023-03-28 17:56:30', '2023-03-28 17:56:30'),
	(5, 'Jon Smith', NULL, 'I like snakes', NULL, NULL, NULL, NULL, '#333333', NULL, NULL, NULL, '2023-03-28 17:56:31', '2023-03-28 17:56:31'),
	(6, 'Mary Smith', NULL, 'I like fish', '2000', NULL, NULL, NULL, '#444444', NULL, NULL, NULL, '2023-03-28 17:56:31', '2023-03-28 17:56:31');
/*!40000 ALTER TABLE `winter_test_people` ENABLE KEYS */;

-- Listage de la structure de la table aadminli_c57fr. winter_test_phones
CREATE TABLE IF NOT EXISTS `winter_test_phones` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `brand` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `person_id` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `winter_test_phones_person_id_index` (`person_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table aadminli_c57fr.winter_test_phones : ~5 rows (environ)
/*!40000 ALTER TABLE `winter_test_phones` DISABLE KEYS */;
INSERT INTO `winter_test_phones` (`id`, `name`, `number`, `brand`, `is_active`, `person_id`, `created_at`, `updated_at`) VALUES
	(1, 'Mobile', '(360) 733-2263', NULL, 1, NULL, '2023-03-28 17:56:30', '2023-03-28 17:56:30'),
	(2, 'Home', '(360) 867-3563', NULL, 1, 1, '2023-03-28 17:56:30', '2023-03-28 17:56:30'),
	(3, 'Work', '(360) 595-2146', NULL, 1, NULL, '2023-03-28 17:56:30', '2023-03-28 17:56:30'),
	(4, 'Fax', '(360) 595-2146', NULL, 1, NULL, '2023-03-28 17:56:30', '2023-03-28 17:56:30'),
	(5, 'Inactive', '(xxx) xxx-xxx', NULL, 0, NULL, '2023-03-28 17:56:30', '2023-03-28 17:56:30');
/*!40000 ALTER TABLE `winter_test_phones` ENABLE KEYS */;

-- Listage de la structure de la table aadminli_c57fr. winter_test_plugins
CREATE TABLE IF NOT EXISTS `winter_test_plugins` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table aadminli_c57fr.winter_test_plugins : ~0 rows (environ)
/*!40000 ALTER TABLE `winter_test_plugins` DISABLE KEYS */;
INSERT INTO `winter_test_plugins` (`id`, `name`, `code`, `content`, `created_at`, `updated_at`) VALUES
	(1, 'Angular', 'angular', 'An AngularJS plugin.', '2023-03-28 17:56:32', '2023-03-28 17:56:32');
/*!40000 ALTER TABLE `winter_test_plugins` ENABLE KEYS */;

-- Listage de la structure de la table aadminli_c57fr. winter_test_posts
CREATE TABLE IF NOT EXISTS `winter_test_posts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci,
  `content_md` text COLLATE utf8mb4_unicode_ci,
  `content_html` text COLLATE utf8mb4_unicode_ci,
  `tags_array` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tags_string` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tags_array_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tags_string_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_published` tinyint(1) NOT NULL DEFAULT '0',
  `published_at` timestamp NULL DEFAULT NULL,
  `status_id` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `winter_test_posts_status_id_index` (`status_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table aadminli_c57fr.winter_test_posts : ~2 rows (environ)
/*!40000 ALTER TABLE `winter_test_posts` DISABLE KEYS */;
INSERT INTO `winter_test_posts` (`id`, `name`, `content`, `content_md`, `content_html`, `tags_array`, `tags_string`, `tags_array_id`, `tags_string_id`, `is_published`, `published_at`, `status_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 'First post, yay!', 'I have some comments!', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2023-03-28 17:56:31', '2023-03-28 17:56:31', NULL),
	(2, 'A lonely toon', 'I have nothing at all', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2023-03-28 17:56:31', '2023-03-28 17:56:31', NULL);
/*!40000 ALTER TABLE `winter_test_posts` ENABLE KEYS */;

-- Listage de la structure de la table aadminli_c57fr. winter_test_posts_tags
CREATE TABLE IF NOT EXISTS `winter_test_posts_tags` (
  `post_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table aadminli_c57fr.winter_test_posts_tags : ~0 rows (environ)
/*!40000 ALTER TABLE `winter_test_posts_tags` DISABLE KEYS */;
/*!40000 ALTER TABLE `winter_test_posts_tags` ENABLE KEYS */;

-- Listage de la structure de la table aadminli_c57fr. winter_test_records
CREATE TABLE IF NOT EXISTS `winter_test_records` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `author_id` int(10) unsigned DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'draft',
  `content` mediumtext COLLATE utf8mb4_unicode_ci,
  `additional_data` mediumtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `published_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `winter_test_records_slug_unique` (`slug`),
  KEY `winter_test_records_author_id_index` (`author_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table aadminli_c57fr.winter_test_records : ~0 rows (environ)
/*!40000 ALTER TABLE `winter_test_records` DISABLE KEYS */;
/*!40000 ALTER TABLE `winter_test_records` ENABLE KEYS */;

-- Listage de la structure de la table aadminli_c57fr. winter_test_related_channels
CREATE TABLE IF NOT EXISTS `winter_test_related_channels` (
  `channel_id` int(10) unsigned NOT NULL,
  `related_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`channel_id`,`related_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table aadminli_c57fr.winter_test_related_channels : ~0 rows (environ)
/*!40000 ALTER TABLE `winter_test_related_channels` DISABLE KEYS */;
/*!40000 ALTER TABLE `winter_test_related_channels` ENABLE KEYS */;

-- Listage de la structure de la table aadminli_c57fr. winter_test_reviews
CREATE TABLE IF NOT EXISTS `winter_test_reviews` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_id` int(10) unsigned DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci,
  `is_positive` tinyint(1) DEFAULT NULL,
  `rating` int(10) unsigned NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table aadminli_c57fr.winter_test_reviews : ~2 rows (environ)
/*!40000 ALTER TABLE `winter_test_reviews` DISABLE KEYS */;
INSERT INTO `winter_test_reviews` (`id`, `product_type`, `product_id`, `content`, `is_positive`, `rating`, `created_at`, `updated_at`) VALUES
	(1, NULL, NULL, 'Orphan review', 0, 2, '2023-03-28 17:56:32', '2023-03-28 17:56:32'),
	(2, 'Winter\\Test\\Models\\Theme', 1, 'Excellent design work', 1, 4, '2023-03-28 17:56:32', '2023-03-28 17:56:32');
/*!40000 ALTER TABLE `winter_test_reviews` ENABLE KEYS */;

-- Listage de la structure de la table aadminli_c57fr. winter_test_roles
CREATE TABLE IF NOT EXISTS `winter_test_roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table aadminli_c57fr.winter_test_roles : ~3 rows (environ)
/*!40000 ALTER TABLE `winter_test_roles` DISABLE KEYS */;
INSERT INTO `winter_test_roles` (`id`, `name`, `description`, `created_at`, `updated_at`) VALUES
	(1, 'Chief Executive Orangutan', 'You can call this person CEO for short', '2023-03-28 17:56:31', '2023-03-28 17:56:31'),
	(2, 'Chief Friendship Organiser', 'You can call this person CFO for short', '2023-03-28 17:56:31', '2023-03-28 17:56:31'),
	(3, 'Caring Technical Officer', 'You can call this person CTO for short', '2023-03-28 17:56:31', '2023-03-28 17:56:31');
/*!40000 ALTER TABLE `winter_test_roles` ENABLE KEYS */;

-- Listage de la structure de la table aadminli_c57fr. winter_test_tags
CREATE TABLE IF NOT EXISTS `winter_test_tags` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table aadminli_c57fr.winter_test_tags : ~0 rows (environ)
/*!40000 ALTER TABLE `winter_test_tags` DISABLE KEYS */;
/*!40000 ALTER TABLE `winter_test_tags` ENABLE KEYS */;

-- Listage de la structure de la table aadminli_c57fr. winter_test_themes
CREATE TABLE IF NOT EXISTS `winter_test_themes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table aadminli_c57fr.winter_test_themes : ~0 rows (environ)
/*!40000 ALTER TABLE `winter_test_themes` DISABLE KEYS */;
INSERT INTO `winter_test_themes` (`id`, `name`, `code`, `content`, `created_at`, `updated_at`) VALUES
	(1, 'Bootstrap', 'bootstrap', 'A bootstrap theme.', '2023-03-28 17:56:32', '2023-03-28 17:56:32');
/*!40000 ALTER TABLE `winter_test_themes` ENABLE KEYS */;

-- Listage de la structure de la table aadminli_c57fr. winter_test_users
CREATE TABLE IF NOT EXISTS `winter_test_users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `security_code` int(11) DEFAULT NULL,
  `media_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `media_file` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table aadminli_c57fr.winter_test_users : ~2 rows (environ)
/*!40000 ALTER TABLE `winter_test_users` DISABLE KEYS */;
INSERT INTO `winter_test_users` (`id`, `username`, `security_code`, `media_image`, `media_file`, `created_at`, `updated_at`) VALUES
	(1, 'Neo', 1111, NULL, NULL, '2023-03-28 17:56:31', '2023-03-28 17:56:31'),
	(2, 'Morpheus', 8888, NULL, NULL, '2023-03-28 17:56:31', '2023-03-28 17:56:31');
/*!40000 ALTER TABLE `winter_test_users` ENABLE KEYS */;

-- Listage de la structure de la table aadminli_c57fr. winter_test_users_roles
CREATE TABLE IF NOT EXISTS `winter_test_users_roles` (
  `user_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  `clearance_level` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `manager_id` int(10) unsigned DEFAULT NULL,
  `is_executive` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `awards` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`user_id`,`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table aadminli_c57fr.winter_test_users_roles : ~1 rows (environ)
/*!40000 ALTER TABLE `winter_test_users_roles` DISABLE KEYS */;
INSERT INTO `winter_test_users_roles` (`user_id`, `role_id`, `clearance_level`, `manager_id`, `is_executive`, `created_at`, `updated_at`, `awards`) VALUES
	(1, 1, 'Top Secret', NULL, 1, '2023-03-28 17:56:31', '2023-03-28 17:56:31', NULL);
/*!40000 ALTER TABLE `winter_test_users_roles` ENABLE KEYS */;

-- Listage de la structure de la table aadminli_c57fr. winter_user_mail_blockers
CREATE TABLE IF NOT EXISTS `winter_user_mail_blockers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `template` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `rainlab_user_mail_blockers_email_index` (`email`),
  KEY `rainlab_user_mail_blockers_template_index` (`template`),
  KEY `rainlab_user_mail_blockers_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table aadminli_c57fr.winter_user_mail_blockers : ~0 rows (environ)
/*!40000 ALTER TABLE `winter_user_mail_blockers` DISABLE KEYS */;
/*!40000 ALTER TABLE `winter_user_mail_blockers` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
