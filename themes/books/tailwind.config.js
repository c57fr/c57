module.exports = {
    content: [
        './**/*.htm',
    ],
    theme: {
        screens: {
            'sm': '480px', // new value for sm breakpoint
            'md': '768px',
            'lg': '1024px',
            'xl': '1280px',
            '2xl': '1536px',
        },
        extend: {},
    },
    plugins: [],
}
