
var result = document.getElementById("result");

var r = 'Différents exemples<hr>';


r += 'Exo suivant';

r += '<hr>';

r += "ffff:sddd".slice("ffff:sddd".indexOf(":")+1);

r += '<hr>';


var chaineCommencePar = function (str, motif) {
  return str.slice(0, motif.length) == motif;
}
r += chaineCommencePar('Il fait bô', 'Il f');

r += '<hr>';


var maSource = "Il fait beau today";
r += maSource.split(' ').join(' ');
r += '<br>';
var maSource2 = ["Il", "fait", "beau", "today"];
r += maSource2.join(" ").split(" ");

r += '<hr>';


var serie = function (n) {
  var arr = [];
  for (var i = 0; i <= n; i++)
    arr.push(i);
  return arr;
}
r += serie(7);

r += '<hr>';

var plusGrandQue = function (ref) {
  return function (n) {
    return n > ref;
  }
};
var plusGrandQueCinq = plusGrandQue(5);
r += plusGrandQueCinq(7);


var ajouter = function (a, b) {
  return a + b;
};
r += '<hr>Fonction anonyme:<br>ajouter(5, 7) = ' + ajouter(5, 7);

var but = 24;
r += '<hr>' + trouverSequence(but) + ' = ' + but;

r += '<hr>Dessinons un triangle en JS<br><br>' + dessineTriangle();

show(r);

function trouverSequence(objectif) {
  function trouver(debut, historique) {
    if (debut == objectif)
      return historique;
    else if (debut > objectif)
      return null;
    else
      return trouver(debut * 3, "(" + historique + " * 3)") || trouver(debut + 5, "(" + historique + " + 5)");
  }
  return trouver(1, "1");
}

function dessineTriangle() {
  var res = '/\\',
    i = 0; str = '', blanc = '';
  while (i < 9) {
    str += res + '<br>';
    blanc += '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
    res = '/' + blanc + '\\';
    i++;
  }
  for (i = 0; i < 40; i++)
    str += '-';
  //console.log(str);
  result.style.textAlign = 'center';
  return str;
}

function show(str) {
  result.innerHTML = str;
}

