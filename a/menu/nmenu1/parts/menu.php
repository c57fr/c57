<nav id="nav" role="navigation">
  <a href="#nav" title="Show navigation">Show navigation</a>
  <a href="#" title="Hide navigation">Hide navigation</a>
  <ul>
    <li><a href="./">Home</a></li>

    <li><a href="./blog.php" aria-haspopup="true">Blog</a>
      <ul>
        <li><a href="./design.php">Design</a></li>
        <li><a href="./html.php">HTML</a></li>
        <li><a href="./css.php">CSS</a></li>
        <li><a href="./js.php">JavaScript</a></li>
      </ul>
    </li>

    <li><a href="./about.php">About</a></li>
  </ul>
</nav>