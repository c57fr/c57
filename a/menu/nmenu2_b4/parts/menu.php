<nav class="navbar navbar-expand-sm navbar-light bg-light">

  <!--
  <a href="#nav" title="Show navigation">Show navigation</a>
  <a href="#" title="Hide navigation">Hide navigation</a>
  <a class="navbar-brand" href="http://menub4">Navbar</a>
  -->

  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="./">Home</a></li>



      <li class="nav-item dropdown">
            <a href="./blog.php" aria-haspopup="true">Blog</a>
          <a class="nav-link dropdown-toggle" href="./blog.php" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Blog</a>
        <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
          <li><a class="dropdown-item" href="./design.php">Design</a></li>
          <li><a class="dropdown-item" href="./html.php">HTML</a></li>
          <li><a class="dropdown-item" href="./css.php">CSS</a></li>
          <li><a class="dropdown-item" href="./js.php">JavaScript</a></li>
        </ul>
      </li>


      <li><a  class="nav-item" href="./about.php">About</a></li>
    </ul>
  </div>
</nav>