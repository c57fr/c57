<ul class="vertical medium-horizontal menu" data-responsive-menu="drilldown medium-dropdown" data-hide-for="small">
  
  <li>
    <a href="./">Index</a>
  </li>
  
  <li>
    <a href="./blog.php" aria-haspopup="true">Blog</a>
    <ul class="vertical menu">
      <li><a href="./design.php">Design</a></li>
      <li><a href="./html.php">HTML</a></li>
      <li><a href="./css.php">CSS</a></li>
      <li><a href="./js.php">JavaScript</a></li>
      <li><a href="#">Item 1B</a></li>
    </ul>
  </li>
  
  <li><a href="./about.php">About</a></li>
  
</ul>