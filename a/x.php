<?php

session_start(); // Démarrer une session pour stocker le score
// unset($_SESSION['score']);
if (!isset($_SESSION['score'])) { // Si le score n'est pas initialisé
	$_SESSION['score'] = 0; // Initialiser le score à 0

    $min           = 1; // Définir la limite inférieure de la plage de nombres possibles
    $max           = 1000; // Définir la limite supérieure de la plage de nombres possibles
    $mysteryNumber = rand($min, $max); // Générer un nombre aléatoire entre $min et $max
}

if (isset($_POST['guess'])) { // Si le formulaire a été soumis avec un nombre
	$guess = intval($_POST['guess']); // Convertir la chaîne en un entier
	++$_SESSION['score']; // Incrémenter le score

	if ($guess === $mysteryNumber) { // Si le nombre mystérieux a été trouvé
		echo 'Félicitations, vous avez trouvé le nombre mystérieux en ' . $_SESSION['score'] . ' coups !'; // Afficher le score
		$_SESSION['score'] = 0; // Réinitialiser le score
	} else { // Si le nombre n'est pas correct
		if ($guess < $mysteryNumber) { // Si le nombre est trop petit
			echo 'Le nombre mystérieux est plus grand que ' . $guess . '.';
		} else { // Si le nombre est trop grand
			echo 'Le nombre mystérieux est plus petit que ' . $guess . '.';
		}
		echo '<br>' . $mysteryNumber. ' - Score: ' . $_SESSION['score'];
	}
}

?>

<form method="post">
	<label for="guess">Devinez un nombre entre <?php echo $min; ?> et
		<?php echo $max; ?>:</label>
	<input type="number" id="guess" name="guess" required>
	<button type="submit">Envoyer</button>
</form>
