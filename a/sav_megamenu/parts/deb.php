<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="author" content="c57.fr">
  <meta name="viewport" content="width=device-width initial-scale=1.0 maximum-scale=1.0 user-scalable=yes" />
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Test</title>
  <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
  <link rel="icon" href="../favicon.ico" />

  <link rel="stylesheet" href="assets/css/demo.css" />
  <link rel="stylesheet" href="assets/css/menu.css" />

  <style>
    .header-logo {
      background: #54b4eb;
    }

    .logo {
      width: 80px;
      margin-top: 10px;
    }

    .header-menu {
      background: #eee;
      margin-bottom: 20px;
    }

    .dm-menu {
      --dm-bg: #eee;
      --dm-item-current-bg: #fff;
      --dm-item-hover-bg: #4ba2d3;
    }
  </style>
  <script src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>
  <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
  <script src="assets/js/menu.js"></script>
  <script src="assets/js/app.js"></script>
  <script src="assets/js/doubleTapToGo.js"></script>
</head>

<body>

  <?php include 'menu.php'; ?>
