<!DOCTYPE html>
<html>
<head>
	<title>QR Code Generator</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
</head>
<body>
	<div class="container">
		<h1>QR Code Generator</h1>
		<form method="post">
			<div class="form-group">
				<label for="url">URL :</label>
				<input type="text" name="url" class="form-control" placeholder="Entrez l'URL à encoder" required>
			</div>
			<button type="submit" class="btn btn-primary">Générer le code QR</button>
		</form>
		<?php
			if(isset($_POST['url'])){
				$url = $_POST['url'];
				$qrCodeUrl = 'https://chart.googleapis.com/chart?chs=150x150&cht=qr&chl='.urlencode($url);
				echo '<img src="'.$qrCodeUrl.'" alt="QR Code">';
			}
		?>
	</div>
</body>
</html>
